//
//  AKOpenGLView.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 24.02.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKOpenGLView.h"
#import "AKOpenGLRenderer.h"
#include <math.h>
#import "OpenGL/gl3.h"
#import "AKScene.h"

@interface AKOpenGLView ()

@property (nonatomic) GLuint projMatrixLoc;
@property (nonatomic) GLuint viewMatrixLoc;
@property (nonatomic, readonly) GLuint program;

@end

@implementation AKOpenGLView

AKOpenGLRenderer* m_renderer;
AKScene* scene;
bool space = false;
NSString *lal = @"lala";
float cursorSpeed = 0.005;
float clickEpsilon = 2;
NSPoint mouseClickPosition;

- (CVReturn) getFrameForTime:(const CVTimeStamp*)outputTime
{
	[self drawView];
	return kCVReturnSuccess;
}

// This is the renderer output callback function
/*
static CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,
									  const CVTimeStamp* now,
									  const CVTimeStamp* outputTime,
									  CVOptionFlags flagsIn,
									  CVOptionFlags* flagsOut,
									  void* displayLinkContext) {
    CVReturn result = [(__bridge AKOpenGLView*)displayLinkContext getFrameForTime:outputTime];
    return result;
}
*/

- (void) awakeFromNib {
    
    NSOpenGLPixelFormatAttribute attrs[] =
	{
		NSOpenGLPFADoubleBuffer,
		NSOpenGLPFADepthSize, 24,
		// Must specify the 3.2 Core Profile to use OpenGL 3.2
		NSOpenGLPFAOpenGLProfile,
		NSOpenGLProfileVersion3_2Core,
		0
	};
	
	NSOpenGLPixelFormat *pf = [[NSOpenGLPixelFormat alloc] initWithAttributes:attrs];
	
	if (!pf)
	{
		NSLog(@"No OpenGL pixel format");
	}
    
    NSOpenGLContext* context = [[NSOpenGLContext alloc] initWithFormat:pf shareContext:nil];
    
	CGLEnable([context CGLContextObj], kCGLCECrashOnRemovedFunctions);
	
    [self setPixelFormat:pf];
    [self setOpenGLContext:context];
    [self setWantsBestResolutionOpenGLSurface:YES];
    [_sceneContentTableView setDataSource:self];
    scene = [[AKScene alloc]init];
    
}

- (void) prepareOpenGL {
	[super prepareOpenGL];
    [self setAcceptsTouchEvents:YES];
	
	// Make all the OpenGL calls to setup rendering
	//  and build the necessary rendering objects
	[self initGL];
	/*
	// Create a display link capable of being used with all active displays
	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	
	// Set the renderer output callback function
	CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, (__bridge void *)(self));
	
	// Set the display link for the current renderer
	CGLContextObj cglContext = [[self openGLContext] CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = [[self pixelFormat] CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
	
	// Activate the display link
	CVDisplayLinkStart(displayLink);
	
	// Register to be notified when the window closes so we can stop the displaylink
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(windowWillClose:)
												 name:NSWindowWillCloseNotification
											   object:[self window]];
     */
}

- (BOOL) acceptsFirstResponder {
    return YES;
}

- (void) windowWillClose:(NSNotification*)notification {
	
	CVDisplayLinkStop(displayLink);
}

- (void) initGL {
	[[self openGLContext] makeCurrentContext];
	
	GLint swapInt = 1;
	[[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
	m_renderer = [[AKOpenGLRenderer alloc] initWithDefaultFBO:0];
    //[self setAutoresizesSubviews:YES];
    [_sceneContentTableView reloadData];
}

- (void) drawRect: (NSRect) bounds {
    [self drawView];
}

- (void) drawView {
	[[self openGLContext] makeCurrentContext];
	CGLLockContext([[self openGLContext] CGLContextObj]);
    
    [scene setBezierCheckbox:([_lamana state] == NSOnState)];
    if ([_checkbox3D state] == NSOnState) {
        [m_renderer render3DScene:scene];
    } else {
        [m_renderer renderScene:scene];
    }
    
    [_cursorScenePosition setStringValue:[scene cursorScenePositionString]];
    [_cursorScreenPosition setStringValue:[scene cursorScreenPositionString]];
    
	CGLFlushDrawable([[self openGLContext] CGLContextObj]);
	CGLUnlockContext([[self openGLContext] CGLContextObj]);
}

- (void) reshape {
	[super reshape];
	CGLLockContext([[self openGLContext] CGLContextObj]);
    
	NSRect viewRectPoints = [self bounds];
    NSRect viewRectPixels = [self convertRectToBacking:viewRectPoints];
    
	[m_renderer resizeWithWidth:viewRectPixels.size.width
                      AndHeight:viewRectPixels.size.height];
	[scene resizeWithWidth:viewRectPixels.size.width
                      AndHeight:viewRectPixels.size.height];
	
	CGLUnlockContext([[self openGLContext] CGLContextObj]);
}

- (void) renewGState {
	[[self window] disableScreenUpdatesUntilFlush];
    //[super renewGStat];
}

- (void) mouseDragged:(NSEvent *)theEvent {
    [scene rotateX:-[theEvent deltaY]*2/1000];
    if (space) {
        [scene rotateZ:-[theEvent deltaX]*2/1000];
    } else {
        [scene rotateY:-[theEvent deltaX]*2/1000];
    }
    

    [self setNeedsDisplay:TRUE];
}

- (void) mouseDown:(NSEvent *)theEvent {
    mouseClickPosition = [theEvent locationInWindow];
}

- (void) mouseUp:(NSEvent *)theEvent {
    NSPoint mouseUpPosition = [theEvent locationInWindow];
    if (sqrtf(
              (mouseUpPosition.x-mouseClickPosition.x)*(mouseUpPosition.x-mouseClickPosition.x) +
              (mouseUpPosition.y-mouseClickPosition.y) * (mouseUpPosition.y-mouseClickPosition.y)
              ) < clickEpsilon)
    {
        [self mouseClick:mouseClickPosition];
    }
}

- (void) mouseClick:(NSPoint)position {
    
    float width = [self bounds].size.width;
    float height = [self bounds].size.height;
    
    position.x = ((position.x/width)-0.5)*2.0;
    position.y = ((position.y/height)-0.5)*2.0;
    
    if ([self.bsplineMode state] != NSOnState) {
        int clicked = [scene selectPointAtPosition:position withBsplineMode:FALSE];
        if (clicked >= 0) {
            NSIndexSet *indexSet = [[NSIndexSet alloc] initWithIndex:clicked];
            [_sceneContentTableView selectRowIndexes:indexSet byExtendingSelection:NO];
        } else {
            [_sceneContentTableView deselectAll:nil];
        }
        [scene selectPointAtIndex:clicked];
    } else {
        int clicked = [scene selectPointAtPosition:position withBsplineMode:TRUE];
        [scene selectPointAtIndex:clicked];
    }

    [self setNeedsDisplay:TRUE];
}

- (void) scrollWheel:(NSEvent *)theEvent {
    if ([self.moveCursor state] == NSOnState) {
        if (space) {
            [scene moveCursorX:-[theEvent deltaX]/500 moveY:0 moveZ:[theEvent deltaY]/500];
        } else {
            [scene moveCursorX:-[theEvent deltaX]/500 moveY:[theEvent deltaY]/500 moveZ:0];
        }
    } else {
        if(space)
        {
            [scene moveX:-[theEvent deltaX]/500 moveY:0 moveZ:[theEvent deltaY]/500];
        }
        else
        {
            [scene moveX:-[theEvent deltaX]/500 moveY:[theEvent deltaY]/500 moveZ:0];
        }
    }
    [self setNeedsDisplay:TRUE];

}

- (void) magnifyWithEvent:(NSEvent *)event {
    [scene scale:1+[event magnification]];
    [self setNeedsDisplay:TRUE];
}

- (void) keyDown:(NSEvent *)theEvent {
    bool moveCursor = false;
    float x = 0;
    float y = 0;
    switch( [theEvent keyCode] ) {
    	case 53: //esc
            [scene reset];
            break;
        case 49: //space
            space = true;
            break;
        case 123: //left
            moveCursor = true;
            x -= cursorSpeed;
            break;
        case 124: //right
            x += cursorSpeed;
            moveCursor = true;
            break;
        case 125: //down
            y -= cursorSpeed;
            moveCursor = true;
            break;
        case 126: //up
            y+= cursorSpeed;
            moveCursor = true;
            break;
    }
    
    if (moveCursor) {
        if (space) {
            [scene moveCursorX:x moveY:0 moveZ:y];
        } else {
            [scene moveCursorX:x moveY:y moveZ:0];
        }
    }
    
    [self setNeedsDisplay:TRUE];
}

- (void) keyUp:(NSEvent *)theEvent {
    switch( [theEvent keyCode] ) {
    	case 49: //space
            space = false;
            break;
    }
    [self setNeedsDisplay:TRUE];
}

- (IBAction) refreshPressed:(NSButton*) sender {
    [scene refresh3DWithE:[_aPerspectiveTextField floatValue] r:[_rPerspectiveTextField floatValue]];
    [self setNeedsDisplay:TRUE];
}

- (IBAction) addPointClicked:(NSButton*) sender {
    [scene addPoint];
    [self setNeedsDisplay:TRUE];
    [_sceneContentTableView reloadData];
}

- (IBAction) clearButtonClicked:(NSButton*) sender {
    [scene clear];
    
    [self setNeedsDisplay:TRUE];
    [_sceneContentTableView reloadData];
}

- (IBAction) saveSceneClicked:(NSButton*) sender {
    [scene saveScene];
}

- (IBAction) loadSceneClicked:(NSButton*) sender {
    [scene loadScene];
    [self setNeedsDisplay:TRUE];
    [_sceneContentTableView reloadData];
}

- (IBAction) addBezierCurveClicked:(NSButton*) sender {
    NSIndexSet* selectedRows = [_sceneContentTableView selectedRowIndexes];
    [scene addBezierCurveFromPoints:selectedRows];
    
    [self setNeedsDisplay:TRUE];
    [_sceneContentTableView reloadData];
}

- (IBAction) addC2BezierCurveClicked:(NSButton*) sender {
    NSIndexSet* selectedRows = [_sceneContentTableView selectedRowIndexes];
    [scene addC2BezierCurveFromPoints:selectedRows withBsplineMode:([self.bsplineMode state] == NSOnState)];
    
    [self setNeedsDisplay:TRUE];
    [_sceneContentTableView reloadData];
}

- (IBAction) addInterpolatonCurveClicked:(NSButton*) sender {
    NSIndexSet* selectedRows = [_sceneContentTableView selectedRowIndexes];
    [scene addInterpolationCurveFromPoints:selectedRows];
    
    [self setNeedsDisplay:TRUE];
    [_sceneContentTableView reloadData];
}

- (IBAction) addBezierSurfaceClicked:(NSButton*) sender {
    
    if ([_bezierSurfaceRollerCheckbox state] == NSOnState) {
        [scene addBezierSurfaceOnRoller:YES withWidth:(int)[_bezierSurfaceVValue integerValue] andHeight:(int)[_bezierSurfaceUValue integerValue]rollerHeight:[_bezierSurfaceRollerHeight floatValue] planeRollerRadiusWidth:[_bezierSurfaceRollerRadius floatValue]];
    } else {
        [scene addBezierSurfaceOnRoller:NO withWidth:(int)[_bezierSurfaceVValue integerValue] andHeight:(int)[_bezierSurfaceUValue integerValue]rollerHeight:[_bezierSurfacePlaneHeight floatValue] planeRollerRadiusWidth:[_bezierSurfacePlaneWidth floatValue]];
    }
    

    
    [self setNeedsDisplay:TRUE];
    [_sceneContentTableView reloadData];
}

- (IBAction) addGregoryPatchClicked:(NSButton*) sender {
    
    NSIndexSet* selectedRows = [_sceneContentTableView selectedRowIndexes];
    [scene addGregoryPatchWithSelectedRows:selectedRows];
    
    
    [self setNeedsDisplay:TRUE];
    [_sceneContentTableView reloadData];
}

- (IBAction) addBezierC2SurfaceClicked:(NSButton*) sender {
    
    if ([_bezierSurfaceRollerCheckbox state] == NSOnState) {
        [scene addBezierC2SurfaceOnRoller:YES withWidth:(int)[_bezierSurfaceVValue integerValue] andHeight:(int)[_bezierSurfaceUValue integerValue]rollerHeight:[_bezierSurfaceRollerHeight floatValue] planeRollerRadiusWidth:[_bezierSurfaceRollerRadius floatValue]];
    } else {
        [scene addBezierC2SurfaceOnRoller:NO withWidth:(int)[_bezierSurfaceVValue integerValue] andHeight:(int)[_bezierSurfaceUValue integerValue]rollerHeight:[_bezierSurfacePlaneHeight floatValue] planeRollerRadiusWidth:[_bezierSurfacePlaneWidth floatValue]];
    }
    
    [self setNeedsDisplay:TRUE];
    [_sceneContentTableView reloadData];
}

- (IBAction) addKielichClicked:(NSButton*) sender {
    
    [scene addKielich];
    
    [self setNeedsDisplay:TRUE];
    [_sceneContentTableView reloadData];
}

- (IBAction) add3BezierSurfaces:(NSButton*) sender {
    
    [scene add3BezierSurfaces];
    
    [self setNeedsDisplay:TRUE];
    [_sceneContentTableView reloadData];
}

- (IBAction) deleteObject:(NSButton*) sender {
    [scene deleteObjects:[_sceneContentTableView selectedRowIndexes]];
    //[scene deleteSelectedObject];
    [scene selectPointAtIndex:-1];
    [self setNeedsDisplay:TRUE];
    [_sceneContentTableView reloadData];
}

- (IBAction) deletePointFromCurve:(NSButton*) sender {
    [scene deleteSelectedPointFromCurve:[_sceneContentTableView selectedRowIndexes]];
    [self setNeedsDisplay:TRUE];
}

- (IBAction) clicked3D:(NSButton*) sender {
    [self setNeedsDisplay:TRUE];
}

- (IBAction) clickedLamana:(NSButton*) sender {
    [self setNeedsDisplay:TRUE];
}

- (IBAction) clickedBsplineMode:(NSButton*) sender {
    [scene setBsplineMode:([self.bsplineMode state] == NSOnState)];
    [scene selectPointAtIndex:-1];
    [_sceneContentTableView deselectAll:nil];
    [self setNeedsDisplay:TRUE];
}

- (NSInteger) numberOfRowsInTableView:(NSTableView *)tableView {
    return [scene sceneElementsCount];
}

- (id)          tableView:(NSTableView *)tableView
objectValueForTableColumn:(NSTableColumn *)column
                      row:(NSInteger)rowIndex {
    //NSTextFieldCell *cell = [[NSTextFieldCell alloc]initTextCell:@"lalala"];
   // [cell setStringValue:@"ttt"];
    return [scene sceneElementAtIndex:rowIndex];
}

- (void)tableView:(NSTableView *)aTableView
   setObjectValue:(id)anObject
   forTableColumn:(NSTableColumn *)aTableColumn
              row:(NSInteger)rowIndex {
    NSString *stringValue = anObject;
    [scene setElementName:stringValue atIndex:(int)rowIndex];
}

- (IBAction)columnChangeSelected:(id)sender
{
    //NSIndexSet* selectedRows = [_sceneContentTableView selectedRowIndexes];
    NSInteger selectedRow = [_sceneContentTableView selectedRow];
    [scene selectPointAtIndex:(int)selectedRow];
    if ([scene selectedPoint] >= 0) {
        [_selectedPointLabel setStringValue:[NSString stringWithFormat:@"Selected: %@", [scene sceneElementAtIndex:selectedRow]]];
    } else {
        [_selectedPointLabel setStringValue:@""];
    }    [self setNeedsDisplay:TRUE];
}

- (IBAction)bezierSurfaceSliderValueChanged:(NSSlider*)sender {
    NSInteger uValue = _bezierSurfaceSliderU.integerValue;
    NSInteger vValue = _bezierSurfaceSliderV.integerValue;
    [_bezierSurfaceSliderUValue setStringValue:[NSString stringWithFormat:@"%ld", (long)uValue]];
    [_bezierSurfaceSliderVValue setStringValue:[NSString stringWithFormat:@"%ld", (long)vValue]];
    
    [scene updateBezierSurfacesWithUValue:uValue andWithVValue:vValue];
    
    [self setNeedsDisplay:TRUE];
}

- (IBAction)trimDValueSliderChange:(NSSlider*)sender {
    [_trimDValue setStringValue:[NSString stringWithFormat:@"%f", 1/[sender floatValue]]];
    
    [scene updateTrimDValue:1/[sender floatValue]];
    
    [self setNeedsDisplay:TRUE];
}


- (IBAction)gregoryPatchSliderValueChanged:(NSSlider*)sender {
    NSInteger uValue = _gregoryPatchSliderU.integerValue;
    NSInteger vValue = _gregoryPatchSliderV.integerValue;
    [_gregoryPatchSliderUValue setStringValue:[NSString stringWithFormat:@"%ld", (long)uValue]];
    [_gregoryPatchSliderVValue setStringValue:[NSString stringWithFormat:@"%ld", (long)vValue]];
    
    [scene updateGregoryPatchesWithUValue:uValue andWithVValue:vValue];
    
    [self setNeedsDisplay:TRUE];
}

- (IBAction)selectPointButtonClick:(id)sender
{
    int selected = [scene selectPointAtCursorPosition];
    if (selected >= 0) {
        [_selectedPointLabel setStringValue:[NSString stringWithFormat:@"Selected: %@", [scene sceneElementAtIndex:selected-1]]];
    } else {
        [_selectedPointLabel setStringValue:@""];
    }
    [self setNeedsDisplay:TRUE];
}

- (IBAction)findIntersectionClick:(id)sender {
    [scene findIntersection:[_sceneContentTableView selectedRowIndexes]];
    [_sceneContentTableView reloadData];
    [self setNeedsDisplay:TRUE];
}

- (IBAction)addSelectedPointsToSelectedCurveClick:(id)sender
{
    [scene addSelectedPointsToSelectedCurve:[_sceneContentTableView selectedRowIndexes]];
    [self setNeedsDisplay:TRUE];
}

- (IBAction)mergeSelectedPoints:(id)sender
{
    [scene mergeSelectedPoints:[_sceneContentTableView selectedRowIndexes]];
    [_sceneContentTableView reloadData];
    [self setNeedsDisplay:TRUE];
}

- (IBAction)deselectPointButtonClick:(id)sender
{
    [scene deselectPoint];
    [_selectedPointLabel setStringValue:@""];
    [self setNeedsDisplay:TRUE];
}

- (IBAction)generatePathsButtonClick:(id)sender
{
    [scene createHeightMapFromAllSurfaces];
}


@end
