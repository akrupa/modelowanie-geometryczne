//
//  AKBezierCurve.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 24.03.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKObject.h"
#import "AKPoint.h"

@interface AKBezierCurve : AKObject {
    NSUInteger pointsCount;
    NSMutableArray *nodesInternalArray;
    NSMutableArray *originalNodesInternalArray;
    
    GLKMatrix4 bernsteinMatrix;
    int m_screenWidth;
    int m_screenHeight;
    int nodesCount;
    int steps;
    int segments;
    int lines;
    
    int *steps2;
    int *lines2;
    int linesLamana;
    GLKMatrix4 MOld;
}

+ (int) qualityDivisor;
+ (void) setQualityDivisor:(int)divisor;

- (AKBezierCurve*) initWithPoints:(NSMutableArray*)pointsArray
                      screenWidth:(int)screenWidth
                     screenHeight:(int)screenHeight
                           number:(NSUInteger)number
                        andMatrix:(GLKMatrix4)M;

- (int) pointsCount;
- (int) linesCount;
- (void) countStepsFromMatrix:(GLKMatrix4)M;
- (void) addPointsToCurve:(NSArray*)pointsArray;
- (void) addPointToCurve:(AKPoint*)pointsArray;
- (void) deletePoint:(AKPoint*)point;
- (void) displayLamana;

-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize
    andArrayIndicOffset:(int *)offset;

@end
