//
//  AKSceneJSON.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 15.05.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKMath.h>

@interface AKSceneJSON : NSObject

- (void) doSomething;
- (void) saveBezierC2SurfacesToFile:(NSArray*)surfaces;
- (void) loadfromFileBezierC2Surfaces:(NSMutableArray*)surfaces withPoints:(NSMutableArray*)points usingScreenWitdh:(int)screenWidth screenHeight:(int)screenHeight andMatrix:(GLKMatrix4)M;

@end
