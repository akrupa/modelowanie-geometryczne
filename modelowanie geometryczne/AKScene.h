//
//  AKScene.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 25.03.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKMath.h>
#import "AKObject.h"
#import "AKPoint.h"

@interface AKScene : NSObject

@property int pointsCount;
@property int bsplinePointsCount;
@property int bezierCurvesCount;

@property int pointsIndexBegin;
@property int bsplinePointsIndexBegin;
@property int bezierCurvesIndexBegin;
@property int trimmingCurvesIndexBegin;
@property int trimmingLinesCount;
@property int bezierLinesCount;


- (AKScene*) init;

- (void) rotateX:(GLfloat)angle;
- (void) rotateY:(GLfloat)angle;
- (void) rotateZ:(GLfloat)angle;
- (void) moveCursorX:(float)deltaX moveY:(float)deltaY moveZ:(float)deltaZ;
- (void) moveX:(GLfloat)deltaX moveY:(GLfloat)deltaY moveZ:(GLfloat)deltaZ;
- (void) scale:(GLfloat)scale;
- (void) saveScene;
- (void) loadScene;
- (void) refresh3DWithE:(GLfloat)eValue r:(GLfloat)rvalue;
- (void) reset;
- (void) clear;

- (void) addPoint;
- (AKPoint*) addPointAtPosition:(GLKVector4)position;
- (void) addBezierCurveFromPoints:(NSIndexSet*)selectedObjects;
- (void) addC2BezierCurveFromPoints:(NSIndexSet*)selectedObjects withBsplineMode:(BOOL)bsplineMode;
- (void) addInterpolationCurveFromPoints:(NSIndexSet*)selectedObjects;
- (void) addGregoryPatchWithSelectedRows:(NSIndexSet*)selectedObjects;
- (void) add3BezierSurfaces;
- (void) updateBezierSurfacesWithUValue:(NSInteger)uValue andWithVValue:(NSInteger)vValue;
- (void) updateTrimDValue:(float)dValue;
- (void) updateGregoryPatchesWithUValue:(NSInteger)uValue andWithVValue:(NSInteger)vValue;
- (void) addBezierSurfaceOnRoller:(BOOL)onRoller withWidth:(int)width andHeight:(int)height rollerHeight:(float)rollerHeight planeRollerRadiusWidth:(float)radiusWidth;
- (void) addBezierC2SurfaceOnRoller:(BOOL)onRoller withWidth:(int)width andHeight:(int)height rollerHeight:(float)rollerHeight planeRollerRadiusWidth:(float)radiusWidth;
- (void) addKielich;
- (void) deleteSelectedObject;
- (void) findIntersection:(NSIndexSet*)selection;
- (int) selectPointAtPosition:(NSPoint)position withBsplineMode:(BOOL)bsplineMode;
- (int) selectPointAtCursorPosition;
- (void) deselectPoint;
- (void) selectPointAtIndex:(int)index;
- (int) selectedPoint;
- (int) selectedPointIndex;
- (void) addSelectedPointsToSelectedCurve:(NSIndexSet*)selection;
- (void) deleteObjects:(NSIndexSet*)selection;
- (void) deleteSelectedPointFromCurve:(NSIndexSet*)selection;
- (void) mergeSelectedPoints:(NSIndexSet*)selection;
- (void) resizeWithWidth:(GLuint)width AndHeight:(GLuint)height;
- (void) setBezierCheckbox:(BOOL)enabled;

- (NSString*)sceneElementAtIndex:(NSInteger)index;
- (NSInteger)sceneElementsCount;

- (NSString*)cursorScenePositionString;
- (NSString*)cursorScreenPositionString;
- (void) setElementName:(NSString*)name atIndex:(int)index;

- (void) copySceneToVertexArray:(GLKVector4**)vertexArray
                       withSize:(int*)vertexArraySize
                andIndicesArray:(indic**)indicArray
                       withSize:(int*)indicArraySize;

- (void) copyLeftEyeSceneSceneToVertexArray:(GLKVector4**)vertexArray
                                   withSize:(int*)vertexArraySize
                            andIndicesArray:(indic**)indicArray
                                   withSize:(int*)indicArraySize;

- (void) copyRightEyeSceneToVertexArray:(GLKVector4**)vertexArray
                               withSize:(int*)vertexArraySize
                        andIndicesArray:(indic**)indicArray
                               withSize:(int*)indicArraySize;

- (void) setBsplineMode: (bool)bsplineEnabled;

- (void) createHeightMapFromAllSurfaces;

@end
