//
//  AKTrimmingCurve.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 14.06.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKObject.h"
#import "AKPoint.h"
#import "AKBezierC2Surface.h"

@interface AKTrimmingCurve : AKObject {
    
    AKBezierC2Surface *m_surface1;
    AKBezierC2Surface *m_surface2;
    
    int m_screenWidth;
    int m_screenHeight;
    
    int steps;
    int lines;
    
    GLKMatrix4 MOld;
    
    float m_u0;
    float m_v0;
    float m_s0;
    float m_t0;
    
    float dValue;
    NSMutableArray *points;
    bool draw;
}

- (AKTrimmingCurve*) initWithSurface1:(AKBezierC2Surface*)surface1
                             surface2:(AKBezierC2Surface*)surface2
                               number:(NSUInteger)number
                            andMatrix:(GLKMatrix4)M;

- (void) countStepsFromMatrix:(GLKMatrix4) M;
-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize
    andArrayIndicOffset:(int *)offset;
- (int) linesCount;
- (int) curvesCount;
- (void) updateTrimDValue:(float)dValue;


@end
