//
//  AKCross.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 25.03.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKCross.h"

@implementation AKCross

-(AKObject*)initAtPosition:(GLKVector4)positionV andNumber:(NSUInteger)number {
    name = @"Cursor";
    internalIndicSize = 3;
    internalArraySize = 6;
    internalIndices = malloc(internalIndicSize * sizeof(indic));
    internalArray = malloc(internalArraySize * sizeof(GLKVector4));
    
    internalArray[0] = positionV;
    internalArray[1] = positionV;
    internalArray[2] = positionV;
    internalArray[3] = positionV;
    internalArray[4] = positionV;
    internalArray[5] = positionV;
    
    internalArray[0].x -= 0.05f;
    internalArray[1].x += 0.05f;
    internalArray[2].y -= 0.05f;
    internalArray[3].y += 0.05f;
    internalArray[4].z -= 0.05f;
    internalArray[5].z += 0.05f;
    
    internalIndices[0].from = 0;
    internalIndices[0].to = 1;
    internalIndices[1].from = 2;
    internalIndices[1].to = 3;
    internalIndices[2].from = 4;
    internalIndices[2].to = 5;
    
    return self;
}

-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize {
    
    memcpy(array + *arrayPoint, internalArray, internalArraySize * sizeof(GLKVector4));
    memcpy(indices + *indicesSize, internalIndices, internalIndicSize * sizeof(indic));
    
    *arrayPoint += internalArraySize;
    *indicesSize += internalIndicSize*2;
    
}

-(int)vertexArraySize {
    return 6;
}

-(int)indicArraySize {
    return 6;
}

@end
