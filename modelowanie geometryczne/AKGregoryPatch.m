//
//  AKGregoryPatch.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 07.06.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKGregoryPatch.h"
#import "AKBezierSurface.h"
#import "AKPoint.h"

@implementation AKGregoryPatch

static int m_qualityDivisor = 2;

- (AKGregoryPatch*) initWithC2BezierPatches:(NSMutableArray *)bezierPatches
                                screenWidth:(int)screenWidth
                               screenHeight:(int)screenHeight
                                     number:(NSUInteger)number
                                  andMatrix:(GLKMatrix4)M {
    
    name = [NSString stringWithFormat:@"Gregory patch%lu", number];
    
    m_screenWidth = screenWidth;
    m_screenHeight = screenHeight;
    MOld = M;
    lines = 0;
    steps = 0;
    u = 4;
    v = 4;
    
    patch0 = [[NSMutableArray alloc]init];
    patch1 = [[NSMutableArray alloc]init];
    patch2 = [[NSMutableArray alloc]init];
    
    patch00 = [[NSMutableArray alloc]init];
    patch01 = [[NSMutableArray alloc]init];
    patch10 = [[NSMutableArray alloc]init];
    patch11 = [[NSMutableArray alloc]init];
    patch20 = [[NSMutableArray alloc]init];
    patch21 = [[NSMutableArray alloc]init];
    
    
    controlPoints = [[NSMutableArray alloc]init];
    innerControlPoints = [[NSMutableArray alloc]init];
    
    for (int i=0; i<10; i++) {
        AKPoint *newPoint = [[AKPoint alloc]initAtPosition:GLKVector4Make(-100, 0, 0, 1) andNumber:300+i];
        [controlPoints addObject:newPoint];
    }
    for (int i=0; i<10; i++) {
        AKPoint *newPoint = [[AKPoint alloc]initAtPosition:GLKVector4Make(-100, 0, 0, 1) andNumber:400+i];
        [innerControlPoints addObject:newPoint];
    }
    
    controlPoints2 = [[NSMutableArray alloc]init];
    innerControlPoints2 = [[NSMutableArray alloc]init];
    
    for (int i=0; i<10; i++) {
        AKPoint *newPoint = [[AKPoint alloc]initAtPosition:GLKVector4Make(-100, 0, 0, 1) andNumber:300+i];
        [controlPoints2 addObject:newPoint];
    }
    for (int i=0; i<10; i++) {
        AKPoint *newPoint = [[AKPoint alloc]initAtPosition:GLKVector4Make(-100, 0, 0, 1) andNumber:400+i];
        [innerControlPoints2 addObject:newPoint];
    }
    
    controlPoints3 = [[NSMutableArray alloc]init];
    innerControlPoints3 = [[NSMutableArray alloc]init];
    
    for (int i=0; i<10; i++) {
        AKPoint *newPoint = [[AKPoint alloc]initAtPosition:GLKVector4Make(-100, 0, 0, 1) andNumber:300+i];
        [controlPoints3 addObject:newPoint];
    }
    for (int i=0; i<10; i++) {
        AKPoint *newPoint = [[AKPoint alloc]initAtPosition:GLKVector4Make(-100, 0, 0, 1) andNumber:400+i];
        [innerControlPoints3 addObject:newPoint];
    }

    
    AKBezierSurface *surface0 = [bezierPatches objectAtIndex:0];
    AKBezierSurface *surface1 = [bezierPatches objectAtIndex:1];
    AKBezierSurface *surface2 = [bezierPatches objectAtIndex:2];
    
    NSMutableSet *p0Set = [NSMutableSet setWithArray:[surface0 getPoints]];
    NSMutableSet *p1Set = [NSMutableSet setWithArray:[surface1 getPoints]];
    NSMutableSet *p2Set = [NSMutableSet setWithArray:[surface2 getPoints]];
    
    [p0Set intersectSet:p1Set];
    if ([p0Set count] == 1) {
        p0 = [[p0Set allObjects]objectAtIndex:0];
    } else {
        return nil;
    }
    
    [p1Set intersectSet:p2Set];
    if ([p1Set count] == 1) {
        p1 = [[p1Set allObjects]objectAtIndex:0];
    } else {
        return nil;
    }
    
    p0Set = [NSMutableSet setWithArray:[surface0 getPoints]];
    p2Set = [NSMutableSet setWithArray:[surface2 getPoints]];

    [p2Set intersectSet:p0Set];
    if ([p2Set count] == 1) {
        p2 = [[p2Set allObjects]objectAtIndex:0];
    } else {
        return nil;
    }
    NSLog(@"Good ;)");
    NSLog(@"Connection on %@, %@, %@", [p0 elementName], [p1 elementName], [p2 elementName]);
    
    NSMutableArray *pointsPatch0 = [surface0 getPoints];
    NSMutableArray *pointsPatch1 = [surface1 getPoints];
    NSMutableArray *pointsPatch2 = [surface2 getPoints];
    middlePoint = nil;
    
    NSLog(@"Selecting points from patch0:");
    [self buildArraysFromPoint1:p0 andPoint2:p2 andFromArrayPoints:pointsPatch0 toPatchArrays:patch0];
    NSLog(@"---------------------------------------------------------------");
    NSLog(@"Selecting points from patch1:");
    [self buildArraysFromPoint1:p0 andPoint2:p1 andFromArrayPoints:pointsPatch1 toPatchArrays:patch1];
    NSLog(@"---------------------------------------------------------------");
    NSLog(@"Selecting points from patch2:");
    [self buildArraysFromPoint1:p1 andPoint2:p2 andFromArrayPoints:pointsPatch2 toPatchArrays:patch2];

    [self rebuildSmallPatches];
    [self updateControlPointsPositions];
    return self;
}

- (AKPoint*)pointToAdd {
    return middlePoint;
}

- (void)buildArraysFromPoint1:(AKPoint*)point1
                    andPoint2:(AKPoint*)point2
           andFromArrayPoints:(NSMutableArray*)pointsArray
                toPatchArrays:(NSMutableArray*)patchArray {
    
    NSUInteger p1Index = [pointsArray indexOfObject:point1];
    NSUInteger p2Index = [pointsArray indexOfObject:point2];
    
    AKPoint* p01;
    AKPoint* p02;
    
    AKPoint* p10;
    AKPoint* p11;
    AKPoint* p12;
    AKPoint* p13;
    
    AKPoint* p20;
    AKPoint* p21;
    AKPoint* p22;
    AKPoint* p23;
    
    if (p1Index == 0 && p2Index == 3) {
        p01 = [pointsArray objectAtIndex:1];
        p02 = [pointsArray objectAtIndex:2];
        p10 = [pointsArray objectAtIndex:4];
        p11 = [pointsArray objectAtIndex:5];
        p12 = [pointsArray objectAtIndex:6];
        p13 = [pointsArray objectAtIndex:7];
        p20 = [pointsArray objectAtIndex:8];
        p21 = [pointsArray objectAtIndex:9];
        p22 = [pointsArray objectAtIndex:10];
        p23 = [pointsArray objectAtIndex:11];
    } else if (p1Index == 3 && p2Index == 0) {
        p01 = [pointsArray objectAtIndex:2];
        p02 = [pointsArray objectAtIndex:1];
        p10 = [pointsArray objectAtIndex:7];
        p11 = [pointsArray objectAtIndex:6];
        p12 = [pointsArray objectAtIndex:5];
        p13 = [pointsArray objectAtIndex:4];
        p20 = [pointsArray objectAtIndex:11];
        p21 = [pointsArray objectAtIndex:10];
        p22 = [pointsArray objectAtIndex:9];
        p23 = [pointsArray objectAtIndex:8];
    } else if (p1Index == 3 && p2Index == 15) {
        p01 = [pointsArray objectAtIndex:7];
        p02 = [pointsArray objectAtIndex:11];
        p10 = [pointsArray objectAtIndex:2];
        p11 = [pointsArray objectAtIndex:6];
        p12 = [pointsArray objectAtIndex:10];
        p13 = [pointsArray objectAtIndex:14];
        p20 = [pointsArray objectAtIndex:1];
        p21 = [pointsArray objectAtIndex:5];
        p22 = [pointsArray objectAtIndex:9];
        p23 = [pointsArray objectAtIndex:13];
    } else if (p1Index == 15 && p2Index == 3) {
        p01 = [pointsArray objectAtIndex:11];
        p02 = [pointsArray objectAtIndex:7];
        p10 = [pointsArray objectAtIndex:14];
        p11 = [pointsArray objectAtIndex:10];
        p12 = [pointsArray objectAtIndex:6];
        p13 = [pointsArray objectAtIndex:2];
        p20 = [pointsArray objectAtIndex:13];
        p21 = [pointsArray objectAtIndex:9];
        p22 = [pointsArray objectAtIndex:5];
        p23 = [pointsArray objectAtIndex:1];
    } else if (p1Index == 0 && p2Index == 12) {
        p01 = [pointsArray objectAtIndex:4];
        p02 = [pointsArray objectAtIndex:8];
        p10 = [pointsArray objectAtIndex:1];
        p11 = [pointsArray objectAtIndex:5];
        p12 = [pointsArray objectAtIndex:9];
        p13 = [pointsArray objectAtIndex:13];
        p20 = [pointsArray objectAtIndex:2];
        p21 = [pointsArray objectAtIndex:6];
        p22 = [pointsArray objectAtIndex:10];
        p23 = [pointsArray objectAtIndex:14];
    } else if (p1Index == 12 && p2Index == 0) {
        p01 = [pointsArray objectAtIndex:8];
        p02 = [pointsArray objectAtIndex:4];
        p10 = [pointsArray objectAtIndex:13];
        p11 = [pointsArray objectAtIndex:9];
        p12 = [pointsArray objectAtIndex:5];
        p13 = [pointsArray objectAtIndex:1];
        p20 = [pointsArray objectAtIndex:14];
        p21 = [pointsArray objectAtIndex:10];
        p22 = [pointsArray objectAtIndex:6];
        p23 = [pointsArray objectAtIndex:2];
    } else if (p1Index == 12 && p2Index == 15) {
        p01 = [pointsArray objectAtIndex:13];
        p02 = [pointsArray objectAtIndex:14];
        p10 = [pointsArray objectAtIndex:8];
        p11 = [pointsArray objectAtIndex:9];
        p12 = [pointsArray objectAtIndex:10];
        p13 = [pointsArray objectAtIndex:11];
        p20 = [pointsArray objectAtIndex:4];
        p21 = [pointsArray objectAtIndex:5];
        p22 = [pointsArray objectAtIndex:6];
        p23 = [pointsArray objectAtIndex:7];
    } else if (p1Index == 15 && p2Index == 12) {
        p01 = [pointsArray objectAtIndex:14];
        p02 = [pointsArray objectAtIndex:13];
        p10 = [pointsArray objectAtIndex:11];
        p11 = [pointsArray objectAtIndex:10];
        p12 = [pointsArray objectAtIndex:9];
        p13 = [pointsArray objectAtIndex:8];
        p20 = [pointsArray objectAtIndex:7];
        p21 = [pointsArray objectAtIndex:6];
        p22 = [pointsArray objectAtIndex:5];
        p23 = [pointsArray objectAtIndex:4];
    }
    
    NSMutableArray *line0 = [[NSMutableArray alloc]init];
    NSMutableArray *line1 = [[NSMutableArray alloc]init];
    NSMutableArray *line2 = [[NSMutableArray alloc]init];
    
    [line0 addObject:point1];
    [line0 addObject:p01];
    [line0 addObject:p02];
    [line0 addObject:point2];
    
    [line1 addObject:p10];
    [line1 addObject:p11];
    [line1 addObject:p12];
    [line1 addObject:p13];
    
    [line2 addObject:p20];
    [line2 addObject:p21];
    [line2 addObject:p22];
    [line2 addObject:p23];

    NSLog(@"line0: %@, %@, %@, %@",
          [[line0 objectAtIndex:0]elementName],
          [[line0 objectAtIndex:1]elementName],
          [[line0 objectAtIndex:2]elementName],
          [[line0 objectAtIndex:3]elementName]);
    
    NSLog(@"line1: %@, %@, %@, %@",
          [[line1 objectAtIndex:0]elementName],
          [[line1 objectAtIndex:1]elementName],
          [[line1 objectAtIndex:2]elementName],
          [[line1 objectAtIndex:3]elementName]);
    
    NSLog(@"line2: %@, %@, %@, %@",
          [[line2 objectAtIndex:0]elementName],
          [[line2 objectAtIndex:1]elementName],
          [[line2 objectAtIndex:2]elementName],
          [[line2 objectAtIndex:3]elementName]);
    
    [patchArray addObject:line0];
    [patchArray addObject:line1];
    [patchArray addObject:line2];
}

- (void) rebuildSmallPatches {
    [patch00 removeAllObjects];
    [patch01 removeAllObjects];
    [patch10 removeAllObjects];
    [patch11 removeAllObjects];
    [patch20 removeAllObjects];
    [patch21 removeAllObjects];
    
    NSMutableArray *line00 = [patch0 objectAtIndex:0];
    NSMutableArray *line01 = [patch0 objectAtIndex:1];
    NSMutableArray *line02 = [patch0 objectAtIndex:2];
    
    NSMutableArray *line10 = [patch1 objectAtIndex:0];
    NSMutableArray *line11 = [patch1 objectAtIndex:1];
    NSMutableArray *line12 = [patch1 objectAtIndex:2];
    
    NSMutableArray *line20 = [patch2 objectAtIndex:0];
    NSMutableArray *line21 = [patch2 objectAtIndex:1];
    NSMutableArray *line22 = [patch2 objectAtIndex:2];
    
    line00 = [self splitLine:line00];
    line01 = [self splitLine:line01];
    line02 = [self splitLine:line02];

    line10 = [self splitLine:line10];
    line11 = [self splitLine:line11];
    line12 = [self splitLine:line12];

    line20 = [self splitLine:line20];
    line21 = [self splitLine:line21];
    line22 = [self splitLine:line22];
    
    if (middlePoint == nil) {
        AKPoint *p0 = [line00 objectAtIndex:3];
        AKPoint *p1 = [line10 objectAtIndex:3];
        AKPoint *p2 = [line20 objectAtIndex:3];
        
        GLKVector4 position = GLKVector4Make(0, 0, 0, 0);
        position = GLKVector4Add(position, [p0 position]);
        position = GLKVector4Add(position, [p1 position]);
        position = GLKVector4Add(position, [p2 position]);
        position = GLKVector4DivideScalar(position, 3);
        middlePoint = [[AKPoint alloc]initAtPosition:position andNumber:200];
    }
    
    NSMutableArray *splitLine000 = [[NSMutableArray alloc]init];
    NSMutableArray *splitLine001 = [[NSMutableArray alloc]init];
    [splitLine000 addObject:[line00 objectAtIndex:0]];
    [splitLine000 addObject:[line00 objectAtIndex:1]];
    [splitLine000 addObject:[line00 objectAtIndex:2]];
    [splitLine000 addObject:[line00 objectAtIndex:3]];
    
    [splitLine001 addObject:[line00 objectAtIndex:3]];
    [splitLine001 addObject:[line00 objectAtIndex:4]];
    [splitLine001 addObject:[line00 objectAtIndex:5]];
    [splitLine001 addObject:[line00 objectAtIndex:6]];

    NSMutableArray *splitLine010 = [[NSMutableArray alloc]init];
    NSMutableArray *splitLine011 = [[NSMutableArray alloc]init];
    [splitLine010 addObject:[line01 objectAtIndex:0]];
    [splitLine010 addObject:[line01 objectAtIndex:1]];
    [splitLine010 addObject:[line01 objectAtIndex:2]];
    [splitLine010 addObject:[line01 objectAtIndex:3]];
    
    [splitLine011 addObject:[line01 objectAtIndex:3]];
    [splitLine011 addObject:[line01 objectAtIndex:4]];
    [splitLine011 addObject:[line01 objectAtIndex:5]];
    [splitLine011 addObject:[line01 objectAtIndex:6]];
    
    NSMutableArray *splitLine020 = [[NSMutableArray alloc]init];
    NSMutableArray *splitLine021 = [[NSMutableArray alloc]init];
    [splitLine020 addObject:[line02 objectAtIndex:0]];
    [splitLine020 addObject:[line02 objectAtIndex:1]];
    [splitLine020 addObject:[line02 objectAtIndex:2]];
    [splitLine020 addObject:[line02 objectAtIndex:3]];
    
    [splitLine021 addObject:[line02 objectAtIndex:3]];
    [splitLine021 addObject:[line02 objectAtIndex:4]];
    [splitLine021 addObject:[line02 objectAtIndex:5]];
    [splitLine021 addObject:[line02 objectAtIndex:6]];
    
    
    
    NSMutableArray *splitLine100 = [[NSMutableArray alloc]init];
    NSMutableArray *splitLine101 = [[NSMutableArray alloc]init];
    [splitLine100 addObject:[line10 objectAtIndex:0]];
    [splitLine100 addObject:[line10 objectAtIndex:1]];
    [splitLine100 addObject:[line10 objectAtIndex:2]];
    [splitLine100 addObject:[line10 objectAtIndex:3]];
    
    [splitLine101 addObject:[line10 objectAtIndex:3]];
    [splitLine101 addObject:[line10 objectAtIndex:4]];
    [splitLine101 addObject:[line10 objectAtIndex:5]];
    [splitLine101 addObject:[line10 objectAtIndex:6]];
    
    NSMutableArray *splitLine110 = [[NSMutableArray alloc]init];
    NSMutableArray *splitLine111 = [[NSMutableArray alloc]init];
    [splitLine110 addObject:[line11 objectAtIndex:0]];
    [splitLine110 addObject:[line11 objectAtIndex:1]];
    [splitLine110 addObject:[line11 objectAtIndex:2]];
    [splitLine110 addObject:[line11 objectAtIndex:3]];
    
    [splitLine111 addObject:[line11 objectAtIndex:3]];
    [splitLine111 addObject:[line11 objectAtIndex:4]];
    [splitLine111 addObject:[line11 objectAtIndex:5]];
    [splitLine111 addObject:[line11 objectAtIndex:6]];
    
    NSMutableArray *splitLine120 = [[NSMutableArray alloc]init];
    NSMutableArray *splitLine121 = [[NSMutableArray alloc]init];
    [splitLine120 addObject:[line12 objectAtIndex:0]];
    [splitLine120 addObject:[line12 objectAtIndex:1]];
    [splitLine120 addObject:[line12 objectAtIndex:2]];
    [splitLine120 addObject:[line12 objectAtIndex:3]];
    
    [splitLine121 addObject:[line12 objectAtIndex:3]];
    [splitLine121 addObject:[line12 objectAtIndex:4]];
    [splitLine121 addObject:[line12 objectAtIndex:5]];
    [splitLine121 addObject:[line12 objectAtIndex:6]];
    
    
    NSMutableArray *splitLine200 = [[NSMutableArray alloc]init];
    NSMutableArray *splitLine201 = [[NSMutableArray alloc]init];
    [splitLine200 addObject:[line20 objectAtIndex:0]];
    [splitLine200 addObject:[line20 objectAtIndex:1]];
    [splitLine200 addObject:[line20 objectAtIndex:2]];
    [splitLine200 addObject:[line20 objectAtIndex:3]];
    
    [splitLine201 addObject:[line20 objectAtIndex:3]];
    [splitLine201 addObject:[line20 objectAtIndex:4]];
    [splitLine201 addObject:[line20 objectAtIndex:5]];
    [splitLine201 addObject:[line20 objectAtIndex:6]];
    
    NSMutableArray *splitLine210 = [[NSMutableArray alloc]init];
    NSMutableArray *splitLine211 = [[NSMutableArray alloc]init];
    [splitLine210 addObject:[line21 objectAtIndex:0]];
    [splitLine210 addObject:[line21 objectAtIndex:1]];
    [splitLine210 addObject:[line21 objectAtIndex:2]];
    [splitLine210 addObject:[line21 objectAtIndex:3]];
    
    [splitLine211 addObject:[line21 objectAtIndex:3]];
    [splitLine211 addObject:[line21 objectAtIndex:4]];
    [splitLine211 addObject:[line21 objectAtIndex:5]];
    [splitLine211 addObject:[line21 objectAtIndex:6]];
    
    NSMutableArray *splitLine220 = [[NSMutableArray alloc]init];
    NSMutableArray *splitLine221 = [[NSMutableArray alloc]init];
    [splitLine220 addObject:[line22 objectAtIndex:0]];
    [splitLine220 addObject:[line22 objectAtIndex:1]];
    [splitLine220 addObject:[line22 objectAtIndex:2]];
    [splitLine220 addObject:[line22 objectAtIndex:3]];
    
    [splitLine221 addObject:[line22 objectAtIndex:3]];
    [splitLine221 addObject:[line22 objectAtIndex:4]];
    [splitLine221 addObject:[line22 objectAtIndex:5]];
    [splitLine221 addObject:[line22 objectAtIndex:6]];
    
    [patch00 addObject:splitLine000];
    [patch00 addObject:splitLine010];
    [patch00 addObject:splitLine020];
    
    splitLine001  = [NSMutableArray arrayWithArray:[[splitLine001 reverseObjectEnumerator]allObjects]];
    splitLine011  = [NSMutableArray arrayWithArray:[[splitLine011 reverseObjectEnumerator]allObjects]];
    splitLine021  = [NSMutableArray arrayWithArray:[[splitLine021 reverseObjectEnumerator]allObjects]];
    [patch01 addObject:splitLine001];
    [patch01 addObject:splitLine011];
    [patch01 addObject:splitLine021];
    
    [patch10 addObject:splitLine100];
    [patch10 addObject:splitLine110];
    [patch10 addObject:splitLine120];
    
    splitLine101  = [NSMutableArray arrayWithArray:[[splitLine101 reverseObjectEnumerator]allObjects]];
    splitLine111  = [NSMutableArray arrayWithArray:[[splitLine111 reverseObjectEnumerator]allObjects]];
    splitLine121  = [NSMutableArray arrayWithArray:[[splitLine121 reverseObjectEnumerator]allObjects]];
    [patch11 addObject:splitLine101];
    [patch11 addObject:splitLine111];
    [patch11 addObject:splitLine121];
    
    [patch20 addObject:splitLine200];
    [patch20 addObject:splitLine210];
    [patch20 addObject:splitLine220];
    
    splitLine201  = [NSMutableArray arrayWithArray:[[splitLine201 reverseObjectEnumerator]allObjects]];
    splitLine211  = [NSMutableArray arrayWithArray:[[splitLine211 reverseObjectEnumerator]allObjects]];
    splitLine221  = [NSMutableArray arrayWithArray:[[splitLine221 reverseObjectEnumerator]allObjects]];
    [patch21 addObject:splitLine201];
    [patch21 addObject:splitLine211];
    [patch21 addObject:splitLine221];
}

- (NSMutableArray*) splitLine:(NSMutableArray*)line {
    NSMutableArray* splittedLine = [[NSMutableArray alloc]init];
    
    AKPoint *point0 = [line objectAtIndex:0];
    AKPoint *point1 = [line objectAtIndex:1];
    AKPoint *point2 = [line objectAtIndex:2];
    AKPoint *point3 = [line objectAtIndex:3];
    
    GLKVector4 position0 = [point0 position];
    GLKVector4 position1 = [point1 position];
    GLKVector4 position2 = [point2 position];
    GLKVector4 position3 = [point3 position];
    
    GLKVector4 position001 = GLKVector4DivideScalar(GLKVector4Add(position0, position1), 2);
    GLKVector4 position012 = GLKVector4DivideScalar(GLKVector4Add(position1, position2), 2);
    GLKVector4 position023 = GLKVector4DivideScalar(GLKVector4Add(position2, position3), 2);
    
    GLKVector4 position112 = GLKVector4DivideScalar(GLKVector4Add(position001, position012), 2);
    GLKVector4 position121 = GLKVector4DivideScalar(GLKVector4Add(position012, position023), 2);

    GLKVector4 position222 = GLKVector4DivideScalar(GLKVector4Add(position112, position121), 2);
    
    AKPoint *p0 = [[AKPoint alloc]initAtPosition:position0 andNumber:0];
    AKPoint *p1 = [[AKPoint alloc]initAtPosition:position001 andNumber:0];
    AKPoint *p2 = [[AKPoint alloc]initAtPosition:position112 andNumber:0];
    AKPoint *p3 = [[AKPoint alloc]initAtPosition:position222 andNumber:0];
    AKPoint *p4 = [[AKPoint alloc]initAtPosition:position121 andNumber:0];
    AKPoint *p5 = [[AKPoint alloc]initAtPosition:position023 andNumber:0];
    AKPoint *p6 = [[AKPoint alloc]initAtPosition:position3 andNumber:0];
    
    [splittedLine addObject:p0];
    [splittedLine addObject:p1];
    [splittedLine addObject:p2];
    [splittedLine addObject:p3];
    [splittedLine addObject:p4];
    [splittedLine addObject:p5];
    [splittedLine addObject:p6];

    return splittedLine;
    
}

- (void)updateControlPointsPositions {
    for (int i=0; i<4; i++) {
        AKPoint *p0 = [[patch00 objectAtIndex:0]objectAtIndex:i];
        AKPoint *controlp0 = [controlPoints objectAtIndex:i];
        [controlp0 setPosition:[p0 position]];
    }
    for (int i=1; i<4; i++) {
        AKPoint *p0 = [[patch00 objectAtIndex:0]objectAtIndex:i];
        AKPoint *p1 = [[patch00 objectAtIndex:1]objectAtIndex:i];
        AKPoint *controlp0 = [controlPoints objectAtIndex:i+3];
        GLKVector4 position = GLKVector4Make(0, 0, 0, 0);
        GLKVector4 position0 = [p0 position];
        GLKVector4 position1 = [p1 position];
        position = GLKVector4Subtract(GLKVector4MultiplyScalar(position0, 2), position1);
        position.w = 1;
        [controlp0 setPosition:position];
    }
    for (int i=1; i<4; i++) {
        AKPoint *p3 = [[patch00 objectAtIndex:0]objectAtIndex:i];
        AKPoint *p2 = [[patch00 objectAtIndex:1]objectAtIndex:i];
        AKPoint *p1 = [[patch00 objectAtIndex:2]objectAtIndex:i];
        AKPoint *controlp0 = [controlPoints objectAtIndex:i+6];
        GLKVector4 position;
        GLKVector4 position3 = [p3 position];
        GLKVector4 position2 = [p2 position];
        GLKVector4 position1 = [p1 position];
        GLKVector4 position4 = GLKVector4Subtract(GLKVector4MultiplyScalar(position3, 2), position2);
        position.x = 2*position4.x - (2*position2.x - position1.x);
        position.y = 2*position4.y - (2*position2.y - position1.y);
        position.z = 2*position4.z - (2*position2.z - position1.z);
        position.w = 2*position4.w - (2*position2.w - position1.w);
        [controlp0 setPosition:position];
    }
    
    
    
    
    for (int i=0; i<4; i++) {
        AKPoint *p0 = [[patch10 objectAtIndex:0]objectAtIndex:i];
        AKPoint *controlp0 = [innerControlPoints objectAtIndex:i];
        [controlp0 setPosition:[p0 position]];
    }
    for (int i=1; i<4; i++) {
        AKPoint *p0 = [[patch10 objectAtIndex:0]objectAtIndex:i];
        AKPoint *p1 = [[patch10 objectAtIndex:1]objectAtIndex:i];
        AKPoint *controlp0 = [innerControlPoints objectAtIndex:i+3];
        GLKVector4 position = GLKVector4Make(0, 0, 0, 0);
        GLKVector4 position0 = [p0 position];
        GLKVector4 position1 = [p1 position];
        position = GLKVector4Subtract(GLKVector4MultiplyScalar(position0, 2), position1);
        position.w = 1;
        [controlp0 setPosition:position];
    }
    for (int i=1; i<4; i++) {
        AKPoint *p3 = [[patch10 objectAtIndex:0]objectAtIndex:i];
        AKPoint *p2 = [[patch10 objectAtIndex:1]objectAtIndex:i];
        AKPoint *p1 = [[patch10 objectAtIndex:2]objectAtIndex:i];
        AKPoint *controlp0 = [innerControlPoints objectAtIndex:i+6];
        GLKVector4 position;
        GLKVector4 position3 = [p3 position];
        GLKVector4 position2 = [p2 position];
        GLKVector4 position1 = [p1 position];
        GLKVector4 position4 = GLKVector4Subtract(GLKVector4MultiplyScalar(position3, 2), position2);
        position.x = 2*position4.x - (2*position2.x - position1.x);
        position.y = 2*position4.y - (2*position2.y - position1.y);
        position.z = 2*position4.z - (2*position2.z - position1.z);
        position.w = 2*position4.w - (2*position2.w - position1.w);
        [controlp0 setPosition:position];
    }
    
    
    
    
    for (int i=0; i<4; i++) {
        AKPoint *p0 = [[patch01 objectAtIndex:0]objectAtIndex:i];
        AKPoint *controlp0 = [controlPoints2 objectAtIndex:i];
        [controlp0 setPosition:[p0 position]];
    }
    for (int i=1; i<4; i++) {
        AKPoint *p0 = [[patch01 objectAtIndex:0]objectAtIndex:i];
        AKPoint *p1 = [[patch01 objectAtIndex:1]objectAtIndex:i];
        AKPoint *controlp0 = [controlPoints2 objectAtIndex:i+3];
        GLKVector4 position = GLKVector4Make(0, 0, 0, 0);
        GLKVector4 position0 = [p0 position];
        GLKVector4 position1 = [p1 position];
        position = GLKVector4Subtract(GLKVector4MultiplyScalar(position0, 2), position1);
        position.w = 1;
        [controlp0 setPosition:position];
    }
    for (int i=1; i<4; i++) {
        AKPoint *p3 = [[patch01 objectAtIndex:0]objectAtIndex:i];
        AKPoint *p2 = [[patch01 objectAtIndex:1]objectAtIndex:i];
        AKPoint *p1 = [[patch01 objectAtIndex:2]objectAtIndex:i];
        AKPoint *controlp0 = [controlPoints2 objectAtIndex:i+6];
        GLKVector4 position;
        GLKVector4 position3 = [p3 position];
        GLKVector4 position2 = [p2 position];
        GLKVector4 position1 = [p1 position];
        GLKVector4 position4 = GLKVector4Subtract(GLKVector4MultiplyScalar(position3, 2), position2);
        position.x = 2*position4.x - (2*position2.x - position1.x);
        position.y = 2*position4.y - (2*position2.y - position1.y);
        position.z = 2*position4.z - (2*position2.z - position1.z);
        position.w = 2*position4.w - (2*position2.w - position1.w);
        [controlp0 setPosition:position];
    }
    
    
    
    
    for (int i=0; i<4; i++) {
        AKPoint *p0 = [[patch21 objectAtIndex:0]objectAtIndex:i];
        AKPoint *controlp0 = [innerControlPoints2 objectAtIndex:i];
        [controlp0 setPosition:[p0 position]];
    }
    for (int i=1; i<4; i++) {
        AKPoint *p0 = [[patch21 objectAtIndex:0]objectAtIndex:i];
        AKPoint *p1 = [[patch21 objectAtIndex:1]objectAtIndex:i];
        AKPoint *controlp0 = [innerControlPoints2 objectAtIndex:i+3];
        GLKVector4 position = GLKVector4Make(0, 0, 0, 0);
        GLKVector4 position0 = [p0 position];
        GLKVector4 position1 = [p1 position];
        position = GLKVector4Subtract(GLKVector4MultiplyScalar(position0, 2), position1);
        position.w = 1;
        [controlp0 setPosition:position];
    }
    for (int i=1; i<4; i++) {
        AKPoint *p3 = [[patch21 objectAtIndex:0]objectAtIndex:i];
        AKPoint *p2 = [[patch21 objectAtIndex:1]objectAtIndex:i];
        AKPoint *p1 = [[patch21 objectAtIndex:2]objectAtIndex:i];
        AKPoint *controlp0 = [innerControlPoints2 objectAtIndex:i+6];
        GLKVector4 position;
        GLKVector4 position3 = [p3 position];
        GLKVector4 position2 = [p2 position];
        GLKVector4 position1 = [p1 position];
        GLKVector4 position4 = GLKVector4Subtract(GLKVector4MultiplyScalar(position3, 2), position2);
        position.x = 2*position4.x - (2*position2.x - position1.x);
        position.y = 2*position4.y - (2*position2.y - position1.y);
        position.z = 2*position4.z - (2*position2.z - position1.z);
        position.w = 2*position4.w - (2*position2.w - position1.w);
        [controlp0 setPosition:position];
    }
    
    
    
    
    
    
    for (int i=0; i<4; i++) {
        AKPoint *p0 = [[patch11 objectAtIndex:0]objectAtIndex:i];
        AKPoint *controlp0 = [controlPoints3 objectAtIndex:i];
        [controlp0 setPosition:[p0 position]];
    }
    for (int i=1; i<4; i++) {
        AKPoint *p0 = [[patch11 objectAtIndex:0]objectAtIndex:i];
        AKPoint *p1 = [[patch11 objectAtIndex:1]objectAtIndex:i];
        AKPoint *controlp0 = [controlPoints3 objectAtIndex:i+3];
        GLKVector4 position = GLKVector4Make(0, 0, 0, 0);
        GLKVector4 position0 = [p0 position];
        GLKVector4 position1 = [p1 position];
        position = GLKVector4Subtract(GLKVector4MultiplyScalar(position0, 2), position1);
        position.w = 1;
        [controlp0 setPosition:position];
    }
    for (int i=1; i<4; i++) {
        AKPoint *p3 = [[patch11 objectAtIndex:0]objectAtIndex:i];
        AKPoint *p2 = [[patch11 objectAtIndex:1]objectAtIndex:i];
        AKPoint *p1 = [[patch11 objectAtIndex:2]objectAtIndex:i];
        AKPoint *controlp0 = [controlPoints3 objectAtIndex:i+6];
        GLKVector4 position;
        GLKVector4 position3 = [p3 position];
        GLKVector4 position2 = [p2 position];
        GLKVector4 position1 = [p1 position];
        GLKVector4 position4 = GLKVector4Subtract(GLKVector4MultiplyScalar(position3, 2), position2);
        position.x = 2*position4.x - (2*position2.x - position1.x);
        position.y = 2*position4.y - (2*position2.y - position1.y);
        position.z = 2*position4.z - (2*position2.z - position1.z);
        position.w = 2*position4.w - (2*position2.w - position1.w);
        [controlp0 setPosition:position];
    }
    
    
    
    
    for (int i=0; i<4; i++) {
        AKPoint *p0 = [[patch20 objectAtIndex:0]objectAtIndex:i];
        AKPoint *controlp0 = [innerControlPoints3 objectAtIndex:i];
        [controlp0 setPosition:[p0 position]];
    }
    for (int i=1; i<4; i++) {
        AKPoint *p0 = [[patch20 objectAtIndex:0]objectAtIndex:i];
        AKPoint *p1 = [[patch20 objectAtIndex:1]objectAtIndex:i];
        AKPoint *controlp0 = [innerControlPoints3 objectAtIndex:i+3];
        GLKVector4 position = GLKVector4Make(0, 0, 0, 0);
        GLKVector4 position0 = [p0 position];
        GLKVector4 position1 = [p1 position];
        position = GLKVector4Subtract(GLKVector4MultiplyScalar(position0, 2), position1);
        position.w = 1;
        [controlp0 setPosition:position];
    }
    for (int i=1; i<4; i++) {
        AKPoint *p3 = [[patch20 objectAtIndex:0]objectAtIndex:i];
        AKPoint *p2 = [[patch20 objectAtIndex:1]objectAtIndex:i];
        AKPoint *p1 = [[patch20 objectAtIndex:2]objectAtIndex:i];
        AKPoint *controlp0 = [innerControlPoints3 objectAtIndex:i+6];
        GLKVector4 position;
        GLKVector4 position3 = [p3 position];
        GLKVector4 position2 = [p2 position];
        GLKVector4 position1 = [p1 position];
        GLKVector4 position4 = GLKVector4Subtract(GLKVector4MultiplyScalar(position3, 2), position2);
        position.x = 2*position4.x - (2*position2.x - position1.x);
        position.y = 2*position4.y - (2*position2.y - position1.y);
        position.z = 2*position4.z - (2*position2.z - position1.z);
        position.w = 2*position4.w - (2*position2.w - position1.w);
        [controlp0 setPosition:position];
    }
    
    
    

}

- (NSMutableArray*) getControlPoints {
    return controlPoints3;
}

- (NSMutableArray*) getInternalControlPoints {
    return innerControlPoints3;
}

-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize
    andArrayIndicOffset:(int *)offset {
    
    memcpy(array + *arrayPoint, internalArray, internalArraySize * sizeof(GLKVector4));
    
    GLushort *indic = (GLushort *)indices;
    GLushort *internalIndic = (GLushort *)internalIndices;
    for (int i=0; i<internalArraySize; i++) {
        //NSLog(@"%d: (%.3f, %.3f, %.3f)",i, internalArray[i].x, internalArray[i].y, internalArray[i].z);
    }
    
    for (int i=0; i < internalIndicSize; i++) {
        indic[*indicesSize + i] = internalIndic[i] + *offset;
    }
    
    for (int i=0; i<internalIndicSize; i+=2) {
        //NSLog(@"%d: %d-%d", i/2, internalIndic[i], internalIndic[i+1]);
    }
    
    *offset += internalArraySize;
    *arrayPoint += internalArraySize;
    *indicesSize += internalIndicSize;
    
}

- (int) linesCount {
    return lines;
}

-(int)vertexArraySize {
    return internalArraySize;
}

-(int)indicArraySize {
    return internalIndicSize*2;
}

- (void) countStepsFromMatrix:(GLKMatrix4) M {
    [self rebuildSmallPatches];
    [self updateControlPointsPositions];
    lines = 0;
    steps = 0;
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
    [tempArray addObject:middlePoint];
    [tempArray addObject:p0];
    
    
    float minX = MAXFLOAT;
    float minY = MAXFLOAT;
    float minZ = MAXFLOAT;
    float maxX = -MAXFLOAT;
    float maxY = -MAXFLOAT;
    float maxZ = -MAXFLOAT;
    for (int i=0; i<2; i++) {
        AKPoint* point = [tempArray objectAtIndex:i];
        GLKVector4 pointPosition = GLKMatrix4MultiplyVector4(M, [point position]);
        
        if (pointPosition.x < minX) {
            minX = pointPosition.x;
        }
        if (pointPosition.y < minY) {
            minY = pointPosition.y;
        }
        if (pointPosition.z < minZ) {
            minZ = pointPosition.z;
        }
        if (pointPosition.x > maxX) {
            maxX = pointPosition.x;
        }
        if (pointPosition.y > maxY) {
            maxY = pointPosition.y;
        }
        if (pointPosition.z > maxZ) {
            maxZ = pointPosition.z;
        }
    }
    float max = MAX(maxX-minX, maxY-minY);
    max = MAX(max, maxZ - minZ);
    
    steps = m_screenWidth*(max/m_qualityDivisor);
    lines = steps*(u+v)*3;
    
    internalArray = malloc((steps+1)*(u+v)*3*sizeof(GLKVector4));
    internalArraySize = (steps+1)*(u+v)*3;
    internalIndices = malloc((steps)*2*(u+v)*3 * sizeof(indic));
    internalIndicSize = (steps)*2*(u+v)*3;
    
    
    for (int j=0; j<u; j++) {
        for (int i=0; i<steps+1; i++) {
            GLKVector4 pos = [self Qu:(float)j/u v:(float)i/steps withControlPoints:controlPoints andInnerControlPoints:innerControlPoints];
            internalArray[i+j*(steps+1)] = pos;
        }
    }
    
    for (int j=0; j<v; j++) {
        for (int i=0; i<steps+1; i++) {
            GLKVector4 pos = [self Qu:(float)i/steps  v:(float)j/v withControlPoints:controlPoints andInnerControlPoints:innerControlPoints];
            internalArray[i+j*(steps+1) + u*(steps+1)] = pos;
        }
    }
    
    for (int j=0; j<u; j++) {
        for (int i=0; i<steps+1; i++) {
            GLKVector4 pos = [self Qu:(float)j/u v:(float)i/steps withControlPoints:controlPoints2 andInnerControlPoints:innerControlPoints2];
            internalArray[i+j*(steps+1)+u*(steps+1)+v*(steps+1)] = pos;
        }
    }
    
    for (int j=0; j<v; j++) {
        for (int i=0; i<steps+1; i++) {
            GLKVector4 pos = [self Qu:(float)i/steps  v:(float)j/v withControlPoints:controlPoints2 andInnerControlPoints:innerControlPoints2];
            internalArray[i+j*(steps+1) + u*(steps+1)+u*(steps+1)+v*(steps+1)] = pos;
        }
    }
    
    for (int j=0; j<u; j++) {
        for (int i=0; i<steps+1; i++) {
            GLKVector4 pos = [self Qu:(float)j/u v:(float)i/steps withControlPoints:controlPoints3 andInnerControlPoints:innerControlPoints3];
            internalArray[i+j*(steps+1)+2*(u*(steps+1)+v*(steps+1))] = pos;
        }
    }
    
    for (int j=0; j<v; j++) {
        for (int i=0; i<steps+1; i++) {
            GLKVector4 pos = [self Qu:(float)i/steps  v:(float)j/v withControlPoints:controlPoints3 andInnerControlPoints:innerControlPoints3];
            internalArray[i+j*(steps+1) + u*(steps+1)+2*(u*(steps+1)+v*(steps+1))] = pos;
        }
    }
    
    
    for (int j=0; j<u; j++) {
        for (int i=0; i<steps; i++) {
            internalIndices[i+j*steps].from = i+j*steps+j;
            internalIndices[i+j*steps].to = i+1+j*steps+j;
        }
    }
    
    for (int j=0; j<v; j++) {
        for (int i=0; i<steps; i++) {
            internalIndices[i+j*steps+u*steps].from = i+j*steps+j+u*(steps+1);
            internalIndices[i+j*steps+u*steps].to = i+1+j*steps+j+u*(steps+1);
        }
    }
    
    for (int j=0; j<u; j++) {
        for (int i=0; i<steps; i++) {
            internalIndices[i+j*steps+u*steps+v*steps].from = i+j*steps+j+u*(steps+1)+v*(steps+1);
            internalIndices[i+j*steps+u*steps+v*steps].to = i+1+j*steps+j+u*(steps+1)+v*(steps+1);
        }
    }
    
    for (int j=0; j<v; j++) {
        for (int i=0; i<steps; i++) {
            internalIndices[i+j*steps+u*steps+u*steps+v*steps].from = i+j*steps+j+u*(steps+1)+u*(steps+1)+v*(steps+1);
            internalIndices[i+j*steps+u*steps+u*steps+v*steps].to = i+1+j*steps+j+u*(steps+1)+u*(steps+1)+v*(steps+1);
        }
    }
    
    for (int j=0; j<u; j++) {
        for (int i=0; i<steps; i++) {
            internalIndices[i+j*steps+2*(u*steps+v*steps)].from = i+j*steps+j+2*(u*(steps+1)+v*(steps+1));
            internalIndices[i+j*steps+2*(u*steps+v*steps)].to = i+1+j*steps+j+2*(u*(steps+1)+v*(steps+1));
        }
    }
    
    for (int j=0; j<v; j++) {
        for (int i=0; i<steps; i++) {
            internalIndices[i+j*steps+u*steps+2*(u*steps+v*steps)].from = i+j*steps+j+u*(steps+1)+2*(u*(steps+1)+v*(steps+1));
            internalIndices[i+j*steps+u*steps+2*(u*steps+v*steps)].to = i+1+j*steps+j+u*(steps+1)+2*(u*(steps+1)+v*(steps+1));
        }
    }
}

+ (void) setQualityDivisor:(int)divisor {
    if (divisor < 1) {
        divisor = 1;
    }
    m_qualityDivisor = divisor;
}

+ (int) qualityDivisor {
    return m_qualityDivisor;
}

- (GLKVector4)Qu:(GLfloat)u v:(GLfloat)v withControlPoints:(NSMutableArray*)controlPoints andInnerControlPoints:(NSMutableArray*)innerControlPoints {

    AKPoint *p0 = middlePoint;
    AKPoint *p1 = [controlPoints objectAtIndex:3];
    AKPoint *p2 = [controlPoints objectAtIndex:0];
    AKPoint *p3 = [innerControlPoints objectAtIndex:3];
    AKPoint *e0Plus = [controlPoints objectAtIndex:9];
    AKPoint *e0Minus = [innerControlPoints objectAtIndex:9];
    AKPoint *e1Minus = [controlPoints objectAtIndex:6];
    AKPoint *e1Plus = [controlPoints objectAtIndex:2];
    AKPoint *e2Plus = [innerControlPoints objectAtIndex:1];
    AKPoint *e2Minus = [controlPoints objectAtIndex:1];
    AKPoint *e3Minus = [innerControlPoints objectAtIndex:2];
    AKPoint *e3Plus = [innerControlPoints objectAtIndex:6];
    AKPoint *f0Plus = [innerControlPoints objectAtIndex:8];
    AKPoint *f0Minus = [controlPoints objectAtIndex:8];
    AKPoint *f1Plus = [controlPoints objectAtIndex:5];
    AKPoint *f1Minus = [innerControlPoints objectAtIndex:7];
    AKPoint *f2Plus = [innerControlPoints objectAtIndex:4];
    AKPoint *f2Minus = [controlPoints objectAtIndex:4];
    AKPoint *f3Plus = [controlPoints objectAtIndex:7];
    AKPoint *f3Minus = [innerControlPoints objectAtIndex:5];
    
    GLKVector4 Bu = GLKVector4Make((1-u)*(1-u)*(1-u),
                                   3*(1-u)*(1-u)*u,
                                   3*(1-u)*u*u,
                                   u*u*u);
    
    GLKVector4 Bv = GLKVector4Make((1-v)*(1-v)*(1-v),
                                   3*(1-v)*(1-v)*v,
                                   3*(1-v)*v*v,
                                   v*v*v);
    
    float F0x;
    float F1x;
    float F2x;
    float F3x;
    if (u+v != 0) {
        F0x = (u*[f0Plus position].x + v*[f0Minus position].x)/(u+v);
    } else {
        F0x = 0;
    }
    if (1-u+v!=0) {
        F1x = ((1-u)*[f1Minus position].x + v*[f1Plus position].x)/(1-u+v);
    } else {
        F1x = 0;
    }
    if(2-u-v!=0) {
        F2x = ((1-u)*[f2Plus position].x + (1-v)*[f2Minus position].x)/(2-u-v);
    } else {
        F2x = 0;
    }
    if (1+u-v!=0) {
        F3x = (u*[f3Minus position].x + (1-v)*[f3Plus position].x)/(1+u-v);
    } else {
        F3x = 0;
    }
    
    float F0y;
    float F1y;
    float F2y;
    float F3y;
    if (u+v != 0) {
        F0y = (u*[f0Plus position].y + v*[f0Minus position].y)/(u+v);
    } else {
        F0y = 0;
    }
    if (1-u+v!=0) {
        F1y = ((1-u)*[f1Minus position].y + v*[f1Plus position].y)/(1-u+v);
    } else {
        F1y = 0;
    }
    if(2-u-v!=0) {
        F2y = ((1-u)*[f2Plus position].y + (1-v)*[f2Minus position].y)/(2-u-v);
    } else {
        F2y = 0;
    }
    if (1+u-v!=0) {
        F3y = (u*[f3Minus position].y + (1-v)*[f3Plus position].y)/(1+u-v);
    } else {
        F3y = 0;
    }
    
    float F0z;
    float F1z;
    float F2z;
    float F3z;
    if (u+v != 0) {
        F0z = (u*[f0Plus position].z + v*[f0Minus position].z)/(u+v);
    } else {
        F0z = 0;
    }
    if (1-u+v!=0) {
        F1z = ((1-u)*[f1Minus position].z + v*[f1Plus position].z)/(1-u+v);
    } else {
        F1z = 0;
    }
    if(2-u-v!=0) {
        F2z = ((1-u)*[f2Plus position].z + (1-v)*[f2Minus position].z)/(2-u-v);
    } else {
        F2z = 0;
    }
    if (1+u-v!=0) {
        F3z = (u*[f3Minus position].z + (1-v)*[f3Plus position].z)/(1+u-v);
    } else {
        F3z = 0;
    }
    
    GLKMatrix4 Gx = GLKMatrix4Make([p0 position].x, [e0Minus position].x, [e3Plus position].x, [p3 position].x,
                                   [e0Plus position].x, F0x, F3x, [e3Minus position].x,
                                   [e1Minus position].x, F1x, F2x, [e2Plus position].x,
                                   [p1 position].x, [e1Plus position].x, [e2Minus position].x, [p2 position].x);
    
    GLKMatrix4 Gy = GLKMatrix4Make([p0 position].y, [e0Minus position].y, [e3Plus position].y, [p3 position].y,
                                   [e0Plus position].y, F0y, F3y, [e3Minus position].y,
                                   [e1Minus position].y, F1y, F2y, [e2Plus position].y,
                                   [p1 position].y, [e1Plus position].y, [e2Minus position].y, [p2 position].y);
    
    GLKMatrix4 Gz = GLKMatrix4Make([p0 position].z, [e0Minus position].z, [e3Plus position].z, [p3 position].z,
                                   [e0Plus position].z, F0z, F3z, [e3Minus position].z,
                                   [e1Minus position].z, F1z, F2z, [e2Plus position].z,
                                   [p1 position].z, [e1Plus position].z, [e2Minus position].z, [p2 position].z);
    
    float x = GLKVector4DotProduct(GLKMatrix4MultiplyVector4(Gx, Bu), Bv);
    float y = GLKVector4DotProduct(GLKMatrix4MultiplyVector4(Gy, Bu), Bv);
    float z = GLKVector4DotProduct(GLKMatrix4MultiplyVector4(Gz, Bu), Bv);
    
    return GLKVector4Make(x, y, z, 1);
}

- (void) setUValue:(NSInteger)uValue andVValue:(NSInteger)vValue {
    u = (int)uValue;
    v = (int)vValue;
}

@end
