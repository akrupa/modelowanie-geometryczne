//
//  AKBezierC2Surface.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 11.05.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKObject.h"

@interface AKBezierC2Surface : AKObject {

NSMutableArray *nodesBezierInternalArray;
NSMutableArray *nodesBezierInternalArray2;
NSMutableArray *bezierCurvesArray;

GLKMatrix4 bernsteinMatrix;
GLKMatrix4 bernsteinDivMatrix;

int m_screenWidth;
int m_screenHeight;
int uSteps;
int vSteps;

int steps;
int lines;

int *steps2;
int *lines2;

int linesLamana;

int m_width;
int m_height;

GLKMatrix4 MOld;
    
}

@property NSMutableArray *nodesInternalArray;

- (AKBezierC2Surface*) initWithPoints:(NSMutableArray*)pointsArray
                        screenWidth:(int)screenWidth
                       screenHeight:(int)screenHeight
                          withWidth:(int)width
                         withHeight:(int)height
                          andMatrix:(GLKMatrix4)M;

- (void) countStepsFromMatrix:(GLKMatrix4) M;

-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize
    andArrayIndicOffset:(int *)offset;
- (int) linesCount;
- (int) curvesCount;
- (void) setUValue:(NSInteger)uValue andVValue:(NSInteger)vValue;
- (void) displayLamana;
- (NSNumber*) rows;
- (NSNumber*) columns;
- (GLKVector4) getPositionForU:(GLfloat)u andForV:(GLfloat)v;
- (GLKVector4) getDivUForU:(GLfloat)u andForV:(GLfloat)v;
- (GLKVector4) getDivVForU:(GLfloat)u andForV:(GLfloat)v;
- (void) applySurfaceToHeightMap:(float**)heightMap normalMap:(GLKVector4**)normalMap withSize:(unsigned int)size;
- (void) applySurfaceToHeightMap:(unsigned char*)heightMap withSize:(unsigned int)size andToNormalMap:(unsigned char*)normalMap;
- (void)generateFourthPathForFourthHeightMap:(float**)heightMap andNormalMap:(GLKVector4**)normalMap withSize:(unsigned int)size addToPaths:(NSMutableArray*)positions;

@end
