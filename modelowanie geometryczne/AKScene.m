//
//  AKScene.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 25.03.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKScene.h"
#import "AKObject.h"
#import "AKPoint.h"
#import "AKCross.h"
#import "AKBezierCurve.h"
#import "AKBezierC2Curve.h"
#import "AKBezierSurface.h"
#import "AKBezierC2Surface.h"
#import "AKSceneJSON.h"
#import "AKInterpolationCurve.h"
#import "AKGregoryPatch.h"
#import "AKTrimmingCurve.h"
#import <GLKit/GLKMath.h>


@implementation AKScene {

    GLuint m_viewWidth;
    GLuint m_viewHeight;
    
    NSUInteger pointPlusCounter;

    GLKMatrix4 transformationMatrix;
    GLKMatrix4 cursorTransformationMatrix;
    GLKMatrix4 projectionMatrixR;
    GLKMatrix4 projectionMatrixL;
    GLKMatrix4 projectionMatrix;

    GLKVector4 *array;
    GLKVector4 *transformArray;
    indic *indices;
    int arraySize;
    int indicSize;
    int selectedPoint;
    int selectedObject;

    float r_projection;
    float e_projection;

    GLKVector4 cursorScenePosition;
    GLKVector4 cursorScreenPostion;

    NSMutableArray *sceneElementsArray;
    AKCross *cursor;
    AKPoint *selectedAKPoint;

    int bezierArrayBegin;
    int bezierIndicBegin;
    
    bool displayLamana;
    bool sceneBsplineMode;
    
    bool isDirty;
    
    AKSceneJSON *jsonScene;
}

static const float selectionMouseEpsilon = 0.05;
static const float selectionCursorEpsilon = 0.01;


- (AKScene*) init {
    
    if((self = [super init])) {
        r_projection = 3.0f;
        e_projection = 0.3f;
        
        self.pointsCount = 0;
        self.bsplinePointsCount = 0;
        self.bezierCurvesCount = 0;
        
        self.pointsIndexBegin = 12;
        self.bezierCurvesIndexBegin = 6;
        
        m_viewWidth = 600;
		m_viewHeight = 600;
        
        transformationMatrix = GLKMatrix4Identity;
        cursorTransformationMatrix = GLKMatrix4Identity;
        projectionMatrixR = GLKMatrix4Identity;
        projectionMatrixL = GLKMatrix4Identity;
        projectionMatrix = GLKMatrix4Identity;
        projectionMatrixR.m23 = 1.0/3.0;
        projectionMatrixL.m23 = 1.0/3.0;
        projectionMatrix.m23 = 1.0/r_projection;
        
        projectionMatrixR.m20 = -0.3/6.0;
        projectionMatrixL.m20 = 0.3/6.0;
        
        sceneElementsArray = [[NSMutableArray alloc]init];
        jsonScene = [[AKSceneJSON alloc]init];
        
        cursorScenePosition.w = 1;
        pointPlusCounter = 0;
        
        arraySize = 0;
        indicSize = 0;
        selectedPoint = -1;
        selectedObject = -1;
        isDirty = true;
        
        cursor = [[AKCross alloc]initAtPosition:GLKVector4Make(0, 0, 0, 1) andNumber:0];
        [sceneElementsArray addObject:cursor];
        [self rebuildArrays];
        
    }
    return self;
}

- (void) resizeWithWidth:(GLuint)width AndHeight:(GLuint)height
{    
	m_viewWidth = width;
	m_viewHeight = height;
}

- (void) rebuildArrays {
    [self refreshBezierCurves];
    arraySize = 0;
    indicSize = 0;
    self.bezierCurvesIndexBegin = 6;
    self.bsplinePointsIndexBegin = 6;

    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        arraySize += [obj vertexArraySize];
        indicSize += [obj indicArraySize];
    }
    indicSize = (indicSize+1)/2;
    free(array);
    array = malloc((arraySize+1) * sizeof(GLKVector4));
    free(transformArray);
    transformArray = malloc((arraySize+1) * sizeof(GLKVector4));
    free(indices);
    indices = malloc(indicSize * sizeof(indic));
    
    int arrayPoint = 0;
    int indicPoint = 0;
    
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKCross class]]) {
            [obj addObjectToArray:array atArrayPoint:&arrayPoint andIndices:indices atIndicesPoint:&indicPoint];
        }
    }
    
    self.pointsCount = 0;
    self.bsplinePointsCount = 0;
    
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKPoint class]]) {
            self.bezierCurvesIndexBegin++;
            self.bsplinePointsIndexBegin ++;
            self.pointsCount++;
            [obj addObjectToArray:array atArrayPoint:&arrayPoint andIndices:indices atIndicesPoint:&indicPoint];
        }
    }
    if (self.bezierCurvesIndexBegin & 1) {
        self.bsplinePointsIndexBegin++;
        self.bezierCurvesIndexBegin++;
        indicPoint++;
    }
    
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKBezierC2Curve class]]) {
            AKBezierC2Curve *curve = (AKBezierC2Curve*)obj;
            if (curve.bsplineMode) {
                self.bsplinePointsCount += [curve.bsplineNodesInternalArray count];
                for (int j=0; j<[curve.bsplineNodesInternalArray count]; j++) {
                    AKPoint *pointObj = [curve.bsplineNodesInternalArray objectAtIndex:j];
                    [pointObj addObjectToArray:array atArrayPoint:&arrayPoint andIndices:indices atIndicesPoint:&indicPoint];
                    self.bezierCurvesIndexBegin++;
                }
            }
        }
    }
    if (self.bezierCurvesIndexBegin & 1) {
        self.bezierCurvesIndexBegin++;
        indicPoint++;
    }
    
    
    bezierArrayBegin = arrayPoint;
    bezierIndicBegin = indicPoint;
    
    [self rebuildBezierCurvesArrays];
}

- (void) rebuildBezierCurvesArrays {
    
    self.bezierLinesCount = 0;
    self.bezierCurvesCount = 0;
    int bezierArrayIndicBegin = bezierArrayBegin;
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKBezierCurve class]]) {
            AKBezierCurve *curve = (AKBezierCurve*)obj;
            self.bezierCurvesCount++;
            self.bezierLinesCount += [curve linesCount];
            [curve addObjectToArray:array
                       atArrayPoint:&bezierArrayBegin
                         andIndices:indices
                     atIndicesPoint:&bezierIndicBegin
                andArrayIndicOffset:&bezierArrayIndicBegin];
        }
        if ([obj isKindOfClass:[AKBezierC2Curve class]]) {
            AKBezierC2Curve *curve = (AKBezierC2Curve*)obj;
            self.bezierCurvesCount++;
            self.bezierLinesCount += [curve linesCount];
            [curve addObjectToArray:array
                       atArrayPoint:&bezierArrayBegin
                         andIndices:indices
                     atIndicesPoint:&bezierIndicBegin
                andArrayIndicOffset:&bezierArrayIndicBegin];
        }
        if ([obj isKindOfClass:[AKInterpolationCurve class]]) {
            AKInterpolationCurve *curve = (AKInterpolationCurve*)obj;
            self.bezierCurvesCount++;
            self.bezierLinesCount += [curve linesCount];
            [curve addObjectToArray:array
                       atArrayPoint:&bezierArrayBegin
                         andIndices:indices
                     atIndicesPoint:&bezierIndicBegin
                andArrayIndicOffset:&bezierArrayIndicBegin];
        }
    }
    
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKBezierSurface class]]) {
            AKBezierSurface *surface = (AKBezierSurface*)obj;
            self.bezierCurvesCount += [surface curvesCount];
            self.bezierLinesCount += [surface linesCount];
            [surface addObjectToArray:array
                         atArrayPoint:&bezierArrayBegin
                           andIndices:indices
                       atIndicesPoint:&bezierIndicBegin
                  andArrayIndicOffset:&bezierArrayIndicBegin];
        }
    }
    
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKBezierC2Surface class]]) {
            AKBezierC2Surface *surface = (AKBezierC2Surface*)obj;
            self.bezierCurvesCount += [surface curvesCount];
            self.bezierLinesCount += [surface linesCount];
            [surface addObjectToArray:array
                         atArrayPoint:&bezierArrayBegin
                           andIndices:indices
                       atIndicesPoint:&bezierIndicBegin
                  andArrayIndicOffset:&bezierArrayIndicBegin];
        }
    }
    
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKGregoryPatch class]]) {
            AKGregoryPatch *surface = (AKGregoryPatch*)obj;
            self.bezierLinesCount += [surface linesCount];
            [surface addObjectToArray:array
                         atArrayPoint:&bezierArrayBegin
                           andIndices:indices
                       atIndicesPoint:&bezierIndicBegin
                  andArrayIndicOffset:&bezierArrayIndicBegin];
        }
    }
    self.trimmingLinesCount = 0;
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKTrimmingCurve class]]) {
            AKTrimmingCurve *curve = (AKTrimmingCurve*)obj;
            self.trimmingLinesCount += [curve linesCount];
            self.trimmingCurvesIndexBegin = bezierIndicBegin;
            [curve addObjectToArray:array
                         atArrayPoint:&bezierArrayBegin
                           andIndices:indices
                       atIndicesPoint:&bezierIndicBegin
                  andArrayIndicOffset:&bezierArrayIndicBegin];
        }
    }
}

- (void) refreshBezierCurves {
    
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrix, transformationMatrix);
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKBezierCurve class]]) {
            AKBezierCurve *curve = (AKBezierCurve*)obj;
            [curve countStepsFromMatrix:M];
            if (displayLamana) {
                [curve displayLamana];
            }
        }
        if ([obj isKindOfClass:[AKBezierC2Curve class]]) {
            AKBezierC2Curve *curve = (AKBezierC2Curve*)obj;
            [curve countStepsFromMatrix:M];
            if (displayLamana) {
                [curve displayLamana];
            }
        }
        if ([obj isKindOfClass:[AKInterpolationCurve class]]) {
            AKInterpolationCurve *curve = (AKInterpolationCurve*)obj;
            [curve countStepsFromMatrix:M];
        }
        if ([obj isKindOfClass:[AKTrimmingCurve class]]) {
            AKTrimmingCurve *curve = (AKTrimmingCurve*)obj;
            if (isDirty) {
                [curve countStepsFromMatrix:M];
                isDirty = false;
            }
        }
        if ([obj isKindOfClass:[AKBezierSurface class]]) {
            AKBezierSurface *surface = (AKBezierSurface*)obj;
            [surface countStepsFromMatrix:M];
            if (displayLamana) {
                [surface displayLamana];
            }
        }
        if ([obj isKindOfClass:[AKBezierC2Surface class]]) {
            AKBezierC2Surface *surface = (AKBezierC2Surface*)obj;
            [surface countStepsFromMatrix:M];
            if (displayLamana) {
                [surface displayLamana];
            }
        }
        if ([obj isKindOfClass:[AKGregoryPatch class]]) {
            AKGregoryPatch *surface = (AKGregoryPatch*)obj;
            [surface countStepsFromMatrix:M];
        }
    }
}

- (void) rotateX:(float)angle {
    GLKMatrix4 matrix = GLKMatrix4Identity;
    matrix.m11 = cos(angle);
    matrix.m12 = sin(angle);
    matrix.m21 = -sin(angle);
    matrix.m22 = cos(angle);
    transformationMatrix = GLKMatrix4Multiply(matrix, transformationMatrix);
    [self rebuildArrays];
}

- (void) rotateY:(float)angle {
    GLKMatrix4 matrix = GLKMatrix4Identity;
    matrix.m00 = cos(angle);
    matrix.m02 = -sin(angle);
    matrix.m20 = sin(angle);
    matrix.m22 = cos(angle);
    transformationMatrix = GLKMatrix4Multiply(matrix, transformationMatrix);
    [self rebuildArrays];
}

- (void) rotateZ:(float)angle {
    GLKMatrix4 matrix = GLKMatrix4Identity;
    matrix.m00 = cos(angle);
    matrix.m10 = -sin(angle);
    matrix.m01 = sin(angle);
    matrix.m11 = cos(angle);
    transformationMatrix = GLKMatrix4Multiply(matrix, transformationMatrix);
    [self rebuildArrays];
}

- (void) moveX:(float)deltaX moveY:(float)deltaY moveZ:(float)deltaZ {
    GLKMatrix4 matrix = GLKMatrix4Identity;
    matrix.m30 = deltaX;
    matrix.m31 = deltaY;
    matrix.m32 = deltaZ;
    transformationMatrix = GLKMatrix4Multiply(matrix, transformationMatrix);
    [self rebuildArrays];
}

- (void) moveCursorX:(float)deltaX moveY:(float)deltaY moveZ:(float)deltaZ {
    GLKMatrix4 matrix = GLKMatrix4Identity;
    matrix.m30 = deltaX;
    matrix.m31 = deltaY;
    matrix.m32 = deltaZ;
    cursorTransformationMatrix = GLKMatrix4Multiply(matrix, cursorTransformationMatrix);
    cursorScenePosition = GLKMatrix4GetColumn(cursorTransformationMatrix, 3);
    if (sceneBsplineMode) {
        if (selectedAKPoint != nil) {
            isDirty = true;
            [selectedAKPoint movePointAtX:deltaX AtY:deltaY AtZ:deltaZ];
            for (int i=0; i < [sceneElementsArray count]; i++) {
                AKObject *obj = [sceneElementsArray objectAtIndex:i];
                if ([obj isKindOfClass:[AKBezierC2Curve class]]) {
                    AKBezierC2Curve *curve = (AKBezierC2Curve*)obj;
                    [curve refreshBernstein];
                }
            }
        }
        
    } else {
        if (selectedPoint >= 0) {
            AKPoint *point = [sceneElementsArray objectAtIndex:(selectedPoint+1)];
            [point movePointAtX:deltaX AtY:deltaY AtZ:deltaZ];
            isDirty = true;
        }
        for (int i=0; i < [sceneElementsArray count]; i++) {
            AKObject *obj = [sceneElementsArray objectAtIndex:i];
            if ([obj isKindOfClass:[AKBezierC2Curve class]]) {
                AKBezierC2Curve *curve = (AKBezierC2Curve*)obj;
                [curve refreshBernstein];
            }
            isDirty = true;
        }
    }

    [self rebuildArrays];
}

- (void) scale:(float)scale {
    GLKMatrix4 matrix = GLKMatrix4Identity;
    matrix.m00 = scale;
    matrix.m11 = scale;
    matrix.m22 = scale;
    transformationMatrix = GLKMatrix4Multiply(matrix, transformationMatrix);
    [self rebuildArrays];
}

- (void) saveScene {
    NSMutableArray *surfacesArray = [[NSMutableArray alloc]init];
    
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKBezierC2Surface class]]) {
            AKBezierC2Surface *surface = (AKBezierC2Surface*)obj;
            [surfacesArray addObject:surface];
        }
    }
    [jsonScene saveBezierC2SurfacesToFile:surfacesArray];
}

- (void) loadScene {
    NSMutableArray *surfaces = [[NSMutableArray alloc]init];
    NSMutableArray *points = [[NSMutableArray alloc]init];
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrixL, transformationMatrix);
    [jsonScene loadfromFileBezierC2Surfaces:surfaces withPoints:points usingScreenWitdh:m_viewWidth screenHeight:m_viewHeight andMatrix:M];
    if ([surfaces count] != 0 && [points count] != 0) {
        [self clear];
        for (int i=0; i<[points count]; i++) {
            AKPoint *point = [points objectAtIndex:i];
            [sceneElementsArray addObject:point];
        }
        for (int i=0; i<[surfaces count]; i++) {
            AKPoint *surface = [surfaces objectAtIndex:i];
            [sceneElementsArray addObject:surface];
        }
        [self rebuildArrays];
    }
}

- (void) refresh3DWithE:(GLfloat)eValue r:(GLfloat)rvalue {
    
    r_projection = rvalue;
    e_projection = eValue;
    
    projectionMatrix.m23 = 1/r_projection;
    projectionMatrixL.m23 = 1/r_projection;
    projectionMatrixR.m23 = 1/r_projection;
    
    projectionMatrixL.m20 = e_projection/(2*r_projection);
    projectionMatrixR.m20 = -e_projection/(2*r_projection);
}

- (void) reset {
    transformationMatrix = GLKMatrix4Identity;
}

- (void) clear {
    [sceneElementsArray removeAllObjects];
    cursor = [[AKCross alloc]initAtPosition:GLKVector4Make(0, 0, 0, 1) andNumber:0];
    [sceneElementsArray addObject:cursor];
    [self rebuildArrays];
}


- (void) addPoint {
    GLKVector4 position  = cursorScenePosition;
    NSLog(@"Insert point at position: %.3f, %.3f, %.3f", position.x, position.y, position.z);
    AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:pointPlusCounter++];
    
    if (selectedObject >= 0) {
        AKObject *obj = [sceneElementsArray objectAtIndex:selectedObject+1];
        if ([obj isKindOfClass:[AKBezierCurve class]]) {
            AKBezierCurve *curve = (AKBezierCurve*)obj;
            [curve addPointToCurve:point];
        }
        if ([obj isKindOfClass:[AKBezierC2Curve class]]) {
            AKBezierC2Curve *curve = (AKBezierC2Curve*)obj;
            [curve addPointToCurve:point];
        }
        if ([obj isKindOfClass:[AKInterpolationCurve class]]) {
            AKInterpolationCurve *curve = (AKInterpolationCurve*)obj;
            [curve addPointToCurve:point];
        }
    }
    
    [sceneElementsArray addObject:point];
    [self rebuildArrays];
}

- (AKPoint*) addPointAtPosition:(GLKVector4)position {
    NSLog(@"Insert point at position: %.3f, %.3f, %.3f", position.x, position.y, position.z);
    AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:pointPlusCounter++];
    [sceneElementsArray addObject:point];
    [self rebuildArrays];
    return point;
}

- (void) addBezierCurveFromPoints:(NSIndexSet*)selectedObjects {
    NSMutableArray *selectedPoints = [[NSMutableArray alloc]init];
    
    [selectedObjects enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        AKObject *obj = [sceneElementsArray objectAtIndex:idx+1];
        if ([obj isKindOfClass:[AKPoint class]]) {
            [selectedPoints addObject:obj];
        }
    }];
    if([selectedPoints count] == 0) {
        return;
    }
    
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrixL, transformationMatrix);
    
    AKBezierCurve *curve = [[AKBezierCurve alloc]initWithPoints:selectedPoints
                                                    screenWidth:m_viewWidth
                                                   screenHeight:m_viewHeight
                                                      number:0
                                                      andMatrix:M];
    
    [sceneElementsArray addObject:curve];
    [self rebuildArrays];
}

- (void) addGregoryPatchWithSelectedRows:(NSIndexSet*)selectedObjects {
    NSMutableArray *selectedPatches = [[NSMutableArray alloc]init];
    
    [selectedObjects enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        AKObject *obj = [sceneElementsArray objectAtIndex:idx+1];
        if ([obj isKindOfClass:[AKBezierSurface class]]) {
            [selectedPatches addObject:obj];
        }
    }];
    if([selectedPatches count] != 3) {
        return;
    }
    
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrixL, transformationMatrix);
    
    AKGregoryPatch *gregoryPatch = [[AKGregoryPatch alloc]initWithC2BezierPatches:selectedPatches screenWidth:m_viewWidth screenHeight:m_viewHeight number:0 andMatrix:M];
    if (gregoryPatch == nil) {
        return;
    }
    
    AKPoint *pointToAdd = [gregoryPatch pointToAdd];
    [sceneElementsArray addObject:pointToAdd];
    [sceneElementsArray addObject:gregoryPatch];
    
    /*
    NSMutableArray *pointsToAdd = [gregoryPatch getControlPoints];
    for (int i=0; i<[pointsToAdd count]; i++) {
        [sceneElementsArray addObject:[pointsToAdd objectAtIndex:i]];
    }
    
    NSMutableArray *pointsToAdd2 = [gregoryPatch getInternalControlPoints];
    for (int i=0; i<[pointsToAdd2 count]; i++) {
        [sceneElementsArray addObject:[pointsToAdd2 objectAtIndex:i]];
    }
     */
    [self rebuildArrays];
}

- (void) addC2BezierCurveFromPoints:(NSIndexSet*)selectedObjects withBsplineMode:(BOOL)bsplineMode {
    NSMutableArray *selectedPoints = [[NSMutableArray alloc]init];
    sceneBsplineMode = bsplineMode;
    [selectedObjects enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        AKObject *obj = [sceneElementsArray objectAtIndex:idx+1];
        if ([obj isKindOfClass:[AKPoint class]]) {
            [selectedPoints addObject:obj];
        }
    }];
    if([selectedPoints count] == 0) {
        return;
    }
    
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrixL, transformationMatrix);
    
    AKBezierC2Curve *curve = [[AKBezierC2Curve alloc]initWithBsplinePoints:selectedPoints screenWidth:m_viewWidth screenHeight:m_viewHeight number:0 andMatrix:M];
    
    [sceneElementsArray addObject:curve];
    [self rebuildArrays];
}

- (void) addInterpolationCurveFromPoints:(NSIndexSet*)selectedObjects {
    
    NSMutableArray *selectedPoints = [[NSMutableArray alloc]init];
    [selectedObjects enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        AKObject *obj = [sceneElementsArray objectAtIndex:idx+1];
        if ([obj isKindOfClass:[AKPoint class]]) {
            [selectedPoints addObject:obj];
        }
    }];
    if([selectedPoints count] == 0) {
        return;
    }
    
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrixL, transformationMatrix);
    
    AKInterpolationCurve *curve = [[AKInterpolationCurve alloc]initWithPoints:selectedPoints screenWidth:m_viewWidth screenHeight:m_viewHeight number:0 andMatrix:M];
    

    [sceneElementsArray addObject:curve];
    [self rebuildArrays];
     
}

- (void) deleteObjects:(NSIndexSet*)selectedObjects {
    NSMutableArray *selectedObjectsArray = [[NSMutableArray alloc]init];

    [selectedObjects enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        AKObject *obj = [sceneElementsArray objectAtIndex:idx+1];
        [selectedObjectsArray addObject:obj];
    }];
    
    for (int i=0; i<[selectedObjectsArray count]; i++) {
        [sceneElementsArray removeObject:[selectedObjectsArray objectAtIndex:i]];
    }
    
    [self rebuildArrays];
}

- (void) updateBezierSurfacesWithUValue:(NSInteger)uValue andWithVValue:(NSInteger)vValue {
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKBezierSurface class]]) {
            AKBezierSurface *surface = (AKBezierSurface*)obj;
            [surface setUValue:uValue andVValue:vValue];
        }
        if ([obj isKindOfClass:[AKBezierC2Surface class]]) {
            AKBezierC2Surface *surface = (AKBezierC2Surface*)obj;
            [surface setUValue:uValue andVValue:vValue];
        }
    }
    [self rebuildArrays];
}

- (void) updateTrimDValue:(float)dValue {
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKTrimmingCurve class]]) {
            AKTrimmingCurve *curve = (AKTrimmingCurve*)obj;
            [curve updateTrimDValue:dValue];
        }
    }
    isDirty = true;
    [self rebuildArrays];
}

- (void) updateGregoryPatchesWithUValue:(NSInteger)uValue andWithVValue:(NSInteger)vValue {
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKGregoryPatch class]]) {
            AKGregoryPatch *surface = (AKGregoryPatch*)obj;
            [surface setUValue:uValue andVValue:vValue];
        }
    }
    [self rebuildArrays];
}

- (void) add3BezierSurfaces {
    NSMutableArray *surfacePoints1 = [[NSMutableArray alloc]init];
    NSMutableArray *surfacePoints2 = [[NSMutableArray alloc]init];
    NSMutableArray *surfacePoints3 = [[NSMutableArray alloc]init];
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrixL, transformationMatrix);
    
    AKPoint *p11;
    AKPoint *p12;
    AKPoint *p21;
    AKPoint *p22;
    AKPoint *p31;
    AKPoint *p32;

    
    float distX = 4/(1*4);
    float distZ = 4/(1*4);
    for (int i=0; i<4; i++) {
        for (int j=0; j<4; j++) {
            GLKVector4 position = GLKVector4Make(cursorScenePosition.x+5, cursorScenePosition.y, cursorScenePosition.z+6, 1);
            position.x += -1.5 * distX + i*distX;
            position.z += -1.5 * distZ + j*distZ;

            
            AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:pointPlusCounter++];
            
            if (i==0 && j == 0) {
                p11 = point;
            } else if (i == 0 && j == 3) {
                p12 = point;
            }
            
            [sceneElementsArray addObject:point];
            [surfacePoints1 addObject:point];
        }
    }
    

    
    for (int i=0; i<4; i++) {
        for (int j=0; j<4; j++) {
            GLKVector4 position = GLKVector4Make(cursorScenePosition.x-4, cursorScenePosition.y, cursorScenePosition.z+4, 1);
            position.x += -1.5 * distX + i*distX;
            position.z += -1.5 * distZ + j*distZ;
            
            AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:pointPlusCounter++];
            
            if (i==3 && j == 3) {
                p21 = point;
            } else if (i == 3 && j == 0) {
                p22 = point;
            }
            
            [sceneElementsArray addObject:point];
            [surfacePoints2 addObject:point];
        }
    }

    
    
    for (int i=0; i<4; i++) {
        for (int j=0; j<4; j++) {
            GLKVector4 position = GLKVector4Make(cursorScenePosition.x, cursorScenePosition.y, cursorScenePosition.z-4, 1);
            position.x += -1.5 * distX + i*distX;
            position.z += -1.5 * distZ + j*distZ;
            
            AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:pointPlusCounter++];
            
            if (i==3 && j == 3) {
                p31 = point;
            } else if (i == 0 && j == 3) {
                p32 = point;
            }
            [sceneElementsArray addObject:point];
            [surfacePoints3 addObject:point];
        }
    }
    

    AKBezierSurface *surface1 = [[AKBezierSurface alloc]initWithPoints:surfacePoints1 screenWidth:m_viewWidth screenHeight:m_viewHeight number:0 onRoller:FALSE withWidth:1 withHeight:1 andMatrix:M];
    if(surface1 != nil)
        [sceneElementsArray addObject:surface1];
    AKBezierSurface *surface2 = [[AKBezierSurface alloc]initWithPoints:surfacePoints2 screenWidth:m_viewWidth screenHeight:m_viewHeight number:0 onRoller:FALSE withWidth:1 withHeight:1 andMatrix:M];
    if(surface2 != nil)
        [sceneElementsArray addObject:surface2];
    AKBezierSurface *surface3 = [[AKBezierSurface alloc]initWithPoints:surfacePoints3 screenWidth:m_viewWidth screenHeight:m_viewHeight number:0 onRoller:FALSE withWidth:1 withHeight:1 andMatrix:M];
    if(surface3 != nil)
        [sceneElementsArray addObject:surface3];
    
    GLKVector4 positionSum;
    positionSum.x = 0;
    positionSum.y = 0;
    positionSum.z = 0;
    positionSum.w = 0;
    
    [sceneElementsArray removeObject:p11];
    [sceneElementsArray removeObject:p31];
    NSMutableArray *selectedPoints1 = [[NSMutableArray alloc]init];
    [selectedPoints1 addObject:p11];
    [selectedPoints1 addObject:p31];
    positionSum = GLKVector4Add([p11 position], positionSum);
    positionSum = GLKVector4Add([p31 position], positionSum);
    positionSum = GLKVector4DivideScalar(positionSum, 2);
    AKPoint* newPoint = [[AKPoint alloc]initAtPosition:positionSum andNumber:101];
    
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        [obj replacePoints:selectedPoints1 withPoint:newPoint];
    }
    
    [sceneElementsArray addObject:newPoint];
    
    
    positionSum.x = 0;
    positionSum.y = 0;
    positionSum.z = 0;
    positionSum.w = 0;
    
    [sceneElementsArray removeObject:p12];
    [sceneElementsArray removeObject:p21];
    NSMutableArray *selectedPoints2 = [[NSMutableArray alloc]init];
    [selectedPoints2 addObject:p12];
    [selectedPoints2 addObject:p21];
    positionSum = GLKVector4Add([p12 position], positionSum);
    positionSum = GLKVector4Add([p21 position], positionSum);
    positionSum = GLKVector4DivideScalar(positionSum, 2);
    AKPoint* newPoint2 = [[AKPoint alloc]initAtPosition:positionSum andNumber:102];
    
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        [obj replacePoints:selectedPoints2 withPoint:newPoint2];
    }
    
    [sceneElementsArray addObject:newPoint2];
    
    
    positionSum.x = 0;
    positionSum.y = 0;
    positionSum.z = 0;
    positionSum.w = 0;
    
    [sceneElementsArray removeObject:p22];
    [sceneElementsArray removeObject:p32];
    NSMutableArray *selectedPoints3 = [[NSMutableArray alloc]init];
    [selectedPoints3 addObject:p22];
    [selectedPoints3 addObject:p32];
    positionSum = GLKVector4Add([p22 position], positionSum);
    positionSum = GLKVector4Add([p32 position], positionSum);
    positionSum = GLKVector4DivideScalar(positionSum, 2);
    AKPoint* newPoint3 = [[AKPoint alloc]initAtPosition:positionSum andNumber:103];
    
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        [obj replacePoints:selectedPoints3 withPoint:newPoint3];
    }
    
    [sceneElementsArray addObject:newPoint3];
    

    [self rebuildArrays];

}

- (void) addBezierSurfaceOnRoller:(BOOL)onRoller withWidth:(int)width andHeight:(int)height rollerHeight:(float)rollerHeight planeRollerRadiusWidth:(float)radiusWidth {
    NSMutableArray *surfacePoints = [[NSMutableArray alloc]init];

    if (onRoller) {
        float rH = rollerHeight/(height*4.0);
        for (int i=0; i<4+(width-1)*3; i++) {
            for (int j=0; j<4+(height-1)*3; j++) {
                
                GLKVector4 position = cursorScenePosition;
                position.x += sin(i/(width*3.0) * 2*M_PI)*radiusWidth;
                position.z += cos(i/(width*3.0) * 2*M_PI)*radiusWidth;
                position.y += rH*j;
                
                AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:(i*4+j)+self.pointsCount];
                [sceneElementsArray addObject:point];
                [surfacePoints addObject:point];
            }
        }
        
    } else {
        float distX = radiusWidth/(width*4);
        float distZ = rollerHeight/(height*4);
        for (int i=0; i<4+(width-1)*3; i++) {
            for (int j=0; j<4+(height-1)*3; j++) {
                GLKVector4 position = cursorScenePosition;
                position.x += -1.5 * distX + i*distX;
                position.z += -1.5 * distZ + j*distZ;
            
                AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:(i*4+j)+self.pointsCount];
                [sceneElementsArray addObject:point];
                [surfacePoints addObject:point];
            }
        }
    }
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrixL, transformationMatrix);

    AKBezierSurface *surface = [[AKBezierSurface alloc]initWithPoints:surfacePoints screenWidth:m_viewWidth screenHeight:m_viewHeight number:0 onRoller:onRoller withWidth:width withHeight:height andMatrix:M];
    if(surface != nil)
        [sceneElementsArray addObject:surface];
    [self rebuildArrays];
}

- (void) addBezierC2SurfaceOnRoller:(BOOL)onRoller withWidth:(int)width andHeight:(int)height rollerHeight:(float)rollerHeight planeRollerRadiusWidth:(float)radiusWidth {
    NSMutableArray *surfacePoints = [[NSMutableArray alloc]init];
    
    if (onRoller) {
        if(width < 3)
            return;
        
        
        float rH = rollerHeight/(height*4.0);
        for (int i=0; i<width; i++) {
            for (int j=0; j<3+height; j++) {
                
                GLKVector4 position = cursorScenePosition;
                position.x += sin(i/(float)width * 2*M_PI)*radiusWidth;
                position.z += cos(i/(float)width * 2*M_PI)*radiusWidth;
                position.y += rH*(j-(3+height)/2);
                
                AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:(i*4+j)+self.pointsCount];
                [sceneElementsArray addObject:point];
                [surfacePoints addObject:point];
            }
        }
        
        for (int i=0; i<3; i++) {
            for (int j=0; j<3+height; j++) {
                AKPoint *point = [surfacePoints objectAtIndex:i*(3+height)+j];
                [surfacePoints addObject:point];
            }
        }
        
    } else {
        float distX = radiusWidth/(width*4);
        float distZ = rollerHeight/(height*4);
        for (int i=0; i<3+width; i++) {
            for (int j=0; j<3+height; j++) {
                GLKVector4 position = cursorScenePosition;
                position.x += (i-2-width) * distX + i*distX;
                position.z += (j-2-height) * distZ + j*distZ;
                
                AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:(i*4+j)+self.pointsCount];
                [sceneElementsArray addObject:point];
                [surfacePoints addObject:point];
            }
        }
    }
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrixL, transformationMatrix);
    
    AKBezierC2Surface *surface = [[AKBezierC2Surface alloc]initWithPoints:surfacePoints screenWidth:m_viewWidth screenHeight:m_viewHeight withWidth:width withHeight:height andMatrix:M];
    if(surface != nil)
        [sceneElementsArray addObject:surface];
    [self rebuildArrays];
}

- (void) addUchwytWithWidth:(int)width andHeight:(int)height rollerHeight:(float)rollerHeight planeRollerRadiusWidth:(float)radiusWidth {
    NSMutableArray *surfacePoints = [[NSMutableArray alloc]init];

    float rH = rollerHeight/(height*4.0);
    for (int i=0; i<width; i++) {
        for (int j=0; j<3+height; j++) {
            
            GLKVector4 position = cursorScenePosition;
            position.x += sin((float)1/(2+height) * M_PI)/8;
            position.x -= sin((float)j/(2+height) * M_PI)/4;
            position.x += sin(i/(float)width * 2*M_PI)*radiusWidth;
            position.y += cos(i/(float)width * 2*M_PI)*radiusWidth;
            position.z += rH*(j-(3+height)/2);
            
            AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:(i*4+j)+self.pointsCount];
            //[sceneElementsArray addObject:point];
            [surfacePoints addObject:point];
        }
    }
    
    for (int i=0; i<3; i++) {
        for (int j=0; j<3+height; j++) {
            AKPoint *point = [surfacePoints objectAtIndex:i*(3+height)+j];
            [surfacePoints addObject:point];
        }
    }
    
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrixL, transformationMatrix);
    
    AKBezierC2Surface *surface = [[AKBezierC2Surface alloc]initWithPoints:surfacePoints screenWidth:m_viewWidth screenHeight:m_viewHeight withWidth:width withHeight:height andMatrix:M];
    if(surface != nil)
        [sceneElementsArray addObject:surface];
    [self rebuildArrays];
}

- (void) addTrzonWithWidth:(int)width andHeight:(int)height rollerHeight:(float)rollerHeight planeRollerRadiusWidth:(float)radiusWidth {
    NSMutableArray *surfacePoints = [[NSMutableArray alloc]init];
    [self moveCursorX:0 moveY:0 moveZ:(float)rollerHeight/((float)height)/10 ];
    float rH = rollerHeight/(height*4.0);
    for (int i=0; i<width; i++) {
        
        for (int j=0; j<2; j++) {
            
            GLKVector4 position = cursorScenePosition;
            position.x += sin(i/(float)width * 2*M_PI)*radiusWidth;
            position.y += cos(i/(float)width * 2*M_PI)*radiusWidth;
            position.z += rH*(j-(3+height)/2);
            
            AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:(i*4+j)+self.pointsCount];
            //[sceneElementsArray addObject:point];
            [surfacePoints addObject:point];
        }
        
        for (int j=2; j<6; j++) {
            
            GLKVector4 position = cursorScenePosition;
            position.x += sin(i/(float)width * 2*M_PI)*radiusWidth*2;
            position.y += cos(i/(float)width * 2*M_PI)*radiusWidth*2;
            position.z += rH*(2-(3+height)/2);
            
            AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:(i*4+j)+self.pointsCount];
            //[sceneElementsArray addObject:point];
            [surfacePoints addObject:point];
        }
        
        
        for (int j=6; j<3+height; j++) {
            
            GLKVector4 position = cursorScenePosition;
            position.x += sin(i/(float)width * 2*M_PI)*radiusWidth;
            position.y += cos(i/(float)width * 2*M_PI)*radiusWidth;
            position.z += rH*(j-3-(3+height)/2);
            
            AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:(i*4+j)+self.pointsCount];
            //[sceneElementsArray addObject:point];
            [surfacePoints addObject:point];
        }


    }
    
    for (int i=0; i<3; i++) {
        for (int j=0; j<3+height; j++) {
            AKPoint *point = [surfacePoints objectAtIndex:i*(3+height)+j];
            [surfacePoints addObject:point];
        }
    }
    
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrixL, transformationMatrix);
    
    AKBezierC2Surface *surface = [[AKBezierC2Surface alloc]initWithPoints:surfacePoints screenWidth:m_viewWidth screenHeight:m_viewHeight withWidth:width withHeight:height andMatrix:M];
    if(surface != nil)
        [sceneElementsArray addObject:surface];
    [self rebuildArrays];
}

- (void) addTopWithWidth:(int)width andHeight:(int)height rollerHeight:(float)rollerHeight planeRollerRadiusWidth:(float)radiusWidth {
    NSMutableArray *surfacePoints = [[NSMutableArray alloc]init];
    [self moveCursorX:0 moveY:0 moveZ:(float)rollerHeight/((float)height)*1.3];
    
    float rH = rollerHeight/(height*4.0);
    for (int i=0; i<width; i++) {
        for (int j=0; j<3+height; j++) {
            
            GLKVector4 position = cursorScenePosition;
            position.x += sin(i/(float)width * 2*M_PI)*radiusWidth*pow((float)j/(3+height),pow(1.0f/1.5f, j));
            position.y += cos(i/(float)width * 2*M_PI)*radiusWidth*pow((float)j/(3+height),pow(1.0f/1.5f, j));
            position.z += rH*(j-(3+height)/2);
            AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:(i*4+j)+self.pointsCount];
            //[sceneElementsArray addObject:point];
            [surfacePoints addObject:point];
        }
    }
    
    for (int i=0; i<3; i++) {
        for (int j=0; j<3+height; j++) {
            AKPoint *point = [surfacePoints objectAtIndex:i*(3+height)+j];
            [surfacePoints addObject:point];
        }
    }
    
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrixL, transformationMatrix);
    
    AKBezierC2Surface *surface = [[AKBezierC2Surface alloc]initWithPoints:surfacePoints screenWidth:m_viewWidth screenHeight:m_viewHeight withWidth:width withHeight:height andMatrix:M];
    if(surface != nil)
        [sceneElementsArray addObject:surface];
    [self rebuildArrays];
}

- (void) addKielich {
    [self addUchwytWithWidth:5 andHeight:5 rollerHeight:1.4 planeRollerRadiusWidth:0.05];
    [self addTrzonWithWidth:8 andHeight:8 rollerHeight:4 planeRollerRadiusWidth:0.1];
    [self addTopWithWidth:8 andHeight:5 rollerHeight:1 planeRollerRadiusWidth:0.3];
}

- (void) deleteSelectedObject {
    if (selectedPoint >= 0) {
        AKPoint *pointToDelete = [sceneElementsArray objectAtIndex:selectedPoint+1];
        [sceneElementsArray removeObjectAtIndex:(selectedPoint+1)];
        
        for (int i=0; i < [sceneElementsArray count]; i++) {
            AKObject *obj = [sceneElementsArray objectAtIndex:i];
            if ([obj isKindOfClass:[AKBezierCurve class]]) {
                AKBezierCurve *curve = (AKBezierCurve*)obj;
                [curve deletePoint:pointToDelete];
            }
            if ([obj isKindOfClass:[AKBezierC2Curve class]]) {
                AKBezierC2Curve *curve = (AKBezierC2Curve*)obj;
                [curve deletePoint:pointToDelete];
            }
            if ([obj isKindOfClass:[AKInterpolationCurve class]]) {
                AKInterpolationCurve *curve = (AKInterpolationCurve*)obj;
                [curve deletePoint:pointToDelete];
            }
        }
        
        selectedPoint = -1;
        [self rebuildArrays];
    } else if (selectedObject >= 0) {
        [sceneElementsArray removeObjectAtIndex:(selectedObject+1)];
        selectedObject = -1;
        [self rebuildArrays];
    }
}

- (void) findIntersection:(NSIndexSet*)selection {
    NSMutableArray *selectedSurfaces = [[NSMutableArray alloc]init];
    [selection enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        AKObject *obj = [sceneElementsArray objectAtIndex:idx+1];
        if ([obj isKindOfClass:[AKBezierC2Surface class]]) {
            [selectedSurfaces addObject:obj];
        }
    }];
    
    if ([selectedSurfaces count] != 2) {
        return;
    }
    
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrixL, transformationMatrix);
    
    AKTrimmingCurve *curve = [[AKTrimmingCurve alloc]initWithSurface1:[selectedSurfaces objectAtIndex:0] surface2:[selectedSurfaces objectAtIndex:1] number:0 andMatrix:M];
    //NSMutableArray *points = [curve getPoints];
    
    //[sceneElementsArray addObjectsFromArray:points];
    [sceneElementsArray addObject:curve];
    [self rebuildArrays];
}

- (void) deleteSelectedPointFromCurve:(NSIndexSet*)selection {
    NSMutableArray *selectedPoints = [[NSMutableArray alloc]init];
    NSMutableArray *selectedCurves = [[NSMutableArray alloc]init];
    
    [selection enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        AKObject *obj = [sceneElementsArray objectAtIndex:idx+1];
        if ([obj isKindOfClass:[AKPoint class]]) {
            [selectedPoints addObject:obj];
        } else if ([obj isKindOfClass:[AKBezierCurve class]]) {
            [selectedCurves addObject:obj];
        } else if ([obj isKindOfClass:[AKBezierC2Curve class]]) {
            [selectedCurves addObject:obj];
        } else if ([obj isKindOfClass:[AKInterpolationCurve class]]) {
            [selectedCurves addObject:obj];
        }
    }];
    
    if ([selectedPoints count] == 1 && [selectedCurves count] == 1) {
        AKPoint *point = [selectedPoints firstObject];
        if ([[selectedCurves firstObject] isKindOfClass:[AKBezierCurve class]]) {
            AKBezierCurve *curve = [selectedCurves firstObject];
            
            [curve deletePoint:point] ;
            [self rebuildArrays];
        } else if ([[selectedCurves firstObject] isKindOfClass:[AKBezierC2Curve class]]) {
            AKBezierC2Curve *curve = [selectedCurves firstObject];
            
            [curve deletePoint:point] ;
            [self rebuildArrays];
        } else if ([[selectedCurves firstObject] isKindOfClass:[AKInterpolationCurve class]]) {
            AKInterpolationCurve *curve = [selectedCurves firstObject];
            
            [curve deletePoint:point] ;
            [self rebuildArrays];
        }
    }
}

- (void) mergeSelectedPoints:(NSIndexSet*)selection {
    NSMutableArray *selectedPoints = [[NSMutableArray alloc]init];
    
    [selection enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        AKObject *obj = [sceneElementsArray objectAtIndex:idx+1];
        if ([obj isKindOfClass:[AKPoint class]]) {
            [selectedPoints addObject:obj];
        }
    }];
    if ([selectedPoints count] < 2) {
        return;
    }
    
    GLKVector4 positionSum;
    positionSum.x = 0;
    positionSum.y = 0;
    positionSum.z = 0;
    positionSum.w = 0;
    
    for (int i=0; i<[selectedPoints count]; i++) {
        AKPoint *point = selectedPoints[i];
        [sceneElementsArray removeObject:point];
        
        GLKVector4 position = [point position];
        positionSum = GLKVector4Add(position, positionSum);
    }
    
    positionSum = GLKVector4DivideScalar(positionSum, [selectedPoints count]);
    AKPoint* newPoint = [[AKPoint alloc]initAtPosition:positionSum andNumber:pointPlusCounter++];
    
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        [obj replacePoints:selectedPoints withPoint:newPoint];
    }
    
    [sceneElementsArray addObject:newPoint];
    [self rebuildArrays];
}

- (void) selectPointAtIndex:(int)index {
    if (index == -1) {
        selectedObject = -1;
        selectedPoint = -1;
        return;
    }
    
    AKObject* obj = [sceneElementsArray objectAtIndex:index+1];
    if ([obj isKindOfClass:[AKPoint class]]) {
        selectedPoint = index;
        selectedObject = -1;
    } else {
        selectedObject = index;
        selectedPoint = -1;
    }
}

- (int) selectedPoint {
    return selectedPoint;
}

- (int) selectedPointIndex {
    
    if (selectedPoint == -1) {
        return -1;
    }
    
    if (sceneBsplineMode) {
        int selectedPointIndex = -1;
        for (int i=0; i < [sceneElementsArray count]; i++) {
            AKObject *obj = [sceneElementsArray objectAtIndex:i];
            if ([obj isKindOfClass:[AKBezierC2Curve class]]) {
                AKBezierC2Curve *curve = (AKBezierC2Curve*)obj;
                for (int j=0; j<[curve.bsplineNodesInternalArray count]; j++) {
                    selectedPointIndex++;
                    AKPoint *point = [curve.bsplineNodesInternalArray objectAtIndex:j];
                    if (point == selectedAKPoint) {
                        if (self.pointsCount & 1) {
                            return selectedPointIndex + self.pointsCount+1;
                        }
                        return selectedPointIndex + self.pointsCount;
                    }
                }
            }
        }
    } else {
        int selectedPointIndex = -1;
        
        for (int i=0; i < [sceneElementsArray count]; i++) {
            AKObject *obj = [sceneElementsArray objectAtIndex:i];
            if ([obj isKindOfClass:[AKPoint class]]) {
                selectedPointIndex++;
            }
            if (i == selectedPoint+1) {
                break;
            }
        }
        
        return selectedPointIndex;
    }
    return -1;
}

- (void) addSelectedPointsToSelectedCurve:(NSIndexSet*)selection {
    NSMutableArray *selectedPoints = [[NSMutableArray alloc]init];
    NSMutableArray *selectedCurves = [[NSMutableArray alloc]init];
    
    [selection enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        AKObject *obj = [sceneElementsArray objectAtIndex:idx+1];
        if ([obj isKindOfClass:[AKPoint class]]) {
            [selectedPoints addObject:obj];
        } else if ([obj isKindOfClass:[AKBezierCurve class]]) {
            [selectedCurves addObject:obj];
        } else if ([obj isKindOfClass:[AKBezierC2Curve class]]) {
            [selectedCurves addObject:obj];
        } else if ([obj isKindOfClass:[AKInterpolationCurve class]]) {
            [selectedCurves addObject:obj];
        }
    }];
    
    if ([selectedPoints count] > 0 && [selectedCurves count] == 1) {
        if ([[selectedCurves firstObject]isKindOfClass:[AKBezierCurve class]]) {
            AKBezierCurve *curve = [selectedCurves firstObject];
            
            [curve addPointsToCurve:selectedPoints];
            [self rebuildArrays];
        } else if ([[selectedCurves firstObject]isKindOfClass:[AKBezierC2Curve class]]) {
            AKBezierC2Curve *curve = [selectedCurves firstObject];
            
            [curve addPointsToCurve:selectedPoints];
            [self rebuildArrays];
        } else if ([[selectedCurves firstObject]isKindOfClass:[AKInterpolationCurve class]]) {
            AKInterpolationCurve *curve = [selectedCurves firstObject];
            
            [curve addPointsToCurve:selectedPoints];
            [self rebuildArrays];
        }
    }
    
}

- (void) setBezierCheckbox:(BOOL)enabled {
    displayLamana = enabled;
    [self rebuildArrays];
}

- (int) selectPointAtPosition:(NSPoint)position withBsplineMode:(BOOL)bsplineMode {

    sceneBsplineMode = bsplineMode;
    GLKVector4 selectedPointPosition;
    if (!bsplineMode) {
        int selectIndex = -1;
        float minDistance = MAXFLOAT;
        
        for (int i=0; i < [sceneElementsArray count]; i++) {
            AKObject *obj = [sceneElementsArray objectAtIndex:i];
            if ([obj isKindOfClass:[AKPoint class]]) {
                AKPoint *point = (AKPoint *)obj;
                GLKVector4 pointPosition = [point position];
                GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrix, transformationMatrix);
                GLKVector4 pointScreenPosition = GLKMatrix4MultiplyVector4(M, pointPosition);
                pointScreenPosition = GLKVector4DivideScalar(pointScreenPosition, pointScreenPosition.w);
                float distance = sqrtf((pointScreenPosition.x - position.x)*(pointScreenPosition.x - position.x) +
                                       (pointScreenPosition.y - position.y)*(pointScreenPosition.y - position.y));
                if (distance < minDistance) {
                    selectedPointPosition = pointPosition;
                    minDistance = distance;
                    selectIndex = i;
                }
            }
        }
        if (minDistance < selectionMouseEpsilon) {
            cursorScenePosition = selectedPointPosition;
            cursorTransformationMatrix = GLKMatrix4Identity;
            cursorTransformationMatrix.m30 = selectedPointPosition.x;
            cursorTransformationMatrix.m31 = selectedPointPosition.y;
            cursorTransformationMatrix.m32 = selectedPointPosition.z;
            return selectIndex-1;
        }
    } else {
        AKPoint *minSelectedPoint;
        int selectIndex = -1;
        float minDistance = MAXFLOAT;
        for (int i=0; i < [sceneElementsArray count]; i++) {
            AKObject *obj = [sceneElementsArray objectAtIndex:i];
            if ([obj isKindOfClass:[AKBezierC2Curve class]]) {
                AKBezierC2Curve *curve = (AKBezierC2Curve *)obj;
                
                for (int j=0; j< [curve.bsplineNodesInternalArray count]; j++) {
                    AKPoint *point = [curve.bsplineNodesInternalArray objectAtIndex:j];
                    GLKVector4 pointPosition = [point position];
                    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrix, transformationMatrix);
                    GLKVector4 pointScreenPosition = GLKMatrix4MultiplyVector4(M, pointPosition);
                    pointScreenPosition = GLKVector4DivideScalar(pointScreenPosition, pointScreenPosition.w);
                    float distance = sqrtf((pointScreenPosition.x - position.x)*(pointScreenPosition.x - position.x) +
                                           (pointScreenPosition.y - position.y)*(pointScreenPosition.y - position.y));
                    if (distance < minDistance) {
                        minDistance = distance;
                        selectIndex = i;
                        minSelectedPoint = point;
                    }
                }
            }
        }
        
        if (minDistance < selectionMouseEpsilon) {
            selectedAKPoint = minSelectedPoint;
            cursorScenePosition = [selectedAKPoint position];
            cursorTransformationMatrix = GLKMatrix4Identity;
            cursorTransformationMatrix.m30 = cursorScenePosition.x;
            cursorTransformationMatrix.m31 = cursorScenePosition.y;
            cursorTransformationMatrix.m32 = cursorScenePosition.z;
            return 0;
        }
    }
    selectedAKPoint = nil;
    return -1;
}

- (int) selectPointAtCursorPosition {
    int selectIndex = -1;
    float minDistance = MAXFLOAT;
    
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKPoint class]]) {
            AKPoint *point = (AKPoint *)obj;
            GLKVector4 pointPosition = [point position];
            pointPosition = GLKVector4DivideScalar(pointPosition, pointPosition.w);
            float distance = sqrtf((pointPosition.x - cursorScenePosition.x)*(pointPosition.x - cursorScenePosition.x) +
                                   (pointPosition.y - cursorScenePosition.y)*(pointPosition.y - cursorScenePosition.y) +
                                   (pointPosition.z - cursorScenePosition.z)*(pointPosition.z - cursorScenePosition.z));
            if (distance < minDistance) {
                minDistance = distance;
                selectIndex = i;
            }
        }
    }
    
    if (minDistance < selectionCursorEpsilon) {
        selectedPoint = selectIndex - 1;
        return selectIndex;
    }
    
    
    return -1;
}

- (void) deselectPoint {
    selectedPoint = -1;
}

- (void) setElementName:(NSString*)name atIndex:(int)index {
    AKObject *element = [sceneElementsArray objectAtIndex:index+1];
    [element setElementName:name];
}

- (void) copySceneToVertexArray:(GLKVector4**)vertexArray
                       withSize:(int*)vertexArraySize
                andIndicesArray:(indic**)indicArray
                       withSize:(int*)indicArraySize {
    
    GLKMatrix4 MCross = GLKMatrix4Multiply(transformationMatrix, cursorTransformationMatrix);
    MCross = GLKMatrix4Multiply(projectionMatrix, MCross);
    for (int i=0; i<6; i++) {
        transformArray[i] = GLKMatrix4MultiplyVector4(MCross, array[i]);
    }
    
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrix, transformationMatrix);
    for (int i=6; i < arraySize; i++) {
        transformArray[i] = GLKMatrix4MultiplyVector4(M, array[i]);
    }
    

    /*
    NSLog(@"---------------------");
    for (int i=0; i < arraySize; i++) {
        NSLog(@"%d:(%.4f, %.4f, %.4f, %.4f)",i, transformArray[i].x, transformArray[i].y, transformArray[i].z, transformArray[i].w);
    }
    NSLog(@"----------------------");
    
    
    NSLog(@"---------------------");
    for (int i=0; i<3; i++) {
        NSLog(@"%d - %d", indices[i].from, indices[i].to);

    }
    for (int i=3; i < (self.pointsCount+1)/2+3; i++) {
        NSLog(@"%d", indices[i].from);
        NSLog(@"%d", indices[i].to);
    }
    for (int i = (self.pointsCount+1)/2+3; i < (self.bsplinePointsCount+1)/2 + (self.pointsCount+1)/2+3; i++) {
        NSLog(@"%d", indices[i].from);
        NSLog(@"%d", indices[i].to);
    }
    
    
    for (int i = (self.bsplinePointsCount+1)/2 + (self.pointsCount+1)/2+3; i < indicSize; i++) {
        NSLog(@"%d: %d - %d",i - ((self.bsplinePointsCount+1)/2 + (self.pointsCount+1)/2+3) ,indices[i].from, indices[i].to);

    }
    NSLog(@"----------------------");
    */
    
    *vertexArray = transformArray;
    *indicArray = indices;
    *vertexArraySize = arraySize;
    *indicArraySize = indicSize;
    
    cursorScreenPostion = GLKMatrix4MultiplyVector4(M, cursorScenePosition);
    cursorScreenPostion = GLKVector4DivideScalar(cursorScreenPostion, cursorScreenPostion.w);
}

- (void) copyLeftEyeSceneSceneToVertexArray:(GLKVector4**)vertexArray
                                   withSize:(int*)vertexArraySize
                            andIndicesArray:(indic**)indicArray
                                   withSize:(int*)indicArraySize {
    GLKMatrix4 MCross = GLKMatrix4Multiply(transformationMatrix, cursorTransformationMatrix);
    MCross = GLKMatrix4Multiply(projectionMatrixL, MCross);
    for (int i=0; i<6; i++) {
        transformArray[i] = GLKMatrix4MultiplyVector4(MCross, array[i]);
    }
    
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrixL, transformationMatrix);
    for (int i=6; i < arraySize; i++) {
        transformArray[i] = GLKMatrix4MultiplyVector4(M, array[i]);
    }
    
    *vertexArray = transformArray;
    *indicArray = indices;
    *vertexArraySize = arraySize;
    *indicArraySize = indicSize;
    
    GLKMatrix4 Mcursor = GLKMatrix4Multiply(projectionMatrix, transformationMatrix);
    cursorScreenPostion = GLKMatrix4MultiplyVector4(Mcursor, cursorScenePosition);
    cursorScreenPostion = GLKVector4DivideScalar(cursorScreenPostion, cursorScreenPostion.w);
}

- (void) copyRightEyeSceneToVertexArray:(GLKVector4**)vertexArray
                               withSize:(int*)vertexArraySize
                        andIndicesArray:(indic**)indicArray
                               withSize:(int*)indicArraySize {
    
    GLKMatrix4 MCross = GLKMatrix4Multiply(transformationMatrix, cursorTransformationMatrix);
    MCross = GLKMatrix4Multiply(projectionMatrixR, MCross);
    for (int i=0; i<6; i++) {
        transformArray[i] = GLKMatrix4MultiplyVector4(MCross, array[i]);
    }
    
    GLKMatrix4 M = GLKMatrix4Multiply(projectionMatrixR, transformationMatrix);
    for (int i=6; i < arraySize; i++) {
        transformArray[i] = GLKMatrix4MultiplyVector4(M, array[i]);
    }
    
    *vertexArray = transformArray;
    *indicArray = indices;
    *vertexArraySize = arraySize;
    *indicArraySize = indicSize;
}

- (NSString *)sceneElementAtIndex:(NSInteger)index {
    AKObject *element = [sceneElementsArray objectAtIndex:(index+1)];
    return [element elementName];
}

- (NSInteger)sceneElementsCount {
    return [sceneElementsArray count] - 1;
}

- (NSString*)cursorScenePositionString {
    return [NSString stringWithFormat:@"(%.3f, %.3f, %.3f)", cursorScenePosition.x, cursorScenePosition.y, cursorScenePosition.z];
}

- (NSString*)cursorScreenPositionString {
    return [NSString stringWithFormat:@"(%f, %f)", ((cursorScreenPostion.x+1.0)/2.0)*m_viewWidth, ((cursorScreenPostion.y+1.0)/2.0)*m_viewHeight];
}

- (void) setBsplineMode: (bool)bsplineEnabled {
    sceneBsplineMode = bsplineEnabled;
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKBezierC2Curve class]]) {
            AKBezierC2Curve *curve = (AKBezierC2Curve *)obj;
            [curve setBsplineModeWithBool:bsplineEnabled];
        }
    }
}

- (void) createHeightMapFromAllSurfaces {
    
    unsigned int size = 600;
    
    float **heightMap = malloc(size*sizeof(float*));
    GLKVector4 **normalMap = malloc(size*sizeof(GLKVector4*));
    for (int i=0; i<size; i++) {
        heightMap[i] = malloc(size*sizeof(float));
        normalMap[i] = malloc(size*sizeof(GLKVector4));
        for (int j=0; j<size; j++) {
            heightMap[i][j] = 0;
            normalMap[i][j] = GLKVector4Make(0, 0, 1, 0);
        }
    }
    
    
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKBezierC2Surface class]]) {
            AKBezierC2Surface *surface = (AKBezierC2Surface *)obj;
            [surface applySurfaceToHeightMap:heightMap normalMap:normalMap withSize:size];
        }
    }
    NSLog(@"Created heightmap");
    
    float**firstHeightMap = [self getNewHeightMapForMill:10*size/150.0f isBall:TRUE fromHeightMap:heightMap withSize:size];
    NSLog(@"Created heightmap for 1. mill");
    float**secondHeightMap = [self getNewHeightMapForMill:5*size/150.0f isBall:FALSE fromHeightMap:heightMap withSize:size];
    NSLog(@"Created heightmap for 2. mill");
    float**thirdHeightMap = [self getNewHeightMapForMill:6*size/150.0f isBall:FALSE fromHeightMap:heightMap withSize:size];
    NSLog(@"Created heightmap for 3. mill");
    float**fourthHeightMap = [self getNewHeightMapForMill:4*size/150.0f isBall:TRUE fromHeightMap:heightMap withSize:size];
    NSLog(@"Created heightmap for 4. mill");
    
    NSString *firstPass = [self generateFirstPassForFirstHeightMap:firstHeightMap withSize:size];
    NSLog(@"Generated 1. pass");
    [firstPass writeToFile:@"frez1.k16.txt" atomically:NO encoding:NSASCIIStringEncoding error:nil];
    
    NSString *secondPass = [self generateSecondPassForSecondHeightMap:secondHeightMap withSize:size];
    [secondPass writeToFile:@"frez2.f10.txt" atomically:NO encoding:NSASCIIStringEncoding error:nil];
    NSLog(@"Generated 2. pass");
    
    NSString *thirdPass = [self generateThirdPassForThirdHeightMap:thirdHeightMap withSize:size];
    [thirdPass writeToFile:@"frez3.f12.txt" atomically:NO encoding:NSASCIIStringEncoding error:nil];
    NSLog(@"Generated 3. pass");
    
    NSString *fourthPass = [self generateFourthPassForFourtHeightMap:fourthHeightMap withNormalMap:normalMap withSize:size];
    [fourthPass writeToFile:@"frez4.k08.txt" atomically:NO encoding:NSASCIIStringEncoding error:nil];
    NSLog(@"Generated 4. pass");
    
    
    for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++) {
            printf("%5.2f ", heightMap[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    printf("\n");
    
    for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++) {
            printf("%5.2f ", firstHeightMap[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    printf("\n");
    
    for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++) {
            printf("%5.2f ", secondHeightMap[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    printf("\n");
    
    for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++) {
            printf("%5.2f ", fourthHeightMap[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    
    /*
    NSBitmapImageRep *heightMapBitmap = [[NSBitmapImageRep alloc] initWithBitmapDataPlanes:nil
                                                                                pixelsWide:size
                                                                                pixelsHigh:size
                                                                             bitsPerSample:8
                                                                           samplesPerPixel:4
                                                                                  hasAlpha:YES
                                                                                  isPlanar:NO
                                                                            colorSpaceName:NSCalibratedRGBColorSpace
                                                                              bitmapFormat:0
                                                                               bytesPerRow:(4 * size)
                                                                              bitsPerPixel:32];
    NSBitmapImageRep *normalMapBitmap = [[NSBitmapImageRep alloc] initWithBitmapDataPlanes:nil
                                                                                pixelsWide:size
                                                                                pixelsHigh:size
                                                                             bitsPerSample:8
                                                                           samplesPerPixel:4
                                                                                  hasAlpha:YES
                                                                                  isPlanar:NO
                                                                            colorSpaceName:NSCalibratedRGBColorSpace
                                                                              bitmapFormat:0
                                                                               bytesPerRow:(4 * size)
                                                                              bitsPerPixel:32];
    NSImage *heightMapImage = [[NSImage alloc] initWithSize:NSMakeSize(size, size)];
    NSImage *normalMapImage = [[NSImage alloc] initWithSize:NSMakeSize(size, size)];
    
    [heightMapImage removeRepresentation:heightMapBitmap];
    [normalMapImage removeRepresentation:normalMapBitmap];
    
    unsigned char *heightData = [heightMapBitmap bitmapData];
    unsigned char *normalData = [normalMapBitmap bitmapData];
    
    
    for (int i=0; i<size; i++) {
        for (int j=0; j<size*4; j+=4) {
            heightData[i*size*4+j+3] = 255;
            normalData[i*size*4+j+0] = 0;
            normalData[i*size*4+j+1] = 0;
            normalData[i*size*4+j+2] = 0;
            normalData[i*size*4+j+3] = 0;
        }
    }
    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKBezierC2Surface class]]) {
            AKBezierC2Surface *surface = (AKBezierC2Surface *)obj;
            [surface applySurfaceToHeightMap:heightData withSize:size andToNormalMap:normalData];
        }
    }
    [heightMapImage addRepresentation:heightMapBitmap];
    [normalMapImage addRepresentation:normalMapBitmap];
    
    NSData *heightNSData = [heightMapBitmap representationUsingType: NSBMPFileType properties: nil];
    NSData *normalNSData = [normalMapBitmap representationUsingType: NSBMPFileType properties: nil];
    [heightNSData writeToFile: @"heightmap.bmp" atomically: NO];
    [normalNSData writeToFile: @"normalmap.bmp" atomically: NO];
     */
}

- (NSString *) generateFirstPassForFirstHeightMap:(float**)heightMap withSize:(unsigned int)size {
    NSMutableArray *positions = [[NSMutableArray alloc]init];
    
    
    float offset = 20-8;
    float currentHeight = 36;
    float step =0.3;
    GLKVector3 currentPosition;
    GLKVector3 currentDirection;
    GLKVector3 tempPos;
    float stepDeep = 8;
    float stepRight = 8;
    
    currentPosition = GLKVector3Make(0, 0, 80);
    [self addVector:currentPosition toArray:positions];
    float lastHeight = 0;
    float lastCurrentHeight = 0;
    while (currentHeight >= 10 ) {
        
        currentPosition = GLKVector3Make(-75, -90, 80);
        [self addVector:currentPosition toArray:positions];
        
        currentPosition = GLKVector3Make(-75, -90, offset+currentHeight);
        [self addVector:currentPosition toArray:positions];
        
        currentPosition = GLKVector3Make(-75, -74, offset+currentHeight);
        [self addVector:currentPosition toArray:positions];
        
        currentDirection = GLKVector3Make(0, step, 0);

        while (currentPosition.x < 80) {
            GLKVector3 newPosition = GLKVector3Add(currentPosition, currentDirection);
            if (newPosition.y >= 0 || [self getHeightFromHeightMap:heightMap withSize:size fromPosition:newPosition]+offset > currentHeight+offset) {
                if ([self getHeightFromHeightMap:heightMap withSize:size fromPosition:currentPosition]+offset <= currentHeight+offset) {
                    [self addVector:currentPosition toArray:positions];
                    if ([self getHeightFromHeightMap:heightMap withSize:size fromPosition:newPosition]+offset <= currentHeight+offset) {
                        currentPosition = newPosition;
                    }
                }
                float yOffset = step * 3;
                if (newPosition.y >= 0) {
                    yOffset = -step;
                }
                while (yOffset+currentPosition.y >= -75) {
                    newPosition = GLKVector3Make(currentPosition.x+stepRight, currentPosition.y+yOffset, currentPosition.z);
                    if ([self isPathClearFromPosition:currentPosition toPosition:newPosition onHeight:currentHeight withHeightMap:heightMap withSize:size]) {
                        break;
                    }
                    
                    yOffset--;
                }
                if (yOffset+currentPosition.y < -75) {
                    // coś nie halo, panic mode!
                    NSLog(@"PANIC MODE!");
                    currentPosition = GLKVector3Make(currentPosition.x, -75, offset+currentHeight);
                    [self addVector:currentPosition toArray:positions];
                    
                    currentPosition = GLKVector3Make(currentPosition.x+stepRight, -75, offset+currentHeight);
                    [self addVector:currentPosition toArray:positions];
                    currentDirection = GLKVector3Make(0, step, 0);
                } else {
                    currentPosition = newPosition;
                    [self addVector:currentPosition toArray:positions];
                    currentDirection = GLKVector3Make(0, -step, 0);
                }
            } else if (newPosition.y <= -75) {
                currentPosition = newPosition;
                [self addVector:currentPosition toArray:positions];
                
                newPosition = currentPosition = GLKVector3Make(currentPosition.x+stepRight, -75, offset+currentHeight);
                [self addVector:currentPosition toArray:positions];
                currentDirection = GLKVector3Make(0, step, 0);
            }
            currentPosition = newPosition;
        }
        currentPosition = GLKVector3Make(currentPosition.x, currentPosition.y, 80);
        [self addVector:currentPosition toArray:positions];
        
        currentPosition = GLKVector3Make(-75, -90, 80);
        [self addVector:currentPosition toArray:positions];
        
        currentPosition = GLKVector3Make(-75, 90, offset+currentHeight);
        [self addVector:currentPosition toArray:positions];
        
        currentPosition = GLKVector3Make(-75, 74, offset+currentHeight);
        [self addVector:currentPosition toArray:positions];
        currentDirection = GLKVector3Make(0, -step, 0);
        
        while (currentPosition.x < 80) {
            GLKVector3 newPosition = GLKVector3Add(currentPosition, currentDirection);
            if (newPosition.y <= 0 || [self getHeightFromHeightMap:heightMap withSize:size fromPosition:newPosition]+offset > currentHeight+offset) {
                if ([self getHeightFromHeightMap:heightMap withSize:size fromPosition:currentPosition]+offset <= currentHeight+offset) {
                    [self addVector:currentPosition toArray:positions];
                    if ([self getHeightFromHeightMap:heightMap withSize:size fromPosition:newPosition]+offset <= currentHeight+offset) {
                        currentPosition = newPosition;
                    }
                }
                float yOffset = -step * 3;
                if (newPosition.y <= 0) {
                    yOffset = step;
                }
                while (yOffset+currentPosition.y <= 75) {
                    newPosition = GLKVector3Make(currentPosition.x+stepRight, currentPosition.y+yOffset, currentPosition.z);
                    if ([self isPathClearFromPosition:currentPosition toPosition:newPosition onHeight:currentHeight withHeightMap:heightMap withSize:size]) {
                        break;
                    }
                    
                    yOffset++;
                }
                if (yOffset+currentPosition.y > 75) {
                    // coś nie halo, panic mode!
                    NSLog(@"PANIC MODE!");
                    currentPosition = GLKVector3Make(currentPosition.x, 75, offset+currentHeight);
                    [self addVector:currentPosition toArray:positions];
                    
                    currentPosition = GLKVector3Make(currentPosition.x+stepRight, 75, offset+currentHeight);
                    [self addVector:currentPosition toArray:positions];
                    currentDirection = GLKVector3Make(0, -step, 0);
                } else {
                    currentPosition = newPosition;
                    [self addVector:currentPosition toArray:positions];
                    currentDirection = GLKVector3Make(0, step, 0);
                }
            } else if (newPosition.y >= 75) {
                currentPosition = newPosition;
                [self addVector:currentPosition toArray:positions];
                
                newPosition = currentPosition = GLKVector3Make(currentPosition.x+stepRight, 75, offset+currentHeight);
                [self addVector:currentPosition toArray:positions];
                currentDirection = GLKVector3Make(0, -step, 0);
            }
            currentPosition = newPosition;
        }
        

        
        currentPosition = GLKVector3Make(75, 0, currentPosition.z);
        tempPos = GLKVector3Make(75, 0, currentPosition.z);
        while ([self isPathClearFromPosition:currentPosition toPosition:tempPos onHeight:currentHeight withHeightMap:heightMap withSize:size] && tempPos.x > 40) {
            tempPos.x -= step;
        }
        if (tempPos.x > 40) {
            tempPos.x += step;
            
            currentPosition = GLKVector3Make(currentPosition.x, currentPosition.y, 80);
            [self addVector:currentPosition toArray:positions];
            
            currentPosition = GLKVector3Make(tempPos.x, -75, 80);
            [self addVector:currentPosition toArray:positions];
            
            currentPosition = GLKVector3Make(tempPos.x, -75, offset+currentHeight);
            [self addVector:currentPosition toArray:positions];
            
            currentPosition = GLKVector3Make(tempPos.x, 75, offset+currentHeight);
            [self addVector:currentPosition toArray:positions];
        }
        
        lastCurrentHeight = currentHeight;

        currentHeight -= stepDeep;
        lastHeight = currentPosition.z;
        currentPosition = GLKVector3Make(currentPosition.x, currentPosition.y, 80);
        [self addVector:currentPosition toArray:positions];
        currentDirection = GLKVector3Make(0, step, 0);
        
        if (currentHeight < 7 && currentHeight > 2) {
            currentHeight = 10;
        }
    }
    
    currentPosition = GLKVector3Make(0, 0, 80);
    [self addVector:currentPosition toArray:positions];
    
    NSMutableString *path = [[NSMutableString alloc]init];
    GLKVector3 pos;
    int i=1;
    for (id position in positions) {
        NSValue *value = position;
        [value getValue:&pos];
        [path appendString:[NSString stringWithFormat:@"N%dX%.3fY%.3fZ%.3f\n", i++, pos.x, pos.y, pos.z]];
    }
    return path;
}

- (NSString *) generateSecondPassForSecondHeightMap:(float**)heightMap withSize:(unsigned int)size {
    NSMutableArray *positions = [[NSMutableArray alloc]init];

    float offset = 20;
    float currentHeight = 0;
    GLKVector3 currentPosition;
    
    currentPosition = GLKVector3Make(0, 0, 80);
    [self addVector:currentPosition toArray:positions];
    
    while (currentHeight >= 0 ) {
        
        currentPosition = GLKVector3Make(-90, 0, 80);
        [self addVector:currentPosition toArray:positions];
        
        currentPosition = GLKVector3Make(-90, 0, offset+currentHeight);
        [self addVector:currentPosition toArray:positions];
        
        float posx = -75;
        for (int i=0; i<size; i++) {
            posx = ((2*(float)i/(float)size)-1)*75;
            int j = -(int)size/2;
            while (j<=0) {
                if (heightMap[i][j+size/2]>0) {
                    break;
                }
                j++;
            }
            if (j>0) {
                j=0;
            }
            float posy = (2*(float)j/(float)size)*75;
            currentPosition = GLKVector3Make(posx, posy, offset+currentHeight);
            [self addVector:currentPosition toArray:positions];

        }

        currentPosition = GLKVector3Make(currentPosition.x, currentPosition.y, 80);
        [self addVector:currentPosition toArray:positions];

        currentPosition = GLKVector3Make(-90, 0, 80);
        [self addVector:currentPosition toArray:positions];
        
        currentPosition = GLKVector3Make(-90, 0, offset+currentHeight);
        [self addVector:currentPosition toArray:positions];
        
        posx = -75;
        for (int i=0; i<size; i++) {
            posx = ((2*(float)i/(float)size)-1)*75;
            int j = (int)size/2-1;
            while (j>=0) {
                if (heightMap[i][j+size/2]>0) {
                    break;
                }
                j--;
            }
            if (j<0) {
                j=0;
            }
            float posy = (2*(float)j/(float)size)*75;
            currentPosition = GLKVector3Make(posx, posy, offset+currentHeight);
            [self addVector:currentPosition toArray:positions];
            
        }
        currentPosition = GLKVector3Make(currentPosition.x, currentPosition.y, 80);
        [self addVector:currentPosition toArray:positions];
        
        
        currentHeight -= 10;
    }
    
    
    
    

    NSMutableString *path = [[NSMutableString alloc]init];
    GLKVector3 pos;
    int i=1;
    for (id position in positions) {
        NSValue *value = position;
        [value getValue:&pos];
        [path appendString:[NSString stringWithFormat:@"N%dX%.3fY%.3fZ%.3f\n", i++, pos.x, pos.y, pos.z]];
    }
    return path;
}

- (NSString *) generateThirdPassForThirdHeightMap:(float**)heightMap withSize:(unsigned int)size {
    NSMutableArray *positions = [[NSMutableArray alloc]init];
    
    float offset = 20;
    float currentHeight = 0;
    GLKVector3 currentPosition;
    GLKVector3 currentDirection;
    float step = 0.5;
    float stepRight = 11;

    currentPosition = GLKVector3Make(-75, -90, 80);
    [self addVector:currentPosition toArray:positions];
    
    currentPosition = GLKVector3Make(-75, -90, offset+currentHeight);
    [self addVector:currentPosition toArray:positions];
    
    currentPosition = GLKVector3Make(-75, -74, offset+currentHeight);
    [self addVector:currentPosition toArray:positions];
    
    currentDirection = GLKVector3Make(0, step, 0);
    
    while (currentPosition.x < 80) {
        GLKVector3 newPosition = GLKVector3Add(currentPosition, currentDirection);
        if (newPosition.y >= 0 || [self getHeightFromHeightMap:heightMap withSize:size fromPosition:newPosition]+offset > currentHeight+offset) {
            if ([self getHeightFromHeightMap:heightMap withSize:size fromPosition:currentPosition]+offset <= currentHeight+offset) {
                [self addVector:currentPosition toArray:positions];
                if ([self getHeightFromHeightMap:heightMap withSize:size fromPosition:newPosition]+offset <= currentHeight+offset) {
                    currentPosition = newPosition;
                }
            }
            float yOffset = step;
            if (newPosition.y >= 0) {
                yOffset = -step;
            }
            while (yOffset+currentPosition.y >= -75) {
                newPosition = GLKVector3Make(currentPosition.x+stepRight, currentPosition.y+yOffset, currentPosition.z);
                if ([self isPathClearFromPosition:currentPosition toPosition:newPosition onHeight:currentHeight withHeightMap:heightMap withSize:size]) {
                    break;
                }
                
                yOffset--;
            }
            if (yOffset+currentPosition.y < -75) {
                // coś nie halo, panic mode!
                NSLog(@"PANIC MODE!");
                currentPosition = GLKVector3Make(currentPosition.x, -75, offset+currentHeight);
                [self addVector:currentPosition toArray:positions];
                
                currentPosition = GLKVector3Make(currentPosition.x+stepRight, -75, offset+currentHeight);
                [self addVector:currentPosition toArray:positions];
                currentDirection = GLKVector3Make(0, step, 0);
            } else {
                currentPosition = newPosition;
                [self addVector:currentPosition toArray:positions];
                currentDirection = GLKVector3Make(0, -step, 0);
            }
        } else if (newPosition.y <= -75) {
            currentPosition = newPosition;
            [self addVector:currentPosition toArray:positions];
            
            newPosition = currentPosition = GLKVector3Make(currentPosition.x+stepRight, -75, offset+currentHeight);
            [self addVector:currentPosition toArray:positions];
            currentDirection = GLKVector3Make(0, step, 0);
        }
        currentPosition = newPosition;
    }
    currentPosition = GLKVector3Make(currentPosition.x, currentPosition.y, 80);
    [self addVector:currentPosition toArray:positions];
    
    currentPosition = GLKVector3Make(-75, -90, 80);
    [self addVector:currentPosition toArray:positions];
    
    currentPosition = GLKVector3Make(-75, 90, offset+currentHeight);
    [self addVector:currentPosition toArray:positions];
    
    currentPosition = GLKVector3Make(-75, 74, offset+currentHeight);
    [self addVector:currentPosition toArray:positions];
    currentDirection = GLKVector3Make(0, -step, 0);
    
    while (currentPosition.x < 80) {
        GLKVector3 newPosition = GLKVector3Add(currentPosition, currentDirection);
        if (newPosition.y <= 0 || [self getHeightFromHeightMap:heightMap withSize:size fromPosition:newPosition]+offset > currentHeight+offset) {
            if ([self getHeightFromHeightMap:heightMap withSize:size fromPosition:currentPosition]+offset <= currentHeight+offset) {
                [self addVector:currentPosition toArray:positions];
                if ([self getHeightFromHeightMap:heightMap withSize:size fromPosition:newPosition]+offset <= currentHeight+offset) {
                    currentPosition = newPosition;
                }
            }
            float yOffset = -step * 3;
            if (newPosition.y <= 0) {
                yOffset = step;
            }
            while (yOffset+currentPosition.y <= 75) {
                newPosition = GLKVector3Make(currentPosition.x+stepRight, currentPosition.y+yOffset, currentPosition.z);
                if ([self isPathClearFromPosition:currentPosition toPosition:newPosition onHeight:currentHeight withHeightMap:heightMap withSize:size]) {
                    break;
                }
                
                yOffset++;
            }
            if (yOffset+currentPosition.y > 75) {
                // coś nie halo, panic mode!
                NSLog(@"PANIC MODE!");
                currentPosition = GLKVector3Make(currentPosition.x, 75, offset+currentHeight);
                [self addVector:currentPosition toArray:positions];
                
                currentPosition = GLKVector3Make(currentPosition.x+stepRight, 75, offset+currentHeight);
                [self addVector:currentPosition toArray:positions];
                currentDirection = GLKVector3Make(0, -step, 0);
            } else {
                currentPosition = newPosition;
                [self addVector:currentPosition toArray:positions];
                currentDirection = GLKVector3Make(0, step, 0);
            }
        } else if (newPosition.y >= 75) {
            currentPosition = newPosition;
            [self addVector:currentPosition toArray:positions];
            
            newPosition = currentPosition = GLKVector3Make(currentPosition.x+stepRight, 75, offset+currentHeight);
            [self addVector:currentPosition toArray:positions];
            currentDirection = GLKVector3Make(0, -step, 0);
        }
        currentPosition = newPosition;
    }
    
    currentPosition = GLKVector3Make(currentPosition.x, currentPosition.y, 80);
    [self addVector:currentPosition toArray:positions];
    
    currentPosition = GLKVector3Make(0, 0, 80);
    [self addVector:currentPosition toArray:positions];


    
    NSMutableString *path = [[NSMutableString alloc]init];
    GLKVector3 pos;
    int i=1;
    for (id position in positions) {
        NSValue *value = position;
        [value getValue:&pos];
        [path appendString:[NSString stringWithFormat:@"N%dX%.3fY%.3fZ%.3f\n", i++, pos.x, pos.y, pos.z]];
    }
    return path;
}

- (NSString *)generateFourthPassForFourtHeightMap:(float**)heightMap withNormalMap:(GLKVector4**) normalMap withSize:(unsigned int)size {
    NSMutableArray *positions = [[NSMutableArray alloc]init];
    
    BOOL first = YES;

    for (int i=0; i < [sceneElementsArray count]; i++) {
        AKObject *obj = [sceneElementsArray objectAtIndex:i];
        if ([obj isKindOfClass:[AKBezierC2Surface class]]) {
            AKBezierC2Surface *surface = (AKBezierC2Surface *)obj;
            if (![[surface elementName]containsString:@"0"]) {
                //continue;
            }
            [surface generateFourthPathForFourthHeightMap:heightMap andNormalMap:normalMap withSize:size addToPaths:positions];
            if (first) {
                first = NO;
                GLKVector3 pos;
                NSValue *val = [positions firstObject];
                [val getValue:&pos];
                pos.z = 80;
                [positions insertObject:[NSValue valueWithBytes:&pos objCType:@encode(GLKVector3)] atIndex:0];
            }
            GLKVector3 pos2;
            NSValue *val = [positions firstObject];
            [val getValue:&pos2];
            pos2.z = 80;
            [positions addObject:[NSValue valueWithBytes:&pos2 objCType:@encode(GLKVector3)]];
            pos2.x = 0;
            pos2.y = 0;
            [positions addObject:[NSValue valueWithBytes:&pos2 objCType:@encode(GLKVector3)]];
        }
    }
    
    
    NSMutableString *path = [[NSMutableString alloc]init];
    GLKVector3 pos;
    int i=1;
    for (id position in positions) {
        NSValue *value = position;
        [value getValue:&pos];
        [path appendString:[NSString stringWithFormat:@"N%dX%.3fY%.3fZ%.3f\n", i++, pos.x, pos.y, pos.z]];
    }
    return path;
}

- (float) getHeightFromHeightMap:(float**)heightMap withSize:(unsigned int)size fromPosition:(GLKVector3) position {
    float height = 0;
    position.x += 75;
    position.y += 75;
    if (position.x > 150 || position.y > 150) {
        return 0;
    }
    height = heightMap[(int)(position.x*(size-1))/150][(int)(position.y*(size-1))/150];
    return height;
}

- (void) addVector:(GLKVector3)vector toArray:(NSMutableArray*)mutableArray {
    //NSLog(@"Add vector:(%.3f, %.3f, %.3f", vector.x, vector.y, vector.z);
    [mutableArray addObject:[NSValue valueWithBytes:&vector objCType:@encode(GLKVector3)]];

}

- (GLKVector3) lastPositionInArray:(NSMutableArray*)mutableArray {
    GLKVector3 pos;
    NSValue *value = [mutableArray lastObject];
    [value getValue:&pos];
    return pos;
}

- (float**)getNewHeightMapForMill:(float)radius isBall:(BOOL)isBall fromHeightMap:(float**)heightMap withSize:(unsigned int)size {
    float baseRadius = radius;
    float baseHeightRadius = 150*radius/size;
    float heightRadius;
    float radiusStep = 0.5f;
    NSLog(@"Base height radius: %lf", baseHeightRadius);
    float** newHeightMap = malloc(size*sizeof(float*));
    for (int i=0; i<size; i++) {
        newHeightMap[i] = malloc(size*sizeof(float*));
        for (int j=0; j<size; j++) {
            newHeightMap[i][j]=0;
        }
    }
    
    for (int x0=0; x0<size; x0++) {
        for (int y0=0; y0<size; y0++) {
            if (isBall) {
                radius = baseRadius;
                heightRadius = baseHeightRadius;
                float baseValue = heightMap[x0][y0];
                [self setHeightMap:newHeightMap withSize:size value:baseHeightRadius onPositionI:x0 onPositionJ:y0 withBaseValue:baseValue];
                if (baseValue == 0) {
                    continue;
                }
                while (radius >= 0) {
                    float heightVal = sqrtf(baseHeightRadius*baseHeightRadius-heightRadius*heightRadius);
                    
                    float x = radius;
                    float y = 0;
                    float radiusError = 1-x;
                    float tmp;
                    
                    while(x >= y)
                    {
                        
                        [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:x+x0 onPositionJ:y+y0 withBaseValue:baseValue];
                        [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:y+x0 onPositionJ:x+y0 withBaseValue:baseValue];
                        [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:-x+x0 onPositionJ:y+y0 withBaseValue:baseValue];
                        [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:-y+x0 onPositionJ:x+y0 withBaseValue:baseValue];
                        [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:-x+x0 onPositionJ:-y+y0 withBaseValue:baseValue];
                        [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:-y+x0 onPositionJ:-x+y0 withBaseValue:baseValue];
                        [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:x+x0 onPositionJ:-y+y0 withBaseValue:baseValue];
                        [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:y+x0 onPositionJ:-x+y0 withBaseValue:baseValue];
                        tmp = 1;
                        /*
                         while (x0-x+tmp < x0 + x) {
                         [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:x0-x onPositionJ:y+y0 withBaseValue:baseValue];
                         [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:x0-x onPositionJ:-y+y0 withBaseValue:baseValue];
                         
                         [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:x0+y onPositionJ:-x+y0 withBaseValue:baseValue];
                         [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:x0-y onPositionJ:-x+y0 withBaseValue:baseValue];
                         tmp++;
                         }
                         */
                        
                        y++;
                        if (radiusError<0)
                        {
                            radiusError += 2 * y + 1;
                        }
                        else
                        {
                            x--;
                            radiusError += 2 * (y - x + 1);
                        }
                    }
                    radius-=radiusStep;
                    heightRadius -= radiusStep*baseHeightRadius/radius;
                }
            }else {
                radius = baseRadius;
                float baseValue = heightMap[x0][y0];
                float heightVal = 0;
                
                float x = radius;
                float y = 0;
                float radiusError = 1-x;
                float tmp;
                
                while(x >= y)
                {
                    [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:x+x0 onPositionJ:y+y0 withBaseValue:baseValue];
                    [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:y+x0 onPositionJ:x+y0 withBaseValue:baseValue];
                    [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:-x+x0 onPositionJ:y+y0 withBaseValue:baseValue];
                    [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:-y+x0 onPositionJ:x+y0 withBaseValue:baseValue];
                    [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:-x+x0 onPositionJ:-y+y0 withBaseValue:baseValue];
                    [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:-y+x0 onPositionJ:-x+y0 withBaseValue:baseValue];
                    [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:x+x0 onPositionJ:-y+y0 withBaseValue:baseValue];
                    [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:y+x0 onPositionJ:-x+y0 withBaseValue:baseValue];
                    tmp = 1;
                    /*
                     while (x0-x+tmp < x0 + x) {
                     [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:x0-x onPositionJ:y+y0 withBaseValue:baseValue];
                     [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:x0-x onPositionJ:-y+y0 withBaseValue:baseValue];
                     
                     [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:x0+y onPositionJ:-x+y0 withBaseValue:baseValue];
                     [self setHeightMap:newHeightMap withSize:size value:heightVal onPositionI:x0-y onPositionJ:-x+y0 withBaseValue:baseValue];
                     tmp++;
                     }
                     */
                    
                    y++;
                    if (radiusError<0)
                    {
                        radiusError += 2 * y + 1;
                    }
                    else
                    {
                        x--;
                        radiusError += 2 * (y - x + 1);
                    }
                }
            }
        }
    }
    return newHeightMap;

}

- (void) setHeightMap:(float**)heightMap withSize:(unsigned int)size value:(float)value onPositionI:(float)ii onPositionJ:(float)jj withBaseValue:(float)baseValue{
    int i = (int)round(ii);
    int j = (int)round(jj);
    if (i<0 || j<0 || i >= size || j >= size) {
        return;
    }
    heightMap[i][j]=MAX(heightMap[i][j], value+baseValue);
}

- (BOOL)isPathClearFromPosition:(GLKVector3)fromPosition toPosition:(GLKVector3)toPosition onHeight:(float)height withHeightMap:(float**)heightMap withSize:(unsigned int)size {
    
    int x1 = (int)round(fromPosition.x);
    int x2 = (int)round(toPosition.x);
    int y1 = (int)round(fromPosition.y);
    int y2 = (int)round(toPosition.y);
    
    int d, dx, dy, ai, bi, xi, yi;
    int x = x1, y = y1;
    
    if (x1 < x2) {
        xi = 1;
        dx = x2 - x1;
    } else {
        xi = -1;
        dx = x1 - x2;
    }
    if (y1 < y2) {
        yi = 1;
        dy = y2 - y1;
    } else {
        yi = -1;
        dy = y1 - y2;
    }
    
    if ([self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(x, y, 0)] > height) {
        return false;
    }
    
    // oś wiodąca OX
    if (dx > dy)
    {
        ai = (dy - dx) * 2;
        bi = dy * 2;
        d = bi - dx;
        // pętla po kolejnych x
        while (x != x2)
        {
            // test współczynnika
            if (d >= 0)
            {
                x += xi;
                y += yi;
                d += ai;
            }
            else
            {
                d += bi;
                x += xi;
            }
            if ([self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(x, y, 0)] > height) {
                return false;
            }
        }
    }
    // oś wiodąca OY
    else
    {
        ai = ( dx - dy ) * 2;
        bi = dx * 2;
        d = bi - dy;
        // pętla po kolejnych y
        while (y != y2)
        {
            // test współczynnika
            if (d >= 0)
            {
                x += xi;
                y += yi;
                d += ai;
            }
            else
            {
                d += bi;
                y += yi;
            }
            if ([self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(x, y, 0)] > height) {
                return false;
            }
        }
    }
    
    return true;
}

@end
