//
//  AKOpenGLView.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 24.02.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>

@interface AKOpenGLView : NSOpenGLView <NSTableViewDataSource, NSTableViewDelegate> {
    CVDisplayLinkRef displayLink;
}

@property (nonatomic) IBOutlet  NSTextField *RbValueTextField;
@property (nonatomic) IBOutlet  NSTextField *rsValueTextField;
@property (nonatomic) IBOutlet  NSTextField *aValueTextField;
@property (nonatomic) IBOutlet  NSTextField *bValueTextField;

@property (nonatomic) IBOutlet  NSTextField *aPerspectiveTextField;
@property (nonatomic) IBOutlet  NSTextField *rPerspectiveTextField;
@property (nonatomic) IBOutlet  NSButton    *checkbox3D;
@property (nonatomic) IBOutlet  NSButton    *lamana;
@property (nonatomic) IBOutlet  NSButton    *moveCursor;
@property (nonatomic) IBOutlet  NSButton    *bsplineMode;
@property (nonatomic) IBOutlet  NSTableView *sceneContentTableView;
@property (nonatomic) IBOutlet  NSTextField *cursorScenePosition;
@property (nonatomic) IBOutlet  NSTextField *cursorScreenPosition;
@property (nonatomic) IBOutlet  NSTextField *selectedPointLabel;


@property (nonatomic) IBOutlet  NSTextField *bezierSurfaceUValue;
@property (nonatomic) IBOutlet  NSTextField *bezierSurfaceVValue;
@property (nonatomic) IBOutlet  NSTextField *bezierSurfaceSliderUValue;
@property (nonatomic) IBOutlet  NSTextField *bezierSurfaceSliderVValue;

@property (nonatomic) IBOutlet  NSTextField *trimDValue;

@property (nonatomic) IBOutlet  NSTextField *bezierSurfacePlaneWidth;
@property (nonatomic) IBOutlet  NSTextField *bezierSurfacePlaneHeight;
@property (nonatomic) IBOutlet  NSTextField *bezierSurfaceRollerHeight;
@property (nonatomic) IBOutlet  NSTextField *bezierSurfaceRollerRadius;
@property (nonatomic) IBOutlet  NSButton    *bezierSurfaceRollerCheckbox;



@property (nonatomic) IBOutlet  NSSlider *bezierSurfaceSliderU;
@property (nonatomic) IBOutlet  NSSlider *bezierSurfaceSliderV;

@property (nonatomic) IBOutlet  NSTextField *gregoryPatchSliderUValue;
@property (nonatomic) IBOutlet  NSTextField *gregoryPatchSliderVValue;
@property (nonatomic) IBOutlet  NSSlider *gregoryPatchSliderU;
@property (nonatomic) IBOutlet  NSSlider *gregoryPatchSliderV;

@end
