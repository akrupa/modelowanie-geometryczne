//
//  AKImageView.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 03.03.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKImageView.h"
#import "AKImageViewRenderer.h"

@implementation AKImageView

int height;
int width;

NSBitmapImageRep *bitmap;
AKImageViewRenderer *m_renderer;
bool _space = false;


-(BOOL)acceptsFirstResponder
{
    return YES;
}

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        height = frame.size.height;
        width = frame.size.width;
        m_renderer = [[AKImageViewRenderer alloc]initWithWidth:frame.size.width height:frame.size.height];
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
	[super drawRect:dirtyRect];
    
    [m_renderer renderImage];
	
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    [m_renderer rotateY:-[theEvent deltaY]*2/1000];
    if (_space) {
        [m_renderer rotateZ:-[theEvent deltaX]*2/1000];
    } else {
        [m_renderer rotateX:[theEvent deltaX]*2/1000];
    }
    
    
    [self setNeedsDisplay:TRUE];
}

-(void)scrollWheel:(NSEvent *)theEvent
{
    if(!_space)
    {
        [m_renderer moveX:-[theEvent deltaY] moveY:-[theEvent deltaX] moveZ:0];
    }
    else
    {
        [m_renderer moveX:0 moveY:-[theEvent deltaX] moveZ:-[theEvent deltaY]];
    }
    
    [self setNeedsDisplay:TRUE];
    
}

-(void) magnifyWithEvent:(NSEvent *)event
{
    [m_renderer scale:1+[event magnification]];
    [self setNeedsDisplay:TRUE];
}

-(void) keyDown:(NSEvent *)theEvent
{
    switch( [theEvent keyCode] ) {
    	case 53: //esc
            [m_renderer reset];
            break;
        case 49: //space
            _space = true;
            break;
    }
    [self setNeedsDisplay:TRUE];
}

-(void) keyUp:(NSEvent *)theEvent
{
    switch( [theEvent keyCode] ) {
    	case 49: //space
            _space = false;
            break;
    }
    [self setNeedsDisplay:TRUE];
}

-(IBAction) refreshPressed:(NSButton*) sender
{
    int scale = [_scaleValueTextField intValue];
    scale = MAX(1, scale);
    [m_renderer refreshWithM:[_mImageValueTextField floatValue]
                           a:[_aImageValueTextField floatValue]
                           b:[_bImageValueTextField floatValue]
                           c:[_cImageValueTextField floatValue]
                       scale:scale];
    [_window makeFirstResponder:self];
    [self setNeedsDisplay:TRUE];
}

@end
