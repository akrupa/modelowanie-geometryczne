//
//  AKPoint.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 22.03.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKPoint.h"

@implementation AKPoint

-(void)addObjectToArray:(GLKVector4**)array
               withSize:(int*)arraySize
             andIndices:(indic**)indices
        withIndicesSize:(int*)indicesSize {

    positionInVertexArray = *arraySize;
    int size = *arraySize + internalArraySize;
    GLKVector4 *newArray = malloc(size*sizeof(GLKVector4));
    if (*arraySize > 0) {
        memcpy(newArray, *array, (*arraySize)*sizeof(GLKVector4));
        free(*array);
    }
    memcpy(newArray + positionInVertexArray, internalArray, internalArraySize*sizeof(GLKVector4));
    *arraySize = size;
    *array = newArray;
}

-(AKObject*)initAtPosition:(GLKVector4)positionV andNumber:(NSUInteger)number {
    name = [NSString stringWithFormat:@"Point%lu", number];
    internalIndicSize = 1;
    internalArraySize = 1;
    internalIndices = malloc(sizeof(indic));
    internalArray = malloc(sizeof(GLKVector4));
    internalArray[0] = positionV;
    internalIndices[0].from = 0;
    return self;
}

- (GLKVector4)position {
    return internalArray[0];
}

- (void) setPosition:(GLKVector4)position {
    internalArray[0] = position;
}

- (void) movePointAtX:(GLfloat)xValue AtY:(GLfloat)yValue AtZ:(GLfloat)zValue {
    internalArray[0].x += xValue;
    internalArray[0].y += yValue;
    internalArray[0].z += zValue;
}

- (void)updateObjectsArray:(GLKVector4**)array {
    GLKVector4 *objectsArray = *array;
    memcpy(objectsArray + positionInVertexArray, internalArray, internalArraySize*sizeof(GLKVector4));
}

-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize {
    
    memcpy(array + *arrayPoint, internalArray, internalArraySize * sizeof(GLKVector4));
    
    GLushort *indic = (GLushort *)indices;
    for (int i=0; i < internalIndicSize; i++) {
        indic[*indicesSize + i] = internalIndices[i].from + *arrayPoint;
    }
    
    *arrayPoint += internalArraySize;
    *indicesSize += internalIndicSize;
    
}

-(int)vertexArraySize {
    return 1;
}

-(int)indicArraySize {
    return 1;
}

- (NSString *)debugDescription {
    return [NSString stringWithFormat:@"(%.3f, %.3f, %.3f)", internalArray[0].x, internalArray[0].y, internalArray[0].z];
}

@end
