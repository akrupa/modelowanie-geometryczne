//
//  AKImageViewRenderer.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 03.03.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKImageViewRenderer.h"
#import <GLKit/GLKMath.h>

#define CLAMP(x, low, high) ({\
__typeof__(x) __x = (x); \
__typeof__(low) __low = (low);\
__typeof__(high) __high = (high);\
__x > __high ? __high : (__x < __low ? __low : __x);\
})

@implementation AKImageViewRenderer

NSBitmapImageRep *bitmap;
int _width, _height;
int frameWidth, frameHeight;
float a = 1.0/(130*130);
float b = 1.0/(100*100);
float c = 1.0/(70*70);
float rv = 500;
float m = 1;
int scale = 1;
NSImage *image;

GLKMatrix4 transMatrix;
GLKMatrix4 projMatrix;
GLKMatrix4 D;


- (id) initWithWidth:(int)width height:(int)height
{
    if((self = [super init]))
    {
        [self setBitmapWithWidth:width height:height];
        
        transMatrix = GLKMatrix4Identity;
        projMatrix = GLKMatrix4Identity;
        projMatrix.m23 = 1.0/rv;
        projMatrix = GLKMatrix4Invert(projMatrix, NULL);
        D = GLKMatrix4Identity;
        D.m00 = a;
        D.m11 = b;
        D.m22 = c;
        D.m33 = -1;
        frameWidth = width;
        frameHeight = height;
    }
    return self;
}

- (void) setBitmapWithWidth:(int)width height:(int) height
{
    _width = width;
    _height = height;
    bitmap = [[NSBitmapImageRep alloc] initWithBitmapDataPlanes:nil
                                                     pixelsWide:width
                                                     pixelsHigh:height
                                                  bitsPerSample:8
                                                samplesPerPixel:4
                                                       hasAlpha:YES
                                                       isPlanar:NO
                                                 colorSpaceName:NSCalibratedRGBColorSpace
                                                   bitmapFormat:0
                                                    bytesPerRow:(4 * width)
                                                   bitsPerPixel:32];
    image = [[NSImage alloc] initWithSize:NSMakeSize(frameWidth, frameHeight)];
}

- (void) renderImage
{
    [image removeRepresentation:bitmap];
    [self updateBitmap];
    [image addRepresentation:bitmap];
    
    [image drawAtPoint:NSMakePoint(0.0, 0.0)
              fromRect: NSMakeRect(0.0, 0.0, frameWidth, frameHeight)
             operation: NSCompositeSourceOver
              fraction: 1.0];
}

- (void) updateBitmap
{
    //GLKMatrix4 M = transMatrix;
    GLKMatrix4 M = GLKMatrix4Multiply(projMatrix, transMatrix);
    GLKMatrix4 MInv = GLKMatrix4Invert(M, NULL);
    GLKMatrix4 DML = GLKMatrix4Multiply(GLKMatrix4Transpose(MInv), D);
    GLKMatrix4 DM = GLKMatrix4Multiply(DML, MInv);
    //DM = GLKMatrix4Invert(DM, NULL);

    unsigned char *data = [bitmap bitmapData];
    
    for (int i=0; i<_width; i++) {
        for (int j=0; j<_height; j++) {
            int x = i - _width/2;
            int y = j - _height/2;
            
            double ak = DM.m22;
            double bk = (DM.m20 + DM.m02)*x + (DM.m21 + DM.m12)*y + DM.m32 + DM.m23;
            //float ck = (DM.m00*x + (DM.m10 + DM.m01)*y + DM.m30 + DM.m03)*x + (DM.m11*y + DM.m31 + DM.m13)*y + DM.m33;
            double ck = DM.m00*x*x + DM.m11*y*y + DM.m33 + (DM.m01 + DM.m10)*x*y + (DM.m03 + DM.m30)*x + (DM.m13 + DM.m31)*y;
            
            double delta = bk*bk - 4*ak*ck;
            
            
            if (delta>=0) {
                double sqrtDelta = sqrtf(delta);
                double z1 = (-bk+sqrtDelta)/(2*ak);
                double z2 = (-bk-sqrtDelta)/(2*ak);
                double z = z1 > z2 ? z1 : z2;
                //[bitmap setColor:[NSColor colorWithCalibratedRed:z green:0 blue:0 alpha:1] atX:j y:i];
                
                if (z > rv) {
                     z = z == z1 ? z2 : z1;
                }
                
                double dx = 2 * DM.m00 * x + (DM.m02 + DM.m20) * z + (DM.m01 + DM.m10) * y + DM.m03 + DM.m30;
                double dy = 2 * DM.m11 * y + (DM.m12 + DM.m21) * z + (DM.m01 + DM.m10) * x + DM.m13 + DM.m31;
                double dz = 2 * DM.m22 * z + (DM.m02 + DM.m20) * x + (DM.m12 + DM.m21) * y + DM.m32 + DM.m23;
                GLKVector3 vec = GLKVector3Normalize(GLKVector3Make(dx, dy, dz));
                //vec = GLKVector3Normalize(GLKMatrix4MultiplyVector3(DM, vec));
                GLKVector3 norm = GLKVector3Make(0, 0, 1);
                
                float cross = GLKVector3DotProduct(vec, norm);
                cross = powf(cross, m);
                if(z > -rv && cross > 0 && z < rv) {
                    data[(i*_width+j)*4] = (unsigned char)CLAMP(255*cross, 0, 255);
                    data[(i*_width+j)*4+1] = (unsigned char)CLAMP(204*cross, 0, 204);
                    data[(i*_width+j)*4+2] = (unsigned char)0;
                    data[(i*_width+j)*4+3] = (unsigned char)255;
                } else if (z > rv || z < -rv){
                    data[(i*_width+j)*4] = (unsigned char)255*0.5;
                    data[(i*_width+j)*4+1] = (unsigned char)0;
                    data[(i*_width+j)*4+2] = (unsigned char)255*0.5;
                    data[(i*_width+j)*4+3] = (unsigned char)255;
                } else {
                    data[(i*_width+j)*4] = (unsigned char)0;
                    data[(i*_width+j)*4+1] = (unsigned char)255*0.5;
                    data[(i*_width+j)*4+2] = (unsigned char)255*0.5;
                    data[(i*_width+j)*4+3] = (unsigned char)255;
                }
            } else {
                data[(i*_width+j)*4] = (unsigned char)255*0.5;
                data[(i*_width+j)*4+1] = (unsigned char)0;
                data[(i*_width+j)*4+2] = (unsigned char)255*0.5;
                data[(i*_width+j)*4+3] = (unsigned char)255;
            }
        }
    }
}

- (void) rotateX:(float)angle
{
    GLKMatrix4 matrix = GLKMatrix4Identity;
    matrix.m11 = cos(angle);
    matrix.m12 = sin(angle);
    matrix.m21 = -sin(angle);
    matrix.m22 = cos(angle);
    
    matrix = GLKMatrix4Invert(matrix, NULL);
    transMatrix = GLKMatrix4Multiply(matrix, transMatrix);
}

- (void) rotateY:(float)angle
{
    GLKMatrix4 matrix = GLKMatrix4Identity;
    matrix.m00 = cos(angle);
    matrix.m02 = -sin(angle);
    matrix.m20 = sin(angle);
    matrix.m22 = cos(angle);
    
    matrix = GLKMatrix4Invert(matrix, NULL);
    transMatrix = GLKMatrix4Multiply(matrix, transMatrix);
}

- (void) rotateZ:(float)angle
{
    GLKMatrix4 matrix = GLKMatrix4Identity;
    matrix.m00 = cos(angle);
    matrix.m10 = -sin(angle);
    matrix.m01 = sin(angle);
    matrix.m11 = cos(angle);
    
    matrix = GLKMatrix4Invert(matrix, NULL);
    transMatrix = GLKMatrix4Multiply(matrix, transMatrix);
}
- (void) moveX:(float)deltaX moveY:(float)deltaY moveZ:(float)deltaZ
{
    GLKMatrix4 matrix = GLKMatrix4Identity;
    matrix.m30 = deltaX;
    matrix.m31 = deltaY;
    matrix.m32 = deltaZ;
    
    //matrix = GLKMatrix4Invert(matrix, NULL);
    transMatrix = GLKMatrix4Multiply(matrix, transMatrix);
}

- (void) scale:(float)scale
{
    GLKMatrix4 matrix = GLKMatrix4Identity;
    matrix.m00 = scale;
    matrix.m11 = scale;
    matrix.m22 = scale;
    
    //matrix = GLKMatrix4Invert(matrix, NULL);
    transMatrix = GLKMatrix4Multiply(matrix, transMatrix);
}

- (void) reset
{
    transMatrix = GLKMatrix4Identity;
    [self renderImage];
}

- (void) refreshWithM:(float)mValue a:(float)aValue b:(float)bValue c:(float)cValue scale:(int)mScale
{
    a = 1/(aValue * aValue);
    b = 1/(bValue * bValue);
    c = 1/(cValue * cValue);
    m = mValue;
    D = GLKMatrix4Identity;
    D.m00 = a;
    D.m11 = b;
    D.m22 = c;
    D.m33 = -1;
    if (scale != mScale) {
        [self setBitmapWithWidth:frameWidth/mScale height:frameHeight/mScale];
        [self scale:scale];
        [self scale:1.0/mScale];
        scale = mScale;
    }
    [self renderImage];
}


@end

