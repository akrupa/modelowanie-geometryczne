//
//  AKSceneJSON.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 15.05.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKSceneJSON.h"
#import "AKBezierC2Surface.h"
#import "AKPoint.h"

@implementation AKSceneJSON

- (void) doSomething {
    NSString *jsonString = @"{\
    \"surfaces\":[\
                {\
                    \"name\":\"Powierzchnia 1\"\
                }]\
    }";
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *e = nil;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
    
    NSLog(@"\n%@", json);
}

- (void) saveBezierC2SurfacesToFile:(NSArray*)surfaces {
    NSError *error;
    NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc]init];
    NSMutableArray *surfacesArray = [[NSMutableArray alloc]init];
    
    for (int i=0; i<[surfaces count]; i++) {
        AKBezierC2Surface *surface = [surfaces objectAtIndex:i];
        NSMutableDictionary *surfaceData = [[NSMutableDictionary alloc]init];
        [surfaceData setObject:[surface elementName] forKey:@"name"];
        [surfaceData setObject:[surface rows] forKey:@"rows"];
        [surfaceData setObject:[surface columns] forKey:@"columns"];
        
        NSMutableArray *indicesArray = [[NSMutableArray alloc]init];
        NSMutableArray *verticesArray = [[NSMutableArray alloc]init];

        int rows = [[surface rows]intValue];
        int columns = [[surface columns]intValue];
        
        if ([[surface nodesInternalArray]objectAtIndex:0] == [[surface nodesInternalArray]objectAtIndex:columns*(3+rows)]) {
            NSLog(@"saving roller");
            for (int i=0; i<3+rows; i++) {
                for (int j=0; j<columns; j++) {
                    [indicesArray addObject:[NSNumber numberWithInt:i*(columns)+j]];
                    AKPoint *point = [[surface nodesInternalArray]objectAtIndex:j*(3+rows)+i];
                    NSMutableDictionary *pointData = [[NSMutableDictionary alloc]init];
                    [pointData setObject:[NSNumber numberWithFloat:[point position].x] forKey:@"x"];
                    [pointData setObject:[NSNumber numberWithFloat:[point position].y] forKey:@"y"];
                    [pointData setObject:[NSNumber numberWithFloat:[point position].z] forKey:@"z"];
                    [verticesArray addObject:pointData];
                }
                for (int j=0; j<3; j++) {
                    [indicesArray addObject:[NSNumber numberWithInt:i*(columns)+j]];
                }
            }
        } else {
            NSLog(@"saving plane");
            for (int i=0; i<3+rows; i++) {
                for (int j=0; j<3+columns; j++) {
                    [indicesArray addObject:[NSNumber numberWithInt:i*(3+columns)+j]];
                    AKPoint *point = [[surface nodesInternalArray]objectAtIndex:i*(3+columns)+j];
                    NSMutableDictionary *pointData = [[NSMutableDictionary alloc]init];
                    [pointData setObject:[NSNumber numberWithFloat:[point position].x] forKey:@"x"];
                    [pointData setObject:[NSNumber numberWithFloat:[point position].y] forKey:@"y"];
                    [pointData setObject:[NSNumber numberWithFloat:[point position].z] forKey:@"z"];
                    [verticesArray addObject:pointData];
                }
            }
        }
        

        
        [surfaceData setObject:indicesArray forKey:@"indices"];
        [surfaceData setObject:verticesArray forKey:@"vertices"];
        
        [surfacesArray addObject:surfaceData];
    }
    [dataDictionary setObject:surfacesArray forKey:@"surfaces"];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
        return;
    }
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    //NSLog(@"\n%@", jsonString);
    
    
    NSSavePanel *savePanel = [NSSavePanel savePanel];
    
    [savePanel setDirectoryURL:[NSURL fileURLWithPath:@"./"]];

    [savePanel beginWithCompletionHandler:^(NSInteger result){
        if (result == NSFileHandlingPanelOKButton) {
            // Close panel before handling errors
            [savePanel orderOut:self];
            
            NSError *error;
            [jsonString writeToURL:[savePanel URL] atomically:YES encoding:NSUTF8StringEncoding error:&error];
            if (error) {
                NSLog(@"ERROR: %@", [error localizedDescription]);
            }
        }
    }];
}

- (void) loadfromFileBezierC2Surfaces:(NSMutableArray*)surfaces withPoints:(NSMutableArray*)points usingScreenWitdh:(int)screenWidth screenHeight:(int)screenHeight andMatrix:(GLKMatrix4)M {
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    [openPanel setDirectoryURL:[NSURL fileURLWithPath:@"./"]];
    
    NSInteger result = [openPanel runModal];
    if (result == NSFileHandlingPanelOKButton) {
        
        [openPanel orderOut:self];
        NSError *error;
        NSString *fileString = [NSString stringWithContentsOfURL:[openPanel URL] encoding:NSUTF8StringEncoding error:&error];
        if (error) {
            NSLog(@"ERROR: %@", [error localizedDescription]);
            return;
        }
        
        NSData *jsonData = [fileString dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        
        if (error) {
            NSLog(@"ERROR JSON parsing: %@", [error localizedDescription]);
            return;
        }
        
        if (![json isKindOfClass:[NSDictionary class]]) {
            NSLog(@"ERROR JSON parsing: root is not a dictionary");
            return;
        }
        
        NSDictionary *root = (NSDictionary*)json;
        
        id surfacesRoot = [root objectForKey:@"surfaces"];
        if (surfacesRoot == nil || ![surfacesRoot isKindOfClass:[NSArray class]]) {
            NSLog(@"ERROR JSON parsing: no surfaces key or surfaces is not an array");
            return;
        }
        
        NSArray *surfacesArray = (NSArray*)surfacesRoot;
        int offset = 0;
        for (int i=0; i<[surfacesArray count]; i++) {
            id surface = [surfacesArray objectAtIndex:i];
            if (surface == nil || ![surface isKindOfClass:[NSDictionary class]]) {
                NSLog(@"ERROR JSON parsing: surface is not a dictionary");
                [surfaces removeAllObjects];
                [points removeAllObjects];
                return;
            }
            NSDictionary *surfaceDictionary = (NSDictionary*)surface;
            
            id rowsData = [surfaceDictionary objectForKey:@"rows"];
            id columnsData = [surfaceDictionary objectForKey:@"columns"];
            id nameData = [surfaceDictionary objectForKey:@"name"];
            id indicesData = [surfaceDictionary objectForKey:@"indices"];
            id verticesData = [surfaceDictionary objectForKey:@"vertices"];
            
            if (rowsData == nil || ![rowsData isKindOfClass:[NSNumber class]]) {
                NSLog(@"ERROR JSON parsing: rows is not a number");
                [surfaces removeAllObjects];
                [points removeAllObjects];
                return;
            }
            if (columnsData == nil || ![columnsData isKindOfClass:[NSNumber class]]) {
                NSLog(@"ERROR JSON parsing: columns is not a number");
                [surfaces removeAllObjects];
                [points removeAllObjects];
                return;
            }
            if (nameData == nil || ![nameData isKindOfClass:[NSString class]]) {
                NSLog(@"ERROR JSON parsing: name is not a string");
                [surfaces removeAllObjects];
                [points removeAllObjects];
                return;
            }
            if (indicesData == nil || ![indicesData isKindOfClass:[NSArray class]]) {
                NSLog(@"ERROR JSON parsing: indices is not an array");
                [surfaces removeAllObjects];
                [points removeAllObjects];
                return;
            }
            if (verticesData == nil || ![verticesData isKindOfClass:[NSArray class]]) {
                NSLog(@"ERROR JSON parsing: vertices is not an array");
                [surfaces removeAllObjects];
                [points removeAllObjects];
                return;
            }
            
            NSNumber *rowsNumber = (NSNumber*)rowsData;
            NSNumber *columnsNumber = (NSNumber*)columnsData;
            NSString *nameString = (NSString*)nameData;
            NSArray *indicesArray = (NSArray*)indicesData;
            NSArray *verticesArray = (NSArray*)verticesData;
            
            for (int j=0; j<[verticesArray count]; j++) {
                id vertexData = [verticesArray objectAtIndex:j];
                if (vertexData == nil || ![vertexData isKindOfClass:[NSDictionary class]]) {
                    NSLog(@"ERROR JSON parsing: vertex is not a dictionary");
                    [surfaces removeAllObjects];
                    [points removeAllObjects];
                    return;
                }
                
                NSDictionary *vertex = (NSDictionary*)vertexData;
                
                id xData = [vertex valueForKey:@"x"];
                id yData = [vertex valueForKey:@"y"];
                id zData = [vertex valueForKey:@"z"];
                
                if (xData == nil ||
                    yData == nil ||
                    zData == nil ||
                    ![xData isKindOfClass:[NSNumber class]] ||
                    ![yData isKindOfClass:[NSNumber class]] ||
                    ![zData isKindOfClass:[NSNumber class]]) {
                    
                    NSLog(@"ERROR JSON parsing: vertex part is not a number");
                    [surfaces removeAllObjects];
                    [points removeAllObjects];
                    return;
                }
                
                
                NSNumber *xNumber = (NSNumber*)xData;
                NSNumber *yNumber = (NSNumber*)yData;
                NSNumber *zNumber = (NSNumber*)zData;
                
                GLKVector4 position;
                position.x = [xNumber floatValue];
                position.y = [zNumber floatValue];
                position.z = [yNumber floatValue];
                position.w = 1;
                AKPoint *point = [[AKPoint alloc]initAtPosition:position andNumber:j];
                [points addObject:point];
            }
            
            
            NSMutableArray *surfacePoints = [[NSMutableArray alloc]init];
            
            int rows = [rowsNumber intValue];
            int cols = [columnsNumber intValue];
            for (int j=0; j<3+cols; j++) {
                for (int k=0; k<3+rows; k++) {
                    int index = k*(cols+3)+j;
                    id indicData = [indicesArray objectAtIndex:index];
                    if (indicData == nil || ![indicData isKindOfClass:[NSNumber class]]) {
                        NSLog(@"ERROR JSON parsing: indic is not a number");
                        [surfaces removeAllObjects];
                        [points removeAllObjects];
                        return;
                    }
                    NSNumber *indic = (NSNumber*)indicData;
                    AKPoint *point = [points objectAtIndex:[indic intValue]+offset];
                    
                    [surfacePoints addObject:point];
                }
            }
            /*
            for (int j=0; j<[indicesArray count]; j++) {
                id indicData = [indicesArray objectAtIndex:j];
                if (indicData == nil || ![indicData isKindOfClass:[NSNumber class]]) {
                    NSLog(@"ERROR JSON parsing: indic is not a number");
                    [surfaces removeAllObjects];
                    [points removeAllObjects];
                    return;
                }
                NSNumber *indic = (NSNumber*)indicData;
                AKPoint *point = [points objectAtIndex:[indic intValue]+offset];

                [surfacePoints addObject:point];
            }
             */
            offset += [verticesArray count];
            
            AKBezierC2Surface *newSurface = [[AKBezierC2Surface alloc]initWithPoints:surfacePoints
                                                                         screenWidth:screenWidth
                                                                        screenHeight:screenHeight
                                                                           withWidth:[columnsNumber intValue]
                                                                          withHeight:[rowsNumber intValue]
                                                                           andMatrix:M];
            [surfaces addObject:newSurface];
            
        }
    }
    
}

@end
