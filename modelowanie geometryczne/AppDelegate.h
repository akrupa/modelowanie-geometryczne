//
//  AppDelegate.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 24.02.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
