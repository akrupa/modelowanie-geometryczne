//
//  AKPoint.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 22.03.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKObject.h"

@interface AKPoint : AKObject {

int positionInVertexArray;

}

- (GLKVector4)position;
- (void) setPosition:(GLKVector4)position;
- (void) movePointAtX:(GLfloat)xValue AtY:(GLfloat)yValue AtZ:(GLfloat)zValue;
- (void) updateObjectsArray:(GLKVector4**)array;

@end
