//
//  main.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 24.02.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
