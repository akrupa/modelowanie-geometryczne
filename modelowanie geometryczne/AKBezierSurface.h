//
//  AKBezierSurface.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 28.04.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKObject.h"

@interface AKBezierSurface : AKObject {
    
    NSMutableArray *nodesInternalArray;
    NSMutableArray *nodesBezierInternalArray;
    NSMutableArray *nodesBezierInternalArray2;
    NSMutableArray *bezierCurvesArray;
    
    GLKMatrix4 bernsteinMatrix;
    
    int m_screenWidth;
    int m_screenHeight;
    int uSteps;
    int vSteps;
    
    int steps;
    int lines;
    
    int *steps2;
    int *lines2;
    
    int linesLamana;
    
    int m_width;
    int m_height;
    
    GLKMatrix4 MOld;
}

- (AKBezierSurface*) initWithPoints:(NSMutableArray*)pointsArray
                        screenWidth:(int)screenWidth
                       screenHeight:(int)screenHeight
                             number:(NSUInteger)number
                           onRoller:(BOOL)onRoller
                          withWidth:(int)width
                         withHeight:(int)height
                          andMatrix:(GLKMatrix4)M;

- (void) countStepsFromMatrix:(GLKMatrix4) M;
- (NSMutableArray*) getPoints;

-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize
    andArrayIndicOffset:(int *)offset;
- (int) linesCount;
- (int) curvesCount;
- (void) setUValue:(NSInteger)uValue andVValue:(NSInteger)vValue;
- (void) displayLamana;

@end
