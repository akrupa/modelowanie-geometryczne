//
//  AKTrimmingCurve.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 14.06.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKTrimmingCurve.h"
#include <gsl/gsl_linalg.h>

@implementation AKTrimmingCurve

- (AKTrimmingCurve*) initWithSurface1:(AKBezierC2Surface*)surface1
                             surface2:(AKBezierC2Surface*)surface2
                               number:(NSUInteger)number
                            andMatrix:(GLKMatrix4)M {
    
    
    name = [NSString stringWithFormat:@"Trimming Curve%lu", number];
    
    m_surface1 = surface1;
    m_surface2 = surface2;
    
    m_u0 = 0.5;
    m_v0 = 0.5;
    m_s0 = 0.5;
    m_t0 = 0.5;
    //GLKVector4 duTmp1 = [m_surface1 getDivUForU:0.5 andForV:0.5];

    dValue = 0.01;

    draw = [self computeStartPosition];
    if(draw)
        [self computeIntersectionCurve];
    
    return self;
}

- (bool) computeStartPosition {
    
    if ([self computeStartPositionFromU0:m_u0 andV0:m_v0 andS0:m_s0 andT0:m_t0]) {
        return true;
    }
    if ([self computeStartPositionFromU0:0.5 andV0:0.5 andS0:0.5 andT0:0.5]) {
        return true;
    }
    if ([self computeStartPositionFromU0:0.25 andV0:0.25 andS0:0.25 andT0:0.25]) {
        return true;
    }
    if ([self computeStartPositionFromU0:0.25 andV0:0.25 andS0:0.75 andT0:0.75]) {
        return true;
    }
    if ([self computeStartPositionFromU0:0.75 andV0:0.75 andS0:0.25 andT0:0.25]) {
        return true;
    }
    
    NSLog(@"Couldn't find start point");
    
    return false;
}

- (bool) computeStartPositionFromU0:(float)s1 andV0:(float)t1 andS0:(float)s2 andT0:(float)t2 {
    GLKVector4 position = GLKVector4Make(0, 0, 0, 1);
    
    GLKVector4 posTemp0 = [m_surface1 getPositionForU:s1 andForV:t1];
    GLKVector4 posTemp1 = [m_surface2 getPositionForU:s2 andForV:t2];
    
    GLKVector3 pos0 = GLKVector3Make(posTemp0.x, posTemp0.y, posTemp0.z);
    GLKVector3 pos1 = GLKVector3Make(posTemp1.x, posTemp1.y, posTemp1.z);
    
    int iteration = 10000;
    float dist = GLKVector3Distance(GLKVector3Make(pos0.x, pos0.y, pos0.z), GLKVector3Make(pos1.x, pos1.y, pos1.z));
    
    while (iteration > 0 && dist > 0.0001) {
        iteration--;
        
        GLKVector4 duTmp1 = [m_surface1 getDivUForU:s1 andForV:t1];
        GLKVector4 dvTmp1 = [m_surface1 getDivVForU:s1 andForV:t1];
        
        GLKVector4 duTmp2 = [m_surface2 getDivUForU:s2 andForV:t2];
        GLKVector4 dvTmp2 = [m_surface2 getDivVForU:s2 andForV:t2];
        
        GLKVector3 du1 = GLKVector3Make(duTmp1.x, duTmp1.y, duTmp1.z);
        GLKVector3 dv1 = GLKVector3Make(dvTmp1.x, dvTmp1.y, dvTmp1.z);
        
        GLKVector3 du2 = GLKVector3Make(duTmp2.x, duTmp2.y, duTmp2.z);
        GLKVector3 dv2 = GLKVector3Make(dvTmp2.x, dvTmp2.y, dvTmp2.z);
        
        GLKVector3 v1 = GLKVector3Subtract(pos0, pos1);
        GLKVector3 v2 = GLKVector3Subtract(pos1, pos0);
        
        v1 = GLKVector3MultiplyScalar(v1, 0.1);
        v2 = GLKVector3MultiplyScalar(v2, 0.1);
        
        float diffU1 = GLKVector3DotProduct(v1, du1)/2;
        float diffV1 = GLKVector3DotProduct(v1, dv1)/2;
        
        float diffU2 = GLKVector3DotProduct(v2, du2)/2;
        float diffV2 = GLKVector3DotProduct(v2, dv2)/2;
        
        t1 -= diffU1;
        s1 -= diffV1;
        
        t2 -= diffU2;
        s2 -= diffV2;
        
        //NSLog(@"%d: %.3f %.3f - %.3f %.3f - %.5f", iteration, s1, t1, s2, t2, dist);
        
        if (s1 > 1 || s1 < 0 || t1 > 1 || t1 < 0 ||
            s2 > 1 || s2 < 0 || t2 > 1 || t2 < 0) {
            break;
        }
        
        posTemp0 = [m_surface1 getPositionForU:s1 andForV:t1];
        posTemp1 = [m_surface2 getPositionForU:s2 andForV:t2];
        
        pos0 = GLKVector3Make(posTemp0.x, posTemp0.y, posTemp0.z);
        pos1 = GLKVector3Make(posTemp1.x, posTemp1.y, posTemp1.z);
        
        dist = GLKVector3Distance(GLKVector3Make(pos0.x, pos0.y, pos0.z), GLKVector3Make(pos1.x, pos1.y, pos1.z));
    }
    if (dist > 0.0001) {
        return false;
    }
    m_u0 = s1;
    m_v0 = t1;
    m_s0 = s2;
    m_t0 = t2;
    
    position = [m_surface1 getPositionForU:s1 andForV:t1];
    //startPosition2 = [m_surface2 getPositionForU:s2 andForV:t2];
    //GLKVector4 divU = [m_surface1 getDivUForU:0.5 andForV:0.5];
    //GLKVector4 divV = [m_surface1 getDivVForU:0.5 andForV:0.5];
    
    return true;
}

- (void) computeIntersectionCurve {
    float d;
    float v0 = m_v0;
    float u0 = m_u0;
    float s0 = m_s0;
    float t0 = m_t0;
    float dist = 1000;
    lines = 0;
    points = [[NSMutableArray alloc]init];
    //[points addObject:[[AKPoint alloc]initAtPosition:[m_surface2 getPositionForU:s0 andForV:t0] andNumber:0]];
    [points addObject:[[AKPoint alloc]initAtPosition:[m_surface1 getPositionForU:u0 andForV:v0] andNumber:0]];
    //return;
    for (int i=0; i<4500; i++) {
        d = dValue;
        dist = 1000;
        while (dist > 0.001) {
            GLKVector4 duTmp1 = [m_surface1 getDivVForU:u0 andForV:v0];
            GLKVector4 dvTmp1 = [m_surface1 getDivUForU:u0 andForV:v0];
            
            GLKVector4 duTmp2 = [m_surface2 getDivVForU:s0 andForV:t0];
            GLKVector4 dvTmp2 = [m_surface2 getDivUForU:s0 andForV:t0];
            
            GLKVector3 Pu = GLKVector3Make(duTmp1.x, duTmp1.y, duTmp1.z);
            //Pu.x = 0;
            GLKVector3 Pv = GLKVector3Make(dvTmp1.x, dvTmp1.y, dvTmp1.z);
            
            GLKVector3 Qs = GLKVector3Make(duTmp2.x, duTmp2.y, duTmp2.z);
            GLKVector3 Qt = GLKVector3Make(dvTmp2.x, dvTmp2.y, dvTmp2.z);
            
            GLKVector3 Pn = GLKVector3CrossProduct(Pu, Pv);
            GLKVector3 Qn = GLKVector3CrossProduct(Qs, Qt);
            GLKVector3 t = GLKVector3MultiplyScalar(GLKVector3CrossProduct(Pn, Qn), 1);
            t = GLKVector3Normalize(t);
            
            float taMacierz[4][5];
            taMacierz[0][0] = Pu.x;
            taMacierz[0][1] = Pv.x;
            taMacierz[0][2] = -Qs.x;
            taMacierz[0][3] = -Qt.x;
            
            taMacierz[1][0] = Pu.y;
            taMacierz[1][1] = Pv.y;
            taMacierz[1][2] = -Qs.y;
            taMacierz[1][3] = -Qt.y;
            
            taMacierz[2][0] = Pu.z;
            taMacierz[2][1] = Pv.z;
            taMacierz[2][2] = -Qs.z;
            taMacierz[2][3] = -Qt.z;
            
            taMacierz[3][0] = GLKVector3DotProduct(Pu, t);
            taMacierz[3][1] = GLKVector3DotProduct(Pv, t);
            taMacierz[3][2] = 0;
            taMacierz[3][3] = 0;
            
            taMacierz[0][4] = 0;
            taMacierz[1][4] = 0;
            taMacierz[2][4] = 0;
            taMacierz[3][4] = d;
            
            double tamtaMacierz[4][4];
            tamtaMacierz[0][0] = Pu.x;
            tamtaMacierz[0][1] = Pv.x;
            tamtaMacierz[0][2] = -Qs.x;
            tamtaMacierz[0][3] = -Qt.x;
            
            tamtaMacierz[1][0] = Pu.y;
            tamtaMacierz[1][1] = Pv.y;
            tamtaMacierz[1][2] = -Qs.y;
            tamtaMacierz[1][3] = -Qt.y;
            
            tamtaMacierz[2][0] = Pu.z;
            tamtaMacierz[2][1] = Pv.z;
            tamtaMacierz[2][2] = -Qs.z;
            tamtaMacierz[2][3] = -Qt.z;
            
            tamtaMacierz[3][0] = GLKVector3DotProduct(Pu, t);
            tamtaMacierz[3][1] = GLKVector3DotProduct(Pv, t);
            tamtaMacierz[3][2] = 0;
            tamtaMacierz[3][3] = 0;
            
            double wyniki[4];
            wyniki[0] = 0;
            wyniki[1] = 0;
            wyniki[2] = 0;
            wyniki[3] = d;
            
            gsl_matrix_view m
            = gsl_matrix_view_array ((double*)tamtaMacierz, 4, 4);
            
            gsl_vector_view b
            = gsl_vector_view_array (wyniki, 4);
            
            gsl_vector *x = gsl_vector_alloc (4);
            
            int s;
            
            gsl_permutation * p = gsl_permutation_alloc (4);
            
            gsl_linalg_LU_decomp (&m.matrix, p, &s);
            
            gsl_linalg_LU_solve (&m.matrix, p, &b.vector, x);
            
            gsl_permutation_free (p);
            float u1 = x->data[0];
            float v1 = x->data[1];
            float s1 = x->data[2];
            float t1 = x->data[3];
            gsl_vector_free (x);
            
            d/=2;
            
            [self gauss4EliminationFromMatrix:taMacierz];
            
            
            //float u1 = taMacierz[0][4];
            //float v1 = taMacierz[1][4];
            //float s1 = taMacierz[2][4];
            //float t1 = taMacierz[3][4];
            u0 += u1;
            v0 += v1;
            s0 += s1;
            t0 += t1;
            dist = fabsf(u1) + fabsf(v1) + fabsf(s1) + fabsf(t1);
            if (!(u0 > 0 && u0 < 1 && v0 > 0 && v0 < 1 &&
                  s0 > 0 && s0 < 1 && t0 > 0 && t0 < 1))
                break;
        }
        if (u0 > 0 && u0 < 1 && v0 > 0 && v0 < 1 &&
            s0 > 0 && s0 < 1 && t0 > 0 && t0 < 1) {
            
            [points addObject:[[AKPoint alloc]initAtPosition:[m_surface1 getPositionForU:u0 andForV:v0] andNumber:0]];
            lines++;
        } else {
            i=10000;
            break;
        }
    }
    
    
    int div = lines;
    v0 = m_v0;
    u0 = m_u0;
    s0 = m_s0;
    t0 = m_t0;
    for (int i=0; i<450; i++) {
        d = dValue;
        dist = 1000;
        while (dist > 0.001) {
            GLKVector4 duTmp1 = [m_surface1 getDivVForU:u0 andForV:v0];
            GLKVector4 dvTmp1 = [m_surface1 getDivUForU:u0 andForV:v0];
            
            GLKVector4 duTmp2 = [m_surface2 getDivVForU:s0 andForV:t0];
            GLKVector4 dvTmp2 = [m_surface2 getDivUForU:s0 andForV:t0];
            
            GLKVector3 Pu = GLKVector3Make(duTmp1.x, duTmp1.y, duTmp1.z);
            //Pu.x = 0;
            GLKVector3 Pv = GLKVector3Make(dvTmp1.x, dvTmp1.y, dvTmp1.z);
            
            GLKVector3 Qs = GLKVector3Make(duTmp2.x, duTmp2.y, duTmp2.z);
            GLKVector3 Qt = GLKVector3Make(dvTmp2.x, dvTmp2.y, dvTmp2.z);
            
            GLKVector3 Pn = GLKVector3CrossProduct(Pu, Pv);
            GLKVector3 Qn = GLKVector3CrossProduct(Qs, Qt);
            GLKVector3 t = GLKVector3MultiplyScalar(GLKVector3CrossProduct(Pn, Qn), 1);
            t = GLKVector3Normalize(t);
            
            float taMacierz[4][5];
            taMacierz[0][0] = Pu.x;
            taMacierz[0][1] = Pv.x;
            taMacierz[0][2] = -Qs.x;
            taMacierz[0][3] = -Qt.x;
            
            taMacierz[1][0] = Pu.y;
            taMacierz[1][1] = Pv.y;
            taMacierz[1][2] = -Qs.y;
            taMacierz[1][3] = -Qt.y;
            
            taMacierz[2][0] = Pu.z;
            taMacierz[2][1] = Pv.z;
            taMacierz[2][2] = -Qs.z;
            taMacierz[2][3] = -Qt.z;
            
            taMacierz[3][0] = GLKVector3DotProduct(Pu, t);
            taMacierz[3][1] = GLKVector3DotProduct(Pv, t);
            taMacierz[3][2] = 0;
            taMacierz[3][3] = 0;
            
            taMacierz[0][4] = 0;
            taMacierz[1][4] = 0;
            taMacierz[2][4] = 0;
            taMacierz[3][4] = d;
            
            double tamtaMacierz[4][4];
            tamtaMacierz[0][0] = Pu.x;
            tamtaMacierz[0][1] = Pv.x;
            tamtaMacierz[0][2] = -Qs.x;
            tamtaMacierz[0][3] = -Qt.x;
            
            tamtaMacierz[1][0] = Pu.y;
            tamtaMacierz[1][1] = Pv.y;
            tamtaMacierz[1][2] = -Qs.y;
            tamtaMacierz[1][3] = -Qt.y;
            
            tamtaMacierz[2][0] = Pu.z;
            tamtaMacierz[2][1] = Pv.z;
            tamtaMacierz[2][2] = -Qs.z;
            tamtaMacierz[2][3] = -Qt.z;
            
            tamtaMacierz[3][0] = GLKVector3DotProduct(Pu, t);
            tamtaMacierz[3][1] = GLKVector3DotProduct(Pv, t);
            tamtaMacierz[3][2] = 0;
            tamtaMacierz[3][3] = 0;
            
            double wyniki[4];
            wyniki[0] = 0;
            wyniki[1] = 0;
            wyniki[2] = 0;
            wyniki[3] = d;
            
            gsl_matrix_view m
            = gsl_matrix_view_array ((double*)tamtaMacierz, 4, 4);
            
            gsl_vector_view b
            = gsl_vector_view_array (wyniki, 4);
            
            gsl_vector *x = gsl_vector_alloc (4);
            
            int s;
            
            gsl_permutation * p = gsl_permutation_alloc (4);
            
            gsl_linalg_LU_decomp (&m.matrix, p, &s);
            
            gsl_linalg_LU_solve (&m.matrix, p, &b.vector, x);
            
            gsl_permutation_free (p);
            float u1 = x->data[0];
            float v1 = x->data[1];
            float s1 = x->data[2];
            float t1 = x->data[3];
            gsl_vector_free (x);
            
            d/=2;
            
            [self gauss4EliminationFromMatrix:taMacierz];
            
            
            //float u1 = taMacierz[0][4];
            //float v1 = taMacierz[1][4];
            //float s1 = taMacierz[2][4];
            //float t1 = taMacierz[3][4];
            u0 -= u1;
            v0 -= v1;
            s0 -= s1;
            t0 -= t1;
            dist = fabsf(u1) + fabsf(v1) + fabsf(s1) + fabsf(t1);
            if (!(u0 > 0 && u0 < 1 && v0 > 0 && v0 < 1 &&
                  s0 > 0 && s0 < 1 && t0 > 0 && t0 < 1))
                break;
        }
        if (u0 > 0 && u0 < 1 && v0 > 0 && v0 < 1 &&
            s0 > 0 && s0 < 1 && t0 > 0 && t0 < 1) {
            
            [points addObject:[[AKPoint alloc]initAtPosition:[m_surface1 getPositionForU:u0 andForV:v0] andNumber:0]];
            lines++;
        } else {
            i=10000;
            break;
        }
    }
    
    
    internalArraySize = lines+2;
    free(internalArray);
    internalArray = malloc(internalArraySize * sizeof(GLKVector4));
    
    
    internalIndicSize = internalArraySize-2;
    free(internalIndices);
    internalIndices = malloc(internalIndicSize * sizeof(indic));
    
    for (int i=0; i<internalArraySize; i++) {
        if (i == div+1) {
            internalArray[i] = [((AKPoint*)[points objectAtIndex:0])position];
            continue;
        } else if (i < div+1) {
            internalArray[i] = [((AKPoint*)[points objectAtIndex:i])position];
        } else {
            internalArray[i] = [((AKPoint*)[points objectAtIndex:i-1])position];
        }
    }
    for (int i=0; i<internalIndicSize+1; i++) {
        if (i == div) {
            continue;
        } else if (i < div) {
            internalIndices[i].from = i;
            internalIndices[i].to = i+1;
        } else {
            internalIndices[i-1].from = i;
            internalIndices[i-1].to = i+1;
        }
    }
    //startPosition2 = [m_surface2 getPositionForU:s0 andForV:t0];
}

- (bool) gauss4EliminationFromMatrix:(float[4][5])M {
    
    int rowCount = 4;
    
    // pivoting
    for (int col = 0; col + 1 < rowCount; col++)
        if (M[col ][ col] == 0)
        // check for zero coefficients
    {
        // find non-zero coefficient
        int swapRow = col + 1;
        for (;swapRow < rowCount; swapRow++) if (M[swapRow][col] != 0) break;
        
        if (M[swapRow][col] != 0) // found a non-zero coefficient?
        {
            // yes, then swap it with the above
            float tmp[rowCount + 1];
            for (int i = 0; i < rowCount + 1; i++)
            { tmp[i] = M[swapRow][i]; M[swapRow][i] = M[col][ i]; M[col][i] = tmp[i]; }
        }
        else return false; // no, then the matrix has no unique solution
    }
    
    // elimination
    for (int sourceRow = 0; sourceRow + 1 < rowCount; sourceRow++)
    {
        for (int destRow = sourceRow + 1; destRow < rowCount; destRow++)
        {
            float df = M[sourceRow][sourceRow];
            float sf = M[destRow][sourceRow];
            for (int i = 0; i < rowCount + 1; i++)
                M[destRow][i] = M[destRow][i] * df - M[sourceRow][i] * sf;
        }
    }
    
    // back-insertion
    for (int row = rowCount - 1; row >= 0; row--)
    {
        float f = M[row][row];
        if (f == 0) return false;
        
        for (int i = 0; i < rowCount + 1; i++) M[row][i] /= f;
        for (int destRow = 0; destRow < row; destRow++)
        { M[destRow][rowCount] -= M[destRow][row] * M[row][rowCount]; M[destRow][row] = 0; }
    }
    return true;
}

- (void) countStepsFromMatrix:(GLKMatrix4) M {
    draw = [self computeStartPosition];
    if (draw) {
        [self computeIntersectionCurve];
    }
}

-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize
    andArrayIndicOffset:(int *)offset {
    
    if (!draw) {
        return;
    }
    
    memcpy(array + *arrayPoint, internalArray, internalArraySize * sizeof(GLKVector4));
    
    GLushort *indic = (GLushort *)indices;
    GLushort *internalIndic = (GLushort *)internalIndices;
    
    for (int i=0; i < internalIndicSize*2; i++) {
        indic[*indicesSize + i] = internalIndic[i] + *offset;
    }
    
    *offset += internalArraySize;
    *arrayPoint += internalArraySize;
    *indicesSize += internalIndicSize*2;
    
}

- (int) linesCount {
    if (!draw) {
        return 0;
    }
    return lines;
}

- (int) curvesCount {
    return 1;
}

- (void) updateTrimDValue:(float)ddValue {
    dValue = ddValue;
}

@end
