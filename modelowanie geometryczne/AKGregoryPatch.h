//
//  AKGregoryPatch.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 07.06.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKObject.h"
#import "AKPoint.h"

@interface AKGregoryPatch : AKObject {
    
    int m_screenWidth;
    int m_screenHeight;
    
    GLKMatrix4 MOld;
    NSMutableArray *patch0;
    NSMutableArray *patch1;
    NSMutableArray *patch2;
    
    NSMutableArray *patch00;
    NSMutableArray *patch01;
    NSMutableArray *patch10;
    NSMutableArray *patch11;
    NSMutableArray *patch20;
    NSMutableArray *patch21;
    
    AKPoint *middlePoint;
    NSMutableArray *controlPoints;
    NSMutableArray *innerControlPoints;
    
    NSMutableArray *controlPoints2;
    NSMutableArray *innerControlPoints2;
    
    NSMutableArray *controlPoints3;
    NSMutableArray *innerControlPoints3;
    
    AKPoint *p0;
    AKPoint *p1;
    AKPoint *p2;
    
    int steps;
    int lines;
    int u;
    int v;
}

- (AKPoint*) pointToAdd;

- (AKGregoryPatch*) initWithC2BezierPatches:(NSMutableArray*)bezierPatches
                        screenWidth:(int)screenWidth
                       screenHeight:(int)screenHeight
                             number:(NSUInteger)number
                          andMatrix:(GLKMatrix4)M;

- (void)buildArraysFromPoint1:(AKPoint*)point1
                    andPoint2:(AKPoint*)point2
           andFromArrayPoints:(NSMutableArray*)pointsArray
                toPatchArrays:(NSMutableArray*)patchArray;

- (void)rebuildSmallPatches;
- (NSMutableArray*) splitLine:(NSMutableArray*)line;
- (NSMutableArray*) getControlPoints;
- (NSMutableArray*) getInternalControlPoints;
- (void)updateControlPointsPositions;
-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize
    andArrayIndicOffset:(int *)offset;
- (void) countStepsFromMatrix:(GLKMatrix4) M;
- (int) linesCount;
+ (void) setQualityDivisor:(int)divisor;
+ (int) qualityDivisor;
- (GLKVector4)Qu:(GLfloat)u v:(GLfloat)v withControlPoints:(NSMutableArray*)controlPoints andInnerControlPoints:(NSMutableArray*)innerControlPoints;
- (void) setUValue:(NSInteger)uValue andVValue:(NSInteger)vValue;

@end
