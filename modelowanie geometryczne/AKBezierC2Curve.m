//
//  AKBezierC2Curve.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 05.04.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKBezierC2Curve.h"

@implementation AKBezierC2Curve

static int m_qualityDivisor = 2;

- (AKBezierC2Curve*) initWithBernsteinPoints:(NSMutableArray*)bernsteinPointsArray
                                 screenWidth:(int)screenWidth
                                screenHeight:(int)screenHeight
                                      number:(NSUInteger)number
                                   andMatrix:(GLKMatrix4)M
                              andBsplineMode:(BOOL)bsplineMode {
    
    originalNodesInternalArray = [[NSMutableArray alloc]initWithArray:bernsteinPointsArray];
    name = [NSString stringWithFormat:@"Bezier CurveC2%lu", number];
    _bsplineNodesInternalArray = [[NSMutableArray alloc]init];
    
    
    [self preparePointsArray:originalNodesInternalArray];
    
    bernsteinMatrix.m00 = -1;
    bernsteinMatrix.m11 = -6;
    bernsteinMatrix.m22 = 0;
    bernsteinMatrix.m33 = 0;
    bernsteinMatrix.m01 = bernsteinMatrix.m10 = 3;
    bernsteinMatrix.m02 = bernsteinMatrix.m20 = -3;
    bernsteinMatrix.m03 = bernsteinMatrix.m30 = 1;
    bernsteinMatrix.m12 = bernsteinMatrix.m21 = 3;
    bernsteinMatrix.m13 = bernsteinMatrix.m31 = 0;
    bernsteinMatrix.m23 = bernsteinMatrix.m32 = 0;
    
    m_screenWidth = screenWidth;
    m_screenHeight = screenHeight;
    linesLamana = 0;
    _bsplineMode = bsplineMode;
    
    if (_bsplineMode) {
        [self switchToBsplineMode];
    }
    
    return self;
}

- (AKBezierC2Curve*) initWithBsplinePoints:(NSMutableArray*)bernsteinPointsArray
                               screenWidth:(int)screenWidth
                              screenHeight:(int)screenHeight
                                    number:(NSUInteger)number
                                 andMatrix:(GLKMatrix4)M {
    
    originalNodesInternalArray = [[NSMutableArray alloc]init];
    _nodesInternalArray = [[NSMutableArray alloc]init];
    for (int i=0; i<[bernsteinPointsArray count]; i++) {
        AKPoint *point = [bernsteinPointsArray objectAtIndex:i];
        AKPoint *pointToAdd = [[AKPoint alloc]initAtPosition:[point position] andNumber:0];
        [originalNodesInternalArray addObject:pointToAdd];
        [_nodesInternalArray addObject:pointToAdd];
    }
    //originalNodesInternalArray = [[NSMutableArray alloc]initWithArray:bernsteinPointsArray];
    name = [NSString stringWithFormat:@"Bezier CurveC2%lu", number];
    _bsplineNodesInternalArray = [[NSMutableArray alloc]initWithArray:bernsteinPointsArray];
    //[self preparePointsArray:originalNodesInternalArray];
    bernsteinMatrix.m00 = -1;
    bernsteinMatrix.m11 = -6;
    bernsteinMatrix.m22 = 0;
    bernsteinMatrix.m33 = 0;
    bernsteinMatrix.m01 = bernsteinMatrix.m10 = 3;
    bernsteinMatrix.m02 = bernsteinMatrix.m20 = -3;
    bernsteinMatrix.m03 = bernsteinMatrix.m30 = 1;
    bernsteinMatrix.m12 = bernsteinMatrix.m21 = 3;
    bernsteinMatrix.m13 = bernsteinMatrix.m31 = 0;
    bernsteinMatrix.m23 = bernsteinMatrix.m32 = 0;
    
    m_screenWidth = screenWidth;
    m_screenHeight = screenHeight;
    linesLamana = 0;
    _bsplineMode = true;
    segments = (int)[bernsteinPointsArray count] -3;
    [self refreshBernstein];
    //[self switchToBernsteinMode];
    //[self switchToBsplineMode];
    
    return self;
    
}

- (void) preparePointsArray:(NSArray*)pointsArray {
    if ([pointsArray count] <= 4) {
        _nodesInternalArray = [[NSMutableArray alloc]initWithArray:pointsArray];
        while ([_nodesInternalArray count]%4 != 0) {
            [_nodesInternalArray addObject:[_nodesInternalArray lastObject]];
        }
    } else {
        _nodesInternalArray = [[NSMutableArray alloc]init];
        
        AKPoint *point0 = [pointsArray objectAtIndex:0];
        [_nodesInternalArray addObject:point0];
        
        AKPoint *point1 = [pointsArray objectAtIndex:1];
        [_nodesInternalArray addObject:point1];
        
        AKPoint *point2 = [pointsArray objectAtIndex:2];
        [_nodesInternalArray addObject:point2];
        
        AKPoint *point3 = [pointsArray objectAtIndex:3];
        [_nodesInternalArray addObject:point3];
        
        AKPoint *point6;
        
        for (int i = 4; i < [pointsArray count]; i++) {
            
            point6 = [pointsArray objectAtIndex:i];
            
            GLKVector4 position;
            position.x = 2*[point3 position].x - [point2 position].x;
            position.y = 2*[point3 position].y - [point2 position].y;
            position.z = 2*[point3 position].z - [point2 position].z;
            position.w = 2*[point3 position].w - [point2 position].w;
            AKPoint *point4 = [[AKPoint alloc]initAtPosition:position andNumber:0];
            
            position.x = 2*[point4 position].x - (2*[point2 position].x - [point1 position].x);
            position.y = 2*[point4 position].y - (2*[point2 position].y - [point1 position].y);
            position.z = 2*[point4 position].z - (2*[point2 position].z - [point1 position].z);
            position.w = 2*[point4 position].w - (2*[point2 position].w - [point1 position].w);
            
            AKPoint *point5 = [[AKPoint alloc]initAtPosition:position andNumber:0];
            
            [_nodesInternalArray addObject:point4];
            [_nodesInternalArray addObject:point5];
            [_nodesInternalArray addObject:point6];
            
            point0 = point3;
            point1 = point4;
            point2 = point5;
            point3 = point6;
        }
    }
    
    nodesCount = (int)[_nodesInternalArray count];
    segments = nodesCount/3.0;
    
}

- (void) addPointsToCurve:(NSArray*)pointsArray {
    [originalNodesInternalArray addObjectsFromArray:pointsArray];
    [self preparePointsArray:originalNodesInternalArray];
    if (_bsplineMode) {
        [self switchToBsplineMode];
    }
}

- (void) addPointToCurve:(AKPoint*)pointsArray {
    [originalNodesInternalArray addObject:pointsArray];
    [self preparePointsArray:originalNodesInternalArray];
    if (_bsplineMode) {
        [self switchToBsplineMode];
    }
}

- (int) pointsCount {
    return self.pointsCount;
}

- (int) linesCount {
    return lines + linesLamana;
}

- (void) countStepsFromMatrix:(GLKMatrix4) M {
    linesLamana = 0;
    MOld = M;
    free(steps2);
    free(lines2);
    steps2 = malloc(segments * sizeof(int));
    lines2 = malloc(segments * sizeof(int));
    steps = 0;
    lines = 0;
    GLKVector4 pointsPositions[4*segments];
    
    for (int segment = 0; segment < segments; segment++) {
        float minX = MAXFLOAT;
        float minY = MAXFLOAT;
        float minZ = MAXFLOAT;
        float maxX = -MAXFLOAT;
        float maxY = -MAXFLOAT;
        float maxZ = -MAXFLOAT;
        
        for(int i=0; i<4; i++) {
            AKPoint* point = [_nodesInternalArray objectAtIndex:segment*3 +i];
            GLKVector4 pointPosition = GLKMatrix4MultiplyVector4(M, [point position]);
            //pointsPositions[4*segment + i] = GLKMatrix4MultiplyVector4(M, [point position]);
            pointsPositions[4*segment + i] = [point position];
            if (pointPosition.x < minX) {
                minX = pointPosition.x;
            }
            if (pointPosition.y < minY) {
                minY = pointPosition.y;
            }
            if (pointPosition.z < minZ) {
                minZ = pointPosition.z;
            }
            if (pointPosition.x > maxX) {
                maxX = pointPosition.x;
            }
            if (pointPosition.y > maxY) {
                maxY = pointPosition.y;
            }
            if (pointPosition.z > maxZ) {
                maxZ = pointPosition.z;
            }
        }
        
        double max = MAX(maxX-minX, maxY-minY);
        max = MAX(max, maxZ - minZ);
        
        steps2[segment] = m_screenWidth*(max/m_qualityDivisor);
        if(steps2[segment]<0) {
            steps2[segment]=0;
        }
        lines2[segment] = steps2[segment];
        
        steps += steps2[segment];
        lines += lines2[segment];
    }
    lines += segments-1;
    
    free(internalArray);
    free(internalIndices);
    internalArray = malloc((steps+segments)*sizeof(GLKVector4));
    internalArraySize = steps+segments;
    internalIndices = malloc((steps+segments-1) * sizeof(indic));
    internalIndicSize = steps+segments-1;
    
    int segmentOffset = 0;
    for (int segment = 0; segment < segments; segment++) {
        float step = 1.0f/steps2[segment];
        GLKMatrix4 pointsPositionMatrix = GLKMatrix4MakeWithColumns(pointsPositions[4*segment+0],
                                                                    pointsPositions[4*segment+1],
                                                                    pointsPositions[4*segment+2],
                                                                    pointsPositions[4*segment+3]);
        
        for (int i=0; i<=steps2[segment]; i++) {
            GLKVector4 tVec = GLKVector4Make((i*step)*(i*step)*(i*step), (i*step)*(i*step), (i*step), 1);
            GLKVector4 mulVec = GLKMatrix4MultiplyVector4(bernsteinMatrix, tVec);
            internalArray[i + segmentOffset] = GLKMatrix4MultiplyVector4(pointsPositionMatrix, mulVec);
        }
        segmentOffset += steps2[segment]+1;
    }
    
    for (int i=0; i<internalIndicSize; i++) {
        internalIndices[i].from = i;
        internalIndices[i].to = i+1;
    }
}

-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize
    andArrayIndicOffset:(int *)offset {
    
    memcpy(array + *arrayPoint, internalArray, internalArraySize * sizeof(GLKVector4));
    
    GLushort *indic = (GLushort *)indices;
    GLushort *internalIndic = (GLushort *)internalIndices;
    
    for (int i=0; i < internalIndicSize*2; i++) {
        indic[*indicesSize + i] = internalIndic[i] + *offset;
    }
    
    *offset += internalArraySize;
    *arrayPoint += internalArraySize;
    *indicesSize += internalIndicSize*2;
}

-(int)vertexArraySize {
    
    int sum = internalArraySize;
    
    if (_bsplineMode) {
        for (int i=0; i<[self.nodesInternalArray count]; i++) {
            AKObject* obj = [self.nodesInternalArray objectAtIndex:i];
            sum += [obj vertexArraySize];
        }
    }
    return sum;
}

-(int)indicArraySize {
    
    int sum = internalIndicSize*2;
    
    if (_bsplineMode) {
        for (int i=0; i<[self.nodesInternalArray count]; i++) {
            AKObject* obj = [self.nodesInternalArray objectAtIndex:i];
            sum += [obj indicArraySize];
        }
    }
    return sum;
}

- (void) deletePoint:(AKPoint*)point {
    [originalNodesInternalArray removeObject:point];
    [self preparePointsArray:originalNodesInternalArray];
    if (_bsplineMode) {
        [self switchToBsplineMode];
    }
}

- (void) displayLamana {
    
    if (_bsplineMode) {
        [self displayLamanaBspline];
    } else {
        [self displayLamanaBernstein];
    }
}

- (void) displayLamanaBernstein {
    GLKVector4 *internalArray2 = malloc((steps+segments+[_nodesInternalArray count])*sizeof(GLKVector4));
    indic *internalIndices2 = malloc((steps+segments+[_nodesInternalArray count]-1) * sizeof(indic));
    
    memcpy(internalArray2, internalArray, internalArraySize*sizeof(GLKVector4));
    memcpy(internalIndices2, internalIndices, internalIndicSize*sizeof(indic));
    
    for (int i=0; i<[_nodesInternalArray count]; i++) {
        AKPoint *point = [_nodesInternalArray objectAtIndex:i];
        internalArray2[steps+segments+i] = GLKMatrix4MultiplyVector4(MOld, [point position]);
        internalArray2[steps+segments+i] = [point position];
    }
    
    for (int i=0 ;i<[_nodesInternalArray count]-1; i++) {
        internalIndices2[internalIndicSize+i].from = internalIndicSize+i+1;
        internalIndices2[internalIndicSize+i].to = internalIndicSize+i+2;
    }
    
    free(internalArray);
    free(internalIndices);
    linesLamana =  (int)[_nodesInternalArray count]-1;
    internalArray = internalArray2;
    internalIndices = internalIndices2;
    
    internalArraySize = steps+segments+(int)[_nodesInternalArray count];
    internalIndicSize = steps+segments+(int)[_nodesInternalArray count]-2;
}

- (void) displayLamanaBspline {
    
    GLKVector4 *internalArray2 = malloc((steps+segments+[_bsplineNodesInternalArray count])*sizeof(GLKVector4));
    indic *internalIndices2 = malloc((steps+segments+[_bsplineNodesInternalArray count]-1) * sizeof(indic));
    
    memcpy(internalArray2, internalArray, internalArraySize*sizeof(GLKVector4));
    memcpy(internalIndices2, internalIndices, internalIndicSize*sizeof(indic));
    
    for (int i=0; i<[_bsplineNodesInternalArray count]; i++) {
        AKPoint *point = [_bsplineNodesInternalArray objectAtIndex:i];
        internalArray2[steps+segments+i] = GLKMatrix4MultiplyVector4(MOld, [point position]);
        internalArray2[steps+segments+i] = [point position];
    }
    
    for (int i=0 ;i<[_bsplineNodesInternalArray count]-1; i++) {
        internalIndices2[internalIndicSize+i].from = internalIndicSize+i+1;
        internalIndices2[internalIndicSize+i].to = internalIndicSize+i+2;
    }
    
    free(internalArray);
    free(internalIndices);
    linesLamana =  (int)[_bsplineNodesInternalArray count]-1;
    internalArray = internalArray2;
    internalIndices = internalIndices2;
    
    internalArraySize = steps+segments+(int)[_bsplineNodesInternalArray count];
    internalIndicSize = steps+segments+(int)[_bsplineNodesInternalArray count]-2;
}

- (void) refresh {
    [self preparePointsArray:originalNodesInternalArray];
}

-(void)refreshBernstein {
    
    if (!_bsplineMode) {
        return;
    }
    
    AKPoint *k0 = [_bsplineNodesInternalArray objectAtIndex:0];
    AKPoint *k1 = [_bsplineNodesInternalArray objectAtIndex:1];
    AKPoint *k2 = [_bsplineNodesInternalArray objectAtIndex:2];
    AKPoint *k3 = [_bsplineNodesInternalArray objectAtIndex:3];
    
    
    GLKVector4 position;
    
    position.x = [k1 position].x + ([k2 position].x - [k1 position].x)/3.0;
    position.y = [k1 position].y + ([k2 position].y - [k1 position].y)/3.0;
    position.z = [k1 position].z + ([k2 position].z - [k1 position].z)/3.0;
    position.w = [k1 position].w + ([k2 position].w - [k1 position].w)/3.0;
    AKPoint *b1 = [originalNodesInternalArray objectAtIndex:1];
    [b1 setPosition:position];
    
    position.x = [k1 position].x + 2*([k2 position].x - [k1 position].x)/3.0;
    position.y = [k1 position].y + 2*([k2 position].y - [k1 position].y)/3.0;
    position.z = [k1 position].z + 2*([k2 position].z - [k1 position].z)/3.0;
    position.w = [k1 position].w + 2*([k2 position].w - [k1 position].w)/3.0;
    AKPoint *b2 = [originalNodesInternalArray objectAtIndex:2];
    [b2 setPosition:position];
    
    position.x = [b1 position].x/2.0 + [k1 position].x/2.0 + ([k0 position].x - [k1 position].x)/6.0;
    position.y = [b1 position].y/2.0 + [k1 position].y/2.0 + ([k0 position].y - [k1 position].y)/6.0;
    position.z = [b1 position].z/2.0 + [k1 position].z/2.0 + ([k0 position].z - [k1 position].z)/6.0;
    position.w = [b1 position].w/2.0 + [k1 position].w/2.0 + ([k0 position].w - [k1 position].w)/6.0;
    AKPoint *b0 = [originalNodesInternalArray objectAtIndex:0];
    [b0 setPosition:position];
    
    position.x = [b2 position].x/2.0 + [k2 position].x/2.0 + ([k3 position].x - [k2 position].x)/6.0;
    position.y = [b2 position].y/2.0 + [k2 position].y/2.0 + ([k3 position].y - [k2 position].y)/6.0;
    position.z = [b2 position].z/2.0 + [k2 position].z/2.0 + ([k3 position].z - [k2 position].z)/6.0;
    position.w = [b2 position].w/2.0 + [k2 position].w/2.0 + ([k3 position].w - [k2 position].w)/6.0;
    AKPoint *b3 = [originalNodesInternalArray objectAtIndex:3];
    [b3 setPosition:position];
    
    for (int i=4; i < [originalNodesInternalArray count]; i++) {
        AKPoint *k4 = [_bsplineNodesInternalArray objectAtIndex:i];
        
        position.x = [k2 position].x/2.0 + [k3 position].x/2.0 + 2*([k3 position].x - [k2 position].x)/6.0 + ([k4 position].x - [k3 position].x)/6.0;
        position.y = [k2 position].y/2.0 + [k3 position].y/2.0 + 2*([k3 position].y - [k2 position].y)/6.0 + ([k4 position].y - [k3 position].y)/6.0;
        position.z = [k2 position].z/2.0 + [k3 position].z/2.0 + 2*([k3 position].z - [k2 position].z)/6.0 + ([k4 position].z - [k3 position].z)/6.0;
        position.w = [k2 position].w/2.0 + [k3 position].w/2.0 + 2*([k3 position].w - [k2 position].w)/6.0 + ([k4 position].w - [k3 position].w)/6.0;
        
        AKPoint *b6 = [originalNodesInternalArray objectAtIndex:i];
        [b6 setPosition:position];
        
        k2 = k3;
        k3 = k4;
    }
    
    [self refresh];
    
}

- (void) setBsplineModeWithBool:(bool)bsplineMode {
    if (!bsplineMode) {
        [self switchToBernsteinMode];
    } else {
        [self switchToBsplineMode];
    }
}

- (void) switchToBernsteinMode {
    _bsplineMode = false;
    [self refreshBernstein];
}

- (void) switchToBsplineMode {
    
    if ([_nodesInternalArray count] < 4) {
        return;
    }
    
    _bsplineMode = true;
    
    AKPoint *b0 = [originalNodesInternalArray objectAtIndex:0];
    AKPoint *b1 = [originalNodesInternalArray objectAtIndex:1];
    AKPoint *b2 = [originalNodesInternalArray objectAtIndex:2];
    AKPoint *b3 = [originalNodesInternalArray objectAtIndex:3];
    
    GLKVector4 position;
    
    position.x = 2*[b1 position].x - [b2 position].x;
    position.y = 2*[b1 position].y - [b2 position].y;
    position.z = 2*[b1 position].z - [b2 position].z;
    position.w = 2*[b1 position].w - [b2 position].w;
    AKPoint *k1 = [[AKPoint alloc]initAtPosition:position andNumber:0];
    
    position.x = 2*[b2 position].x - [b1 position].x;
    position.y = 2*[b2 position].y - [b1 position].y;
    position.z = 2*[b2 position].z - [b1 position].z;
    position.w = 2*[b2 position].w - [b1 position].w;
    AKPoint *k2 = [[AKPoint alloc]initAtPosition:position andNumber:0];
    
    position.x = 6*[b0 position].x - 3*[b1 position].x - 2*[k1 position].x;
    position.y = 6*[b0 position].y - 3*[b1 position].y - 2*[k1 position].y;
    position.z = 6*[b0 position].z - 3*[b1 position].z - 2*[k1 position].z;
    position.w = 6*[b0 position].w - 3*[b1 position].w - 2*[k1 position].w;
    AKPoint *k0 = [[AKPoint alloc]initAtPosition:position andNumber:0];
    
    
    position.x = 6*[b3 position].x - 3*[b2 position].x - 2*[k2 position].x;
    position.y = 6*[b3 position].y - 3*[b2 position].y - 2*[k2 position].y;
    position.z = 6*[b3 position].z - 3*[b2 position].z - 2*[k2 position].z;
    position.w = 6*[b3 position].w - 3*[b2 position].w - 2*[k2 position].w;
    AKPoint *k3 = [[AKPoint alloc]initAtPosition:position andNumber:0];
    
    [_bsplineNodesInternalArray removeAllObjects];
    [_bsplineNodesInternalArray addObject:k0];
    [_bsplineNodesInternalArray addObject:k1];
    [_bsplineNodesInternalArray addObject:k2];
    [_bsplineNodesInternalArray addObject:k3];
    
    
    for (int i=4; i < [originalNodesInternalArray count]; i++) {
        AKPoint *b6 = [originalNodesInternalArray objectAtIndex:i];
        
        
        position.x = 6*[b6 position].x - [k2 position].x - 4*[k3 position].x;
        position.y = 6*[b6 position].y - [k2 position].y - 4*[k3 position].y;
        position.z = 6*[b6 position].z - [k2 position].z - 4*[k3 position].z;
        position.w = 6*[b6 position].w - [k2 position].w - 4*[k3 position].w;
        
        AKPoint *k4 = [[AKPoint alloc]initAtPosition:position andNumber:0];
        [_bsplineNodesInternalArray addObject:k4];
        k2 = k3;
        k3 = k4;
        
    }
    
}

+ (void) setQualityDivisor:(int)divisor {
    if (divisor < 1) {
        divisor = 1;
    }
    m_qualityDivisor = divisor;
}

+ (int) qualityDivisor {
    return m_qualityDivisor;
}

@end
