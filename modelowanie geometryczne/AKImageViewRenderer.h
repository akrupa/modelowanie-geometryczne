//
//  AKImageViewRenderer.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 03.03.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AKImageViewRenderer : NSObject

- (id) initWithWidth:(int)width height:(int)height;
- (void) renderImage;
- (void) rotateX:(GLfloat)angle;
- (void) rotateY:(GLfloat)angle;
- (void) rotateZ:(GLfloat)angle;
- (void) moveX:(GLfloat)deltaX moveY:(GLfloat)deltaY moveZ:(GLfloat)deltaZ;
- (void) scale:(GLfloat)scale;
- (void) reset;
- (void) refreshWithM:(float)mValue a:(float)aValue b:(float)bValue c:(float)cValue scale:(int)mScale;


@end
