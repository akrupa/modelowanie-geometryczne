//
//  AKBezierCurve.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 24.03.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKBezierCurve.h"

@implementation AKBezierCurve

static int m_qualityDivisor = 2;

- (AKBezierCurve*) initWithPoints:(NSMutableArray*)pointsArray
                      screenWidth:(int)screenWidth
                     screenHeight:(int)screenHeight
                           number:(NSUInteger)number
                        andMatrix:(GLKMatrix4)M {
    
    
    
    originalNodesInternalArray = [[NSMutableArray alloc]initWithArray:pointsArray];
    name = [NSString stringWithFormat:@"Bezier Curve%lu", number];

    [self preparePointsArray:originalNodesInternalArray];
    
    bernsteinMatrix.m00 = -1;
    bernsteinMatrix.m11 = -6;
    bernsteinMatrix.m22 = 0;
    bernsteinMatrix.m33 = 0;
    bernsteinMatrix.m01 = bernsteinMatrix.m10 = 3;
    bernsteinMatrix.m02 = bernsteinMatrix.m20 = -3;
    bernsteinMatrix.m03 = bernsteinMatrix.m30 = 1;
    bernsteinMatrix.m12 = bernsteinMatrix.m21 = 3;
    bernsteinMatrix.m13 = bernsteinMatrix.m31 = 0;
    bernsteinMatrix.m23 = bernsteinMatrix.m32 = 0;
    
    m_screenWidth = screenWidth;
    m_screenHeight = screenHeight;
    linesLamana = 0;
    
    
    return self;
}

-(void)replacePoints:(NSMutableArray*)pointsToReplace withPoint:(AKObject*)point {

    
    for (int i=0; i<[pointsToReplace count]; i++) {
        AKPoint *point2 = pointsToReplace[i];
        NSUInteger index2 = [originalNodesInternalArray indexOfObject:point2];
        if (index2 == NSNotFound) {
            continue;
        }
        [originalNodesInternalArray replaceObjectAtIndex:index2 withObject:point];
    }
    [self preparePointsArray:originalNodesInternalArray];

}

- (void) preparePointsArray:(NSArray*)pointsArray {
    nodesInternalArray = [[NSMutableArray alloc]initWithArray:pointsArray];
    if ([nodesInternalArray count] <= 4) {
        while ([nodesInternalArray count]%4 != 0) {
            [nodesInternalArray addObject:[nodesInternalArray lastObject]];
        }
    } else {
        while (([nodesInternalArray count]-4)%3 != 0) {
            [nodesInternalArray addObject:[nodesInternalArray lastObject]];
        }
    }
    
    nodesCount = (int)[nodesInternalArray count];
    segments = nodesCount/3.0;

}

- (void) addPointsToCurve:(NSArray*)pointsArray {
    [originalNodesInternalArray addObjectsFromArray:pointsArray];
    [self preparePointsArray:originalNodesInternalArray];
}

- (void) addPointToCurve:(AKPoint*)pointsArray {
    [originalNodesInternalArray addObject:pointsArray];
    [self preparePointsArray:originalNodesInternalArray];
}

- (int) pointsCount {
    return self.pointsCount;
}

- (int) linesCount {
    return lines + linesLamana;
}

- (void) countStepsFromMatrix:(GLKMatrix4) M {
    linesLamana = 0;
    MOld = M;
    free(steps2);
    free(lines2);
    steps2 = malloc(segments * sizeof(int));
    lines2 = malloc(segments * sizeof(int));
    steps = 0;
    lines = 0;
    GLKVector4 pointsPositions[4*segments];
    
    for (int segment = 0; segment < segments; segment++) {
        float minX = MAXFLOAT;
        float minY = MAXFLOAT;
        float minZ = MAXFLOAT;
        float maxX = -MAXFLOAT;
        float maxY = -MAXFLOAT;
        float maxZ = -MAXFLOAT;
        
        for(int i=0; i<4; i++) {
            AKPoint* point = [nodesInternalArray objectAtIndex:segment*3 +i];
            GLKVector4 pointPosition = GLKMatrix4MultiplyVector4(M, [point position]);
            //pointsPositions[4*segment + i] = GLKMatrix4MultiplyVector4(M, [point position]);
            pointsPositions[4*segment + i] = [point position];
            if (pointPosition.x < minX) {
                minX = pointPosition.x;
            }
            if (pointPosition.y < minY) {
                minY = pointPosition.y;
            }
            if (pointPosition.z < minZ) {
                minZ = pointPosition.z;
            }
            if (pointPosition.x > maxX) {
                maxX = pointPosition.x;
            }
            if (pointPosition.y > maxY) {
                maxY = pointPosition.y;
            }
            if (pointPosition.z > maxZ) {
                maxZ = pointPosition.z;
            }
        }
        
        float max = MAX(maxX-minX, maxY-minY);
        max = MAX(max, maxZ - minZ);
        
        steps2[segment] = m_screenWidth*(max/m_qualityDivisor);
        lines2[segment] = steps2[segment];
        
        steps += steps2[segment];
        lines += lines2[segment];
    }
    lines += segments-1;
    
    free(internalArray);
    free(internalIndices);
    internalArray = malloc((steps+segments)*sizeof(GLKVector4));
    internalArraySize = steps+segments;
    internalIndices = malloc((steps+segments-1) * sizeof(indic));
    internalIndicSize = steps+segments-1;
    
    int segmentOffset = 0;
    for (int segment = 0; segment < segments; segment++) {
        float step = 1.0f/steps2[segment];
        GLKMatrix4 pointsPositionMatrix = GLKMatrix4MakeWithColumns(pointsPositions[4*segment+0],
                                                                    pointsPositions[4*segment+1],
                                                                    pointsPositions[4*segment+2],
                                                                    pointsPositions[4*segment+3]);
        
        for (int i=0; i<=steps2[segment]; i++) {
            GLKVector4 tVec = GLKVector4Make((i*step)*(i*step)*(i*step), (i*step)*(i*step), (i*step), 1);
            GLKVector4 mulVec = GLKMatrix4MultiplyVector4(bernsteinMatrix, tVec);
            internalArray[i + segmentOffset] = GLKMatrix4MultiplyVector4(pointsPositionMatrix, mulVec);
        }
        segmentOffset += steps2[segment]+1;
    }
    
    for (int i=0; i<internalIndicSize; i++) {
        internalIndices[i].from = i;
        internalIndices[i].to = i+1;
    }
}

-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize
    andArrayIndicOffset:(int *)offset {
        
    memcpy(array + *arrayPoint, internalArray, internalArraySize * sizeof(GLKVector4));
    
    GLushort *indic = (GLushort *)indices;
    GLushort *internalIndic = (GLushort *)internalIndices;
    
    for (int i=0; i < internalIndicSize*2; i++) {
        indic[*indicesSize + i] = internalIndic[i] + *offset;
    }
    
    *offset += internalArraySize;
    *arrayPoint += internalArraySize;
    *indicesSize += internalIndicSize*2;
}

-(int)vertexArraySize {
    return internalArraySize;
}

-(int)indicArraySize {
    return internalIndicSize*2;
}

- (void) deletePoint:(AKPoint*)point {
    [originalNodesInternalArray removeObject:point];
    [self preparePointsArray:originalNodesInternalArray];
}

- (void) displayLamana {

    
    
    GLKVector4 *internalArray2 = malloc((steps+segments+[originalNodesInternalArray count])*sizeof(GLKVector4));
    indic *internalIndices2 = malloc((steps+segments+[originalNodesInternalArray count]-1) * sizeof(indic));
    
    memcpy(internalArray2, internalArray, internalArraySize*sizeof(GLKVector4));
    memcpy(internalIndices2, internalIndices, internalIndicSize*sizeof(indic));
    
    for (int i=0; i<[originalNodesInternalArray count]; i++) {
        AKPoint *point = [nodesInternalArray objectAtIndex:i];
        internalArray2[steps+segments+i] = GLKMatrix4MultiplyVector4(MOld, [point position]);
        internalArray2[steps+segments+i] = [point position];
    }
    
    for (int i=0 ;i<[originalNodesInternalArray count]-1; i++) {
        internalIndices2[internalIndicSize+i].from = internalIndicSize+i+1;
        internalIndices2[internalIndicSize+i].to = internalIndicSize+i+2;
    }
    
    free(internalArray);
    free(internalIndices);
    linesLamana =  (int)[originalNodesInternalArray count]-1;
    internalArray = internalArray2;
    internalIndices = internalIndices2;
    
    internalArraySize = steps+segments+(int)[originalNodesInternalArray count];
    internalIndicSize = steps+segments+(int)[originalNodesInternalArray count]-2;
}

+ (void) setQualityDivisor:(int)divisor {
    if (divisor < 1) {
        divisor = 1;
    }
    m_qualityDivisor = divisor;
}

+ (int) qualityDivisor {
    return m_qualityDivisor;
}


@end
