//
//  AKBezierSurface.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 28.04.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKBezierSurface.h"
#import "AKBezierCurve.h"

@implementation AKBezierSurface

- (AKBezierSurface*) initWithPoints:(NSMutableArray*)pointsArray
                        screenWidth:(int)screenWidth
                       screenHeight:(int)screenHeight
                             number:(NSUInteger)number
                           onRoller:(BOOL)onRoller
                          withWidth:(int)width
                         withHeight:(int)height
                          andMatrix:(GLKMatrix4)M {
    
    if (!onRoller && [pointsArray count] >= 4 && ([pointsArray count]-4)%3 != 0) {
        NSLog(@"Error bezier surface array size");
        return nil;
    }
    name = [NSString stringWithFormat:@"Bezier surface%lu", number];

    bernsteinMatrix.m00 = -1;
    bernsteinMatrix.m11 = -6;
    bernsteinMatrix.m22 = 0;
    bernsteinMatrix.m33 = 0;
    bernsteinMatrix.m01 = bernsteinMatrix.m10 = 3;
    bernsteinMatrix.m02 = bernsteinMatrix.m20 = -3;
    bernsteinMatrix.m03 = bernsteinMatrix.m30 = 1;
    bernsteinMatrix.m12 = bernsteinMatrix.m21 = 3;
    bernsteinMatrix.m13 = bernsteinMatrix.m31 = 0;
    bernsteinMatrix.m23 = bernsteinMatrix.m32 = 0;
    
    m_screenWidth = screenWidth;
    m_screenHeight = screenHeight;
    
    nodesInternalArray = [[NSMutableArray alloc]initWithArray:pointsArray];

    nodesBezierInternalArray = [[NSMutableArray alloc]init];
    nodesBezierInternalArray2 = [[NSMutableArray alloc]init];
    bezierCurvesArray = [[NSMutableArray alloc] init];

    uSteps = 4;
    vSteps = 4;
    MOld = M;
    
    m_width = width;
    m_height = height;

    [self calculateArrays];
    
    return self;
}

-(void)replacePoints:(NSMutableArray*)pointsToReplace withPoint:(AKObject*)point {
    
    for (int i=0; i<[pointsToReplace count]; i++) {
        AKPoint *point2 = pointsToReplace[i];
        NSUInteger index2 = [nodesInternalArray indexOfObject:point2];
        if (index2 == NSNotFound) {
            continue;
        }
        [nodesInternalArray replaceObjectAtIndex:index2 withObject:point];
    }
    [self calculateArrays];
}

- (void) calculateArrays {
    GLKVector4 pos;
    [bezierCurvesArray removeAllObjects];
    [nodesBezierInternalArray removeAllObjects];
    [nodesBezierInternalArray2 removeAllObjects];
    
    for (int u = 0; u < vSteps*m_width; u++) {
        for (int i=0; i<uSteps+(m_height-1)*(uSteps-1); i++) {
            AKPoint *p = [[AKPoint alloc]initAtPosition:pos andNumber:0];
            [nodesBezierInternalArray addObject:p];
        }
    }
    
    for (int v = 0; v < vSteps*m_width; v++) {
        for (int i=0; i<uSteps+(m_height-1)*(uSteps-1); i++) {
            AKPoint *p2 = [[AKPoint alloc]initAtPosition:pos andNumber:0];
            [nodesBezierInternalArray2 addObject:p2];
        }
    }
    [self recalculateNodesBezierInternalArray];
    
    
    
    NSMutableArray *points = [[NSMutableArray alloc]init];
    
    for (int i=0; i<m_height; i++) {
        for (int u=0; u<uSteps; u++) {
            [points removeAllObjects];
            for (int j=0; j<4+(m_width-1)*3; j++) {
                int pos = i*(uSteps-1)+u+j*(uSteps + (m_height-1)*(uSteps-1));
                [points addObject:[nodesBezierInternalArray objectAtIndex:pos]];
            }
            [bezierCurvesArray addObject:[[AKBezierCurve alloc] initWithPoints:points screenWidth:m_screenWidth screenHeight:m_screenHeight number:u andMatrix:MOld]];
        }
    }
    
    for (int i=0; i<m_width; i++) {
        for (int v=0; v<vSteps; v++) {
            [points removeAllObjects];
            for (int j=0; j<4+(m_height-1)*3; j++) {
                int pos = v*(uSteps + (m_height-1)*(uSteps-1))+j + i*vSteps*(uSteps + (m_height-1)*(uSteps-1));
                [points addObject:[nodesBezierInternalArray2 objectAtIndex:pos]];
            }
            [bezierCurvesArray addObject:[[AKBezierCurve alloc] initWithPoints:points screenWidth:m_screenWidth screenHeight:m_screenHeight number:v andMatrix:MOld]];
        }
    }
}

- (NSMutableArray*) getPoints {
    return nodesInternalArray;
}

- (void) recalculateNodesBezierInternalArray {
    
    for (int i = 0; i < 4+(m_width-1)*3; i++) {
        for (int k=0; k < m_height; k++) {
            int offset = i*(4+(m_height-1)*3)+k*3;
            AKPoint *p0 = [nodesInternalArray objectAtIndex:offset+0];
            AKPoint *p1 = [nodesInternalArray objectAtIndex:offset+1];
            AKPoint *p2 = [nodesInternalArray objectAtIndex:offset+2];
            AKPoint *p3 = [nodesInternalArray objectAtIndex:offset+3];
            
            float step = 1.0f/(uSteps-1);
            GLKMatrix4 pointsPositionMatrix = GLKMatrix4MakeWithColumns([p0 position],
                                                                        [p1 position],
                                                                        [p2 position],
                                                                        [p3 position]);
            
            for (int j=0; j<uSteps; j++) {
                GLKVector4 tVec = GLKVector4Make((j*step)*(j*step)*(j*step), (j*step)*(j*step), (j*step), 1);
                GLKVector4 mulVec = GLKMatrix4MultiplyVector4(bernsteinMatrix, tVec);
                
                int offset2 = i*(uSteps+(m_height-1)*(uSteps-1))+k*(uSteps-1)+j;
                AKPoint *p = [nodesBezierInternalArray objectAtIndex:offset2];
                
                [p setPosition:GLKMatrix4MultiplyVector4(pointsPositionMatrix, mulVec)];
            }
        }
    }

    
    for (int i = 0; i < 4+(m_height-1)*3; i++) {
        for (int k=0; k < m_width; k++) {
            int offset = i+k*(4+(m_height-1)*3)*3;
            AKPoint *p0 = [nodesInternalArray objectAtIndex:offset+0];
            AKPoint *p1 = [nodesInternalArray objectAtIndex:offset+(4+(m_height-1)*3)];
            AKPoint *p2 = [nodesInternalArray objectAtIndex:offset+2*(4+(m_height-1)*3)];
            AKPoint *p3 = [nodesInternalArray objectAtIndex:offset+3*(4+(m_height-1)*3)];
            
            float step = 1.0f/(vSteps-1);
            GLKMatrix4 pointsPositionMatrix = GLKMatrix4MakeWithColumns([p0 position],
                                                                        [p1 position],
                                                                        [p2 position],
                                                                        [p3 position]);
            
            for (int j=0; j<vSteps; j++) {
                GLKVector4 tVec = GLKVector4Make((j*step)*(j*step)*(j*step), (j*step)*(j*step), (j*step), 1);
                GLKVector4 mulVec = GLKMatrix4MultiplyVector4(bernsteinMatrix, tVec);
                
                int offset2 =  i+j*(uSteps+(m_height-1)*(uSteps-1))+k*vSteps*(uSteps+(m_height-1)*(uSteps-1));
                AKPoint *p = [nodesBezierInternalArray2 objectAtIndex:offset2];
                
                [p setPosition:GLKMatrix4MultiplyVector4(pointsPositionMatrix, mulVec)];
            }
        }
    }

}

- (void) countStepsFromMatrix:(GLKMatrix4) M {
    linesLamana = 0;
    [self recalculateNodesBezierInternalArray];
    for (int i=0; i < [bezierCurvesArray count]; i++) {
        AKBezierCurve *curve = [bezierCurvesArray objectAtIndex:i];
        [curve countStepsFromMatrix:M];
    }
}

-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize
    andArrayIndicOffset:(int *)offset {
    
    for (int i=0; i < [bezierCurvesArray count]; i++) {
        AKBezierCurve *curve = [bezierCurvesArray objectAtIndex:i];
        [curve addObjectToArray:array atArrayPoint:arrayPoint andIndices:indices atIndicesPoint:indicesSize andArrayIndicOffset:offset];
    }
    
    memcpy(array + *arrayPoint, internalArray, internalArraySize * sizeof(GLKVector4));
    
    GLushort *indic = (GLushort *)indices;
    GLushort *internalIndic = (GLushort *)internalIndices;
    
    for (int i=0; i < internalIndicSize*2; i++) {
        indic[*indicesSize + i] = internalIndic[i] + *offset;
    }
    
    *offset += internalArraySize;
    *arrayPoint += internalArraySize;
    *indicesSize += internalIndicSize*2;
    
}

- (int) linesCount {
    int linesC = 0;
    for (int i=0; i < [bezierCurvesArray count]; i++) {
        AKBezierCurve *curve = [bezierCurvesArray objectAtIndex:i];
        linesC += [curve linesCount];
    }
    return linesC + linesLamana;
}

- (int) curvesCount {
    return (int)[bezierCurvesArray count];
}

-(int)vertexArraySize {
    int sum = 0;
    for (int i=0; i < [bezierCurvesArray count]; i++) {
        AKBezierCurve *curve = [bezierCurvesArray objectAtIndex:i];
        sum += [curve vertexArraySize];
    }
    return sum;
}

-(int)indicArraySize {
    int sum = 0;
    for (int i=0; i < [bezierCurvesArray count]; i++) {
        AKBezierCurve *curve = [bezierCurvesArray objectAtIndex:i];
        sum += [curve indicArraySize];
    }
    return sum;
}

- (void) setUValue:(NSInteger)uValue andVValue:(NSInteger)vValue {
    uSteps = (int)uValue;
    vSteps = (int)vValue;
    [self calculateArrays];
}

- (void) displayLamana {
    linesLamana  = 3*m_height*(4+3*(m_width-1))+3*m_width*(4+3*(m_height-1));
    
    if(internalArray)
        free(internalArray);
    if (internalIndices) {
        free(internalIndices);
    }
    
    internalArray = malloc(linesLamana*2*sizeof(GLKVector4));
    internalIndices = malloc(linesLamana*sizeof(indic));
    
    internalArraySize = linesLamana*2;
    internalIndicSize = linesLamana;
    int x = 0;
    
    for (int i=0; i<[nodesInternalArray count]; i++) {
        if (i+1 < [nodesInternalArray count]) {
            internalIndices[x].from = i;
            internalIndices[x].to = i+1;
            AKPoint *pointFrom = [nodesInternalArray objectAtIndex:i];
            AKPoint *pointTo = [nodesInternalArray objectAtIndex:i+1];
            internalArray[2*x] = [pointFrom position];
            internalArray[2*x+1] = [pointTo position];
            x++;
        }
        if (i+uSteps+(m_height-1)*(uSteps-1) < [nodesInternalArray count]) {
            internalIndices[x].from = i;
            internalIndices[x].to = i+uSteps+(m_height-1)*(uSteps-1);
            AKPoint *pointFrom = [nodesInternalArray objectAtIndex:i];
            AKPoint *pointTo = [nodesInternalArray objectAtIndex:i+uSteps+(m_height-1)*(uSteps-1)];
            internalArray[2*x] = [pointFrom position];
            internalArray[2*x+1] = [pointTo position];
            x++;
        }
    }
}



@end
