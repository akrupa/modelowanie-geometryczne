#version 410

in vec4 position;

void main()
{
    gl_Position = position;
    if (gl_Position.z > 1) {
        gl_Position.z = 1;
    }
}