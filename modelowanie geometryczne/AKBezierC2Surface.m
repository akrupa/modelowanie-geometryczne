//
//  AKBezierC2Surface.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 11.05.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKBezierC2Surface.h"
#import "AKBezierC2Curve.h"
#import <math.h>

@implementation AKBezierC2Surface

static int bezierC2surfaceCount = 0;

- (AKBezierC2Surface*) initWithPoints:(NSMutableArray*)pointsArray
                          screenWidth:(int)screenWidth
                         screenHeight:(int)screenHeight
                            withWidth:(int)width
                           withHeight:(int)height
                            andMatrix:(GLKMatrix4)M {
    
    name = [NSString stringWithFormat:@"BezierC2 surface%d", bezierC2surfaceCount++];
    
    bernsteinMatrix.m00 = -1;
    bernsteinMatrix.m11 = -6;
    bernsteinMatrix.m22 = 0;
    bernsteinMatrix.m33 = 0;
    bernsteinMatrix.m01 = bernsteinMatrix.m10 = 3;
    bernsteinMatrix.m02 = bernsteinMatrix.m20 = -3;
    bernsteinMatrix.m03 = bernsteinMatrix.m30 = 1;
    bernsteinMatrix.m12 = bernsteinMatrix.m21 = 3;
    bernsteinMatrix.m13 = bernsteinMatrix.m31 = 0;
    bernsteinMatrix.m23 = bernsteinMatrix.m32 = 0;
    
    bernsteinDivMatrix.m00 = 0;
    bernsteinDivMatrix.m01 = 0;
    bernsteinDivMatrix.m02 = 0;
    bernsteinDivMatrix.m03 = 0;
    
    bernsteinDivMatrix.m10 = -3;
    bernsteinDivMatrix.m11 = 9;
    bernsteinDivMatrix.m12 = -9;
    bernsteinDivMatrix.m13 = 0;
    
    bernsteinDivMatrix.m20 = 6;
    bernsteinDivMatrix.m21 = -12;
    bernsteinDivMatrix.m22 = 6;
    bernsteinDivMatrix.m23 = 3;
    
    bernsteinDivMatrix.m30 = -3;
    bernsteinDivMatrix.m31 = 3;
    bernsteinDivMatrix.m32 = 0;
    bernsteinDivMatrix.m33 = 0;
    
    
    m_screenWidth = screenWidth;
    m_screenHeight = screenHeight;
    
    _nodesInternalArray = [[NSMutableArray alloc]initWithArray:pointsArray];
    
    nodesBezierInternalArray = [[NSMutableArray alloc]init];
    nodesBezierInternalArray2 = [[NSMutableArray alloc]init];
    bezierCurvesArray = [[NSMutableArray alloc] init];
    
    uSteps = 4;
    vSteps = 4;
    MOld = M;
    
    m_width = width;
    m_height = height;
    
    [self calculateArrays];
    
    return self;
}

-(void)replacePoints:(NSMutableArray*)pointsToReplace withPoint:(AKObject*)point {
    
    for (int i=0; i<[pointsToReplace count]; i++) {
        AKPoint *point2 = pointsToReplace[i];
        NSUInteger index2 = [_nodesInternalArray indexOfObject:point2];
        if (index2 == NSNotFound) {
            continue;
        }
        [_nodesInternalArray replaceObjectAtIndex:index2 withObject:point];
    }
    [self calculateArrays];
}

- (void) calculateArrays {
    GLKVector4 pos;
    [bezierCurvesArray removeAllObjects];
    [nodesBezierInternalArray removeAllObjects];
    [nodesBezierInternalArray2 removeAllObjects];
    
    for (int u = 0; u < vSteps*m_width; u++) {
        for (int i=0; i<uSteps+(m_height-1)*(uSteps-1); i++) {
            AKPoint *p = [[AKPoint alloc]initAtPosition:pos andNumber:0];
            [nodesBezierInternalArray addObject:p];
        }
    }
    
    for (int v = 0; v < vSteps*m_width; v++) {
        for (int i=0; i<uSteps+(m_height-1)*(uSteps-1); i++) {
            AKPoint *p2 = [[AKPoint alloc]initAtPosition:pos andNumber:0];
            [nodesBezierInternalArray2 addObject:p2];
        }
    }
    [self recalculateNodesBezierInternalArray];
    
    
    
    NSMutableArray *points = [[NSMutableArray alloc]init];
    
    for (int i=0; i<m_height; i++) {
        for (int u=0; u<uSteps; u++) {
            [points removeAllObjects];
            for (int j=0; j<3+m_width; j++) {
                int pos = i*(uSteps-1)+u+j*(uSteps + (m_height-1)*(uSteps-1));
                [points addObject:[nodesBezierInternalArray objectAtIndex:pos]];
            }
            [bezierCurvesArray addObject:[[AKBezierC2Curve alloc]initWithBsplinePoints:points screenWidth:m_screenWidth screenHeight:m_screenHeight number:u andMatrix:MOld]];
        }
    }
    
    for (int i=0; i<m_width; i++) {
        for (int v=0; v<vSteps; v++) {
            [points removeAllObjects];
            for (int j=0; j<3+m_height; j++) {
                int pos = v*(3+m_height) + j + i*(3+m_height)*vSteps;
                [points addObject:[nodesBezierInternalArray2 objectAtIndex:pos]];
            }
            [bezierCurvesArray addObject:[[AKBezierC2Curve alloc]initWithBsplinePoints:points screenWidth:m_screenWidth screenHeight:m_screenHeight number:v andMatrix:MOld]];
        }
    }
}

- (void) recalculateNodesBezierInternalArray {
    
    for (int i = 0; i < 3+m_width; i++) {
        for (int k=0; k < m_height; k++) {
            int offset = i*(3+m_height)+k;
            AKPoint *p0 = [_nodesInternalArray objectAtIndex:offset+0];
            AKPoint *p1 = [_nodesInternalArray objectAtIndex:offset+1];
            AKPoint *p2 = [_nodesInternalArray objectAtIndex:offset+2];
            AKPoint *p3 = [_nodesInternalArray objectAtIndex:offset+3];
            
            GLKVector4 position0;
            GLKVector4 position1;
            GLKVector4 position2;
            GLKVector4 position3;
            
            position1.x = [p1 position].x + ([p2 position].x - [p1 position].x)/3.0;
            position1.y = [p1 position].y + ([p2 position].y - [p1 position].y)/3.0;
            position1.z = [p1 position].z + ([p2 position].z - [p1 position].z)/3.0;
            position1.w = [p1 position].w + ([p2 position].w - [p1 position].w)/3.0;
            
            position2.x = [p1 position].x + 2*([p2 position].x - [p1 position].x)/3.0;
            position2.y = [p1 position].y + 2*([p2 position].y - [p1 position].y)/3.0;
            position2.z = [p1 position].z + 2*([p2 position].z - [p1 position].z)/3.0;
            position2.w = [p1 position].w + 2*([p2 position].w - [p1 position].w)/3.0;
            
            position0.x = position1.x/2.0 + [p1 position].x/2.0 + ([p0 position].x - [p1 position].x)/6.0;
            position0.y = position1.y/2.0 + [p1 position].y/2.0 + ([p0 position].y - [p1 position].y)/6.0;
            position0.z = position1.z/2.0 + [p1 position].z/2.0 + ([p0 position].z - [p1 position].z)/6.0;
            position0.w = position1.w/2.0 + [p1 position].w/2.0 + ([p0 position].w - [p1 position].w)/6.0;
            
            position3.x = position2.x/2.0 + [p2 position].x/2.0 + ([p3 position].x - [p2 position].x)/6.0;
            position3.y = position2.y/2.0 + [p2 position].y/2.0 + ([p3 position].y - [p2 position].y)/6.0;
            position3.z = position2.z/2.0 + [p2 position].z/2.0 + ([p3 position].z - [p2 position].z)/6.0;
            position3.w = position2.w/2.0 + [p2 position].w/2.0 + ([p3 position].w - [p2 position].w)/6.0;
            
            float step = 1.0f/(uSteps-1);
            GLKMatrix4 pointsPositionMatrix = GLKMatrix4MakeWithColumns(position0,
                                                                        position1,
                                                                        position2,
                                                                        position3);
            
            for (int j=0; j<uSteps; j++) {
                GLKVector4 tVec = GLKVector4Make((j*step)*(j*step)*(j*step), (j*step)*(j*step), (j*step), 1);
                GLKVector4 mulVec = GLKMatrix4MultiplyVector4(bernsteinMatrix, tVec);
                
                int offset2 = i*(uSteps+(m_height-1)*(uSteps-1))+k*(uSteps-1)+j;
                AKPoint *p = [nodesBezierInternalArray objectAtIndex:offset2];
                
                [p setPosition:GLKMatrix4MultiplyVector4(pointsPositionMatrix, mulVec)];
            }
        }
    }
    
    
    for (int i = 0; i < 3+m_height; i++) {
        for (int k=0; k < m_width; k++) {
            int offset = i+k*(3+m_height);
            AKPoint *p0 = [_nodesInternalArray objectAtIndex:offset+0];
            AKPoint *p1 = [_nodesInternalArray objectAtIndex:offset+(3+m_height)];
            AKPoint *p2 = [_nodesInternalArray objectAtIndex:offset+2*(3+m_height)];
            AKPoint *p3 = [_nodesInternalArray objectAtIndex:offset+3*(3+m_height)];
            
            GLKVector4 position0;
            GLKVector4 position1;
            GLKVector4 position2;
            GLKVector4 position3;
            
            position1.x = [p1 position].x + ([p2 position].x - [p1 position].x)/3.0;
            position1.y = [p1 position].y + ([p2 position].y - [p1 position].y)/3.0;
            position1.z = [p1 position].z + ([p2 position].z - [p1 position].z)/3.0;
            position1.w = [p1 position].w + ([p2 position].w - [p1 position].w)/3.0;
            
            position2.x = [p1 position].x + 2*([p2 position].x - [p1 position].x)/3.0;
            position2.y = [p1 position].y + 2*([p2 position].y - [p1 position].y)/3.0;
            position2.z = [p1 position].z + 2*([p2 position].z - [p1 position].z)/3.0;
            position2.w = [p1 position].w + 2*([p2 position].w - [p1 position].w)/3.0;
            
            position0.x = position1.x/2.0 + [p1 position].x/2.0 + ([p0 position].x - [p1 position].x)/6.0;
            position0.y = position1.y/2.0 + [p1 position].y/2.0 + ([p0 position].y - [p1 position].y)/6.0;
            position0.z = position1.z/2.0 + [p1 position].z/2.0 + ([p0 position].z - [p1 position].z)/6.0;
            position0.w = position1.w/2.0 + [p1 position].w/2.0 + ([p0 position].w - [p1 position].w)/6.0;
            
            position3.x = position2.x/2.0 + [p2 position].x/2.0 + ([p3 position].x - [p2 position].x)/6.0;
            position3.y = position2.y/2.0 + [p2 position].y/2.0 + ([p3 position].y - [p2 position].y)/6.0;
            position3.z = position2.z/2.0 + [p2 position].z/2.0 + ([p3 position].z - [p2 position].z)/6.0;
            position3.w = position2.w/2.0 + [p2 position].w/2.0 + ([p3 position].w - [p2 position].w)/6.0;
            
            float step = 1.0f/(vSteps-1);
            GLKMatrix4 pointsPositionMatrix = GLKMatrix4MakeWithColumns(position0,
                                                                        position1,
                                                                        position2,
                                                                        position3);
            
            for (int j=0; j<vSteps; j++) {
                GLKVector4 tVec = GLKVector4Make((j*step)*(j*step)*(j*step), (j*step)*(j*step), (j*step), 1);
                GLKVector4 mulVec = GLKMatrix4MultiplyVector4(bernsteinMatrix, tVec);
                
                int offset2 =  i+j*(3+m_height) + k*(3+m_height)*(vSteps-1);
                AKPoint *p = [nodesBezierInternalArray2 objectAtIndex:offset2];
                
                [p setPosition:GLKMatrix4MultiplyVector4(pointsPositionMatrix, mulVec)];
            }
        }
    }
    
    
}

- (void) countStepsFromMatrix:(GLKMatrix4) M {
    internalArraySize = 0;
    internalIndicSize = 0;
    linesLamana = 0;
    [self recalculateNodesBezierInternalArray];
    for (int i=0; i < [bezierCurvesArray count]; i++) {
        AKBezierC2Curve *curve = [bezierCurvesArray objectAtIndex:i];
        [curve refreshBernstein];
        [curve countStepsFromMatrix:M];
    }
}

-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize
    andArrayIndicOffset:(int *)offset {
    
    for (int i=0; i < [bezierCurvesArray count]; i++) {
        AKBezierC2Curve *curve = [bezierCurvesArray objectAtIndex:i];
        [curve addObjectToArray:array atArrayPoint:arrayPoint andIndices:indices atIndicesPoint:indicesSize andArrayIndicOffset:offset];
    }
    
    memcpy(array + *arrayPoint, internalArray, internalArraySize * sizeof(GLKVector4));
    
    GLushort *indic = (GLushort *)indices;
    GLushort *internalIndic = (GLushort *)internalIndices;
    
    for (int i=0; i < internalIndicSize*2; i++) {
        indic[*indicesSize + i] = internalIndic[i] + *offset;
    }
    
    *offset += internalArraySize;
    *arrayPoint += internalArraySize;
    *indicesSize += internalIndicSize*2;
    
}


- (int) linesCount {
    int linesC = 0;
    for (int i=0; i < [bezierCurvesArray count]; i++) {
        AKBezierC2Curve *curve = [bezierCurvesArray objectAtIndex:i];
        linesC += [curve linesCount];
    }
    return linesC + linesLamana;
}

- (int) curvesCount {
    return (int)[bezierCurvesArray count];
}

-(int)vertexArraySize {
    int sum = 0;
    for (int i=0; i < [bezierCurvesArray count]; i++) {
        AKBezierC2Curve *curve = [bezierCurvesArray objectAtIndex:i];
        sum += [curve vertexArraySize];
    }
    return sum;
}

-(int)indicArraySize {
    int sum = 0;
    for (int i=0; i < [bezierCurvesArray count]; i++) {
        AKBezierC2Curve *curve = [bezierCurvesArray objectAtIndex:i];
        sum += [curve indicArraySize];
    }
    return sum;
}

- (void) setUValue:(NSInteger)uValue andVValue:(NSInteger)vValue {
    uSteps = (int)uValue;
    vSteps = (int)vValue;
    [self calculateArrays];
}

- (void) displayLamana {
    linesLamana  = (2+m_height)*(3+m_width) + (2+m_width)*(3+m_height);
    
    if(internalArray)
        free(internalArray);
    if (internalIndices) {
        free(internalIndices);
    }
    
    internalArray = malloc(linesLamana*2*sizeof(GLKVector4));
    internalIndices = malloc(linesLamana*sizeof(indic));
    
    internalArraySize = linesLamana*2;
    internalIndicSize = linesLamana;
    int x = 0;
    
    for (int i=0; i<3+m_width; i++) {
        for (int j=0; j<2+m_height; j++) {
            int offset = i*(3+m_height)+j;
            internalIndices[x].from = 2*x;
            internalIndices[x].to = 2*x+1;
            AKPoint *pointFrom = [_nodesInternalArray objectAtIndex:offset];
            AKPoint *pointTo = [_nodesInternalArray objectAtIndex:offset+1];
            internalArray[2*x] = [pointFrom position];
            internalArray[2*x+1] = [pointTo position];
            x++;
        }
    }
    
    for (int i=0; i<3+m_height; i++) {
        for (int j=0; j<2+m_width; j++) {
            int offset = j*(3+m_height)+i;
            internalIndices[x].from = 2*x;
            internalIndices[x].to = 2*x+1;
            AKPoint *pointFrom = [_nodesInternalArray objectAtIndex:offset];
            AKPoint *pointTo = [_nodesInternalArray objectAtIndex:offset+(3+m_height)];
            internalArray[2*x] = [pointFrom position];
            internalArray[2*x+1] = [pointTo position];
            x++;
        }
    }
}

- (NSNumber*) rows {
    return [[NSNumber alloc]initWithInt:m_height];
}

- (NSNumber*) columns {
    return [[NSNumber alloc]initWithInt:m_width];
}

- (GLKVector4) getPositionForU:(GLfloat)u andForV:(GLfloat)v {
    u-= 0.00000001f;
    v-= 0.00000001f;
    v *= m_width;
    u *= m_height;
    
    GLKVector4 BsplineIntNewArray[16];
    for (int i=0; i<4; i++) {
        int offset = ((int)v)*(4+m_height-1)+ i*(4+m_height-1) + (int)u;
        for (int j=0; j<4; j++) {
            BsplineIntNewArray[i*4+j] = [((AKPoint*)[_nodesInternalArray objectAtIndex:offset+j]) position];
        }
    }
    
    GLKVector4 bezierIntNewArray[16];
    for (int i=0; i<4; i++) {
        GLKVector4 p0 = BsplineIntNewArray[i*4+0];
        GLKVector4 p1 = BsplineIntNewArray[i*4+1];
        GLKVector4 p2 = BsplineIntNewArray[i*4+2];
        GLKVector4 p3 = BsplineIntNewArray[i*4+3];
        
        GLKVector4 position0;
        GLKVector4 position1;
        GLKVector4 position2;
        GLKVector4 position3;
        
        position1.x = p1.x + (p2.x - p1.x)/3.0;
        position1.y = p1.y + (p2.y - p1.y)/3.0;
        position1.z = p1.z + (p2.z - p1.z)/3.0;
        position1.w = p1.w + (p2.w - p1.w)/3.0;
        
        position2.x = p1.x + 2*(p2.x - p1.x)/3.0;
        position2.y = p1.y + 2*(p2.y - p1.y)/3.0;
        position2.z = p1.z + 2*(p2.z - p1.z)/3.0;
        position2.w = p1.w + 2*(p2.w - p1.w)/3.0;
        
        position0.x = position1.x/2.0 + p1.x/2.0 + (p0.x - p1.x)/6.0;
        position0.y = position1.y/2.0 + p1.y/2.0 + (p0.y - p1.y)/6.0;
        position0.z = position1.z/2.0 + p1.z/2.0 + (p0.z - p1.z)/6.0;
        position0.w = position1.w/2.0 + p1.w/2.0 + (p0.w - p1.w)/6.0;
        
        position3.x = position2.x/2.0 + p2.x/2.0 + (p3.x - p2.x)/6.0;
        position3.y = position2.y/2.0 + p2.y/2.0 + (p3.y - p2.y)/6.0;
        position3.z = position2.z/2.0 + p2.z/2.0 + (p3.z - p2.z)/6.0;
        position3.w = position2.w/2.0 + p2.w/2.0 + (p3.w - p2.w)/6.0;
        
        bezierIntNewArray[i*4+0] = position0;
        bezierIntNewArray[i*4+1] = position1;
        bezierIntNewArray[i*4+2] = position2;
        bezierIntNewArray[i*4+3] = position3;
    }
    
    GLKVector4 bezierNewArray[16];
    for (int i=0; i<4; i++) {
        GLKVector4 p0 = bezierIntNewArray[i+0];
        GLKVector4 p1 = bezierIntNewArray[i+4];
        GLKVector4 p2 = bezierIntNewArray[i+8];
        GLKVector4 p3 = bezierIntNewArray[i+12];
        
        GLKVector4 position0;
        GLKVector4 position1;
        GLKVector4 position2;
        GLKVector4 position3;
        
        position1.x = p1.x + (p2.x - p1.x)/3.0;
        position1.y = p1.y + (p2.y - p1.y)/3.0;
        position1.z = p1.z + (p2.z - p1.z)/3.0;
        position1.w = p1.w + (p2.w - p1.w)/3.0;
        
        position2.x = p1.x + 2*(p2.x - p1.x)/3.0;
        position2.y = p1.y + 2*(p2.y - p1.y)/3.0;
        position2.z = p1.z + 2*(p2.z - p1.z)/3.0;
        position2.w = p1.w + 2*(p2.w - p1.w)/3.0;
        
        position0.x = position1.x/2.0 + p1.x/2.0 + (p0.x - p1.x)/6.0;
        position0.y = position1.y/2.0 + p1.y/2.0 + (p0.y - p1.y)/6.0;
        position0.z = position1.z/2.0 + p1.z/2.0 + (p0.z - p1.z)/6.0;
        position0.w = position1.w/2.0 + p1.w/2.0 + (p0.w - p1.w)/6.0;
        
        position3.x = position2.x/2.0 + p2.x/2.0 + (p3.x - p2.x)/6.0;
        position3.y = position2.y/2.0 + p2.y/2.0 + (p3.y - p2.y)/6.0;
        position3.z = position2.z/2.0 + p2.z/2.0 + (p3.z - p2.z)/6.0;
        position3.w = position2.w/2.0 + p2.w/2.0 + (p3.w - p2.w)/6.0;
        
        bezierNewArray[i+0] = position0;
        bezierNewArray[i+4] = position1;
        bezierNewArray[i+8] = position2;
        bezierNewArray[i+12] = position3;
    }
    
    
    GLKVector4 position = GLKVector4Make(0, 0, 0, 1);
    
    //NSArray *bezierArray = [nodesBezierInternalArray2 subarrayWithRange:NSMakeRange(offset*16, 16)];
    NSMutableArray *bezierArray = [[NSMutableArray alloc]init];
    
    for (int i=0; i<4; i++) {
        
        int offset = (3+3*((int)v-1))*(4+m_height-1)+ i*(4+m_height-1) + (int)u;
        [bezierArray addObject:[nodesBezierInternalArray2 objectAtIndex:offset + 0]];
        [bezierArray addObject:[nodesBezierInternalArray2 objectAtIndex:offset + 1]];
        [bezierArray addObject:[nodesBezierInternalArray2 objectAtIndex:offset + 2]];
        [bezierArray addObject:[nodesBezierInternalArray2 objectAtIndex:offset + 3]];
        
    }
    
    u -= (int)u;
    v -= (int)v;
    
    GLKVector4 pos0;
    GLKVector4 pos1;
    GLKVector4 pos2;
    GLKVector4 pos3;
    
    for (int i=0; i<4; i++) {
        AKPoint* p0 = [bezierArray objectAtIndex:i*4+0];
        AKPoint* p1 = [bezierArray objectAtIndex:i*4+1];
        AKPoint* p2 = [bezierArray objectAtIndex:i*4+2];
        AKPoint* p3 = [bezierArray objectAtIndex:i*4+3];
        
        GLKVector4 position0;
        GLKVector4 position1;
        GLKVector4 position2;
        GLKVector4 position3;
        
        position1.x = [p1 position].x + ([p2 position].x - [p1 position].x)/3.0;
        position1.y = [p1 position].y + ([p2 position].y - [p1 position].y)/3.0;
        position1.z = [p1 position].z + ([p2 position].z - [p1 position].z)/3.0;
        position1.w = [p1 position].w + ([p2 position].w - [p1 position].w)/3.0;
        
        position2.x = [p1 position].x + 2*([p2 position].x - [p1 position].x)/3.0;
        position2.y = [p1 position].y + 2*([p2 position].y - [p1 position].y)/3.0;
        position2.z = [p1 position].z + 2*([p2 position].z - [p1 position].z)/3.0;
        position2.w = [p1 position].w + 2*([p2 position].w - [p1 position].w)/3.0;
        
        position0.x = position1.x/2.0 + [p1 position].x/2.0 + ([p0 position].x - [p1 position].x)/6.0;
        position0.y = position1.y/2.0 + [p1 position].y/2.0 + ([p0 position].y - [p1 position].y)/6.0;
        position0.z = position1.z/2.0 + [p1 position].z/2.0 + ([p0 position].z - [p1 position].z)/6.0;
        position0.w = position1.w/2.0 + [p1 position].w/2.0 + ([p0 position].w - [p1 position].w)/6.0;
        
        position3.x = position2.x/2.0 + [p2 position].x/2.0 + ([p3 position].x - [p2 position].x)/6.0;
        position3.y = position2.y/2.0 + [p2 position].y/2.0 + ([p3 position].y - [p2 position].y)/6.0;
        position3.z = position2.z/2.0 + [p2 position].z/2.0 + ([p3 position].z - [p2 position].z)/6.0;
        position3.w = position2.w/2.0 + [p2 position].w/2.0 + ([p3 position].w - [p2 position].w)/6.0;
        
        GLKMatrix4 pointsPositionMatrix = GLKMatrix4MakeWithColumns(position0,
                                                                    position1,
                                                                    position2,
                                                                    position3);
        GLKVector4 tVec = GLKVector4Make(u*u*u, u*u, u, 1);
        GLKVector4 mulVec = GLKMatrix4MultiplyVector4(bernsteinMatrix, tVec);
        GLKVector4 pos = GLKMatrix4MultiplyVector4(pointsPositionMatrix, mulVec);
        
        switch (i) {
            case 0:
                pos0 = pos;
                break;
            case 1:
                pos1 = pos;
                break;
            case 2:
                pos2 = pos;
                break;
            case 3:
                pos3 = pos;
                break;
        }
        
    }
    
    GLKVector4 du = GLKVector4Make(0, 0, 0, 0);
    for (int i=0; i<4; i++) {
        GLKVector4 tmp2 = GLKVector4Make(0, 0, 0, 0);
        GLKVector4 tmp3 = GLKVector4Make(0, 0, 0, 0);
        for (int j=0; j<4; j++) {
            tmp2 = GLKVector4Add(tmp2, GLKVector4MultiplyScalar(bezierNewArray[i*4+j], Bernstein(j, u)));
        }
        du = GLKVector4Add(du, GLKVector4MultiplyScalar(tmp2, Bernstein(i, v)));
        
    }
    return du;
    
    GLKMatrix4 pointsPositionMatrix = GLKMatrix4MakeWithColumns(pos0,
                                                                pos1,
                                                                pos2,
                                                                pos3);
    GLKVector4 tVec = GLKVector4Make(v*v*v, v*v, v, 1);
    GLKVector4 mulVec = GLKMatrix4MultiplyVector4(bernsteinMatrix, tVec);
    position = GLKMatrix4MultiplyVector4(pointsPositionMatrix, mulVec);
    
    return position;
}

- (GLKVector4) getDivUForU:(GLfloat)u andForV:(GLfloat)v {
    
    u *= m_height;
    v *= m_width;
    
    
    GLKVector4 BsplineIntNewArray[16];
    for (int i=0; i<4; i++) {
        int offset = ((int)v)*(4+m_height-1)+ i*(4+m_height-1) + (int)u;
        for (int j=0; j<4; j++) {
            BsplineIntNewArray[i*4+j] = [((AKPoint*)[_nodesInternalArray objectAtIndex:offset+j]) position];
        }
    }
    
    GLKVector4 bezierIntNewArray[16];
    for (int i=0; i<4; i++) {
        GLKVector4 p0 = BsplineIntNewArray[i*4+0];
        GLKVector4 p1 = BsplineIntNewArray[i*4+1];
        GLKVector4 p2 = BsplineIntNewArray[i*4+2];
        GLKVector4 p3 = BsplineIntNewArray[i*4+3];
        
        GLKVector4 position0;
        GLKVector4 position1;
        GLKVector4 position2;
        GLKVector4 position3;
        
        position1.x = p1.x + (p2.x - p1.x)/3.0;
        position1.y = p1.y + (p2.y - p1.y)/3.0;
        position1.z = p1.z + (p2.z - p1.z)/3.0;
        position1.w = p1.w + (p2.w - p1.w)/3.0;
        
        position2.x = p1.x + 2*(p2.x - p1.x)/3.0;
        position2.y = p1.y + 2*(p2.y - p1.y)/3.0;
        position2.z = p1.z + 2*(p2.z - p1.z)/3.0;
        position2.w = p1.w + 2*(p2.w - p1.w)/3.0;
        
        position0.x = position1.x/2.0 + p1.x/2.0 + (p0.x - p1.x)/6.0;
        position0.y = position1.y/2.0 + p1.y/2.0 + (p0.y - p1.y)/6.0;
        position0.z = position1.z/2.0 + p1.z/2.0 + (p0.z - p1.z)/6.0;
        position0.w = position1.w/2.0 + p1.w/2.0 + (p0.w - p1.w)/6.0;
        
        position3.x = position2.x/2.0 + p2.x/2.0 + (p3.x - p2.x)/6.0;
        position3.y = position2.y/2.0 + p2.y/2.0 + (p3.y - p2.y)/6.0;
        position3.z = position2.z/2.0 + p2.z/2.0 + (p3.z - p2.z)/6.0;
        position3.w = position2.w/2.0 + p2.w/2.0 + (p3.w - p2.w)/6.0;
        
        bezierIntNewArray[i*4+0] = position0;
        bezierIntNewArray[i*4+1] = position1;
        bezierIntNewArray[i*4+2] = position2;
        bezierIntNewArray[i*4+3] = position3;
    }
    
    GLKVector4 bezierNewArray[16];
    for (int i=0; i<4; i++) {
        GLKVector4 p0 = bezierIntNewArray[i+0];
        GLKVector4 p1 = bezierIntNewArray[i+4];
        GLKVector4 p2 = bezierIntNewArray[i+8];
        GLKVector4 p3 = bezierIntNewArray[i+12];
        
        GLKVector4 position0;
        GLKVector4 position1;
        GLKVector4 position2;
        GLKVector4 position3;
        
        position1.x = p1.x + (p2.x - p1.x)/3.0;
        position1.y = p1.y + (p2.y - p1.y)/3.0;
        position1.z = p1.z + (p2.z - p1.z)/3.0;
        position1.w = p1.w + (p2.w - p1.w)/3.0;
        
        position2.x = p1.x + 2*(p2.x - p1.x)/3.0;
        position2.y = p1.y + 2*(p2.y - p1.y)/3.0;
        position2.z = p1.z + 2*(p2.z - p1.z)/3.0;
        position2.w = p1.w + 2*(p2.w - p1.w)/3.0;
        
        position0.x = position1.x/2.0 + p1.x/2.0 + (p0.x - p1.x)/6.0;
        position0.y = position1.y/2.0 + p1.y/2.0 + (p0.y - p1.y)/6.0;
        position0.z = position1.z/2.0 + p1.z/2.0 + (p0.z - p1.z)/6.0;
        position0.w = position1.w/2.0 + p1.w/2.0 + (p0.w - p1.w)/6.0;
        
        position3.x = position2.x/2.0 + p2.x/2.0 + (p3.x - p2.x)/6.0;
        position3.y = position2.y/2.0 + p2.y/2.0 + (p3.y - p2.y)/6.0;
        position3.z = position2.z/2.0 + p2.z/2.0 + (p3.z - p2.z)/6.0;
        position3.w = position2.w/2.0 + p2.w/2.0 + (p3.w - p2.w)/6.0;
        
        bezierNewArray[i+0] = position0;
        bezierNewArray[i+4] = position1;
        bezierNewArray[i+8] = position2;
        bezierNewArray[i+12] = position3;
    }
    
    
    GLKVector4 position = GLKVector4Make(0, 0, 0, 1);
    
    //NSArray *bezierArray = [nodesBezierInternalArray2 subarrayWithRange:NSMakeRange(offset*16, 16)];
    NSMutableArray *bezierArray = [[NSMutableArray alloc]init];
    GLKVector4 bezierPointsArray[16];
    
    for (int i=0; i<4; i++) {
        int offset = (3+3*((int)v-1))*(4+m_height-1)+ i*(4+m_height-1) + (int)u;
        [bezierArray addObject:[nodesBezierInternalArray2 objectAtIndex:offset + 0]];
        [bezierArray addObject:[nodesBezierInternalArray2 objectAtIndex:offset + 1]];
        [bezierArray addObject:[nodesBezierInternalArray2 objectAtIndex:offset + 2]];
        [bezierArray addObject:[nodesBezierInternalArray2 objectAtIndex:offset + 3]];
    }
    
    u -= (int)u;
    v -= (int)v;
    
    GLKVector4 pos0;
    GLKVector4 pos1;
    GLKVector4 pos2;
    GLKVector4 pos3;
    
    for (int i=0; i<4; i++) {
        AKPoint* p0 = [bezierArray objectAtIndex:i*4+0];
        AKPoint* p1 = [bezierArray objectAtIndex:i*4+1];
        AKPoint* p2 = [bezierArray objectAtIndex:i*4+2];
        AKPoint* p3 = [bezierArray objectAtIndex:i*4+3];
        
        GLKVector4 position0;
        GLKVector4 position1;
        GLKVector4 position2;
        GLKVector4 position3;
        
        position1.x = [p1 position].x + ([p2 position].x - [p1 position].x)/3.0;
        position1.y = [p1 position].y + ([p2 position].y - [p1 position].y)/3.0;
        position1.z = [p1 position].z + ([p2 position].z - [p1 position].z)/3.0;
        position1.w = [p1 position].w + ([p2 position].w - [p1 position].w)/3.0;
        
        position2.x = [p1 position].x + 2*([p2 position].x - [p1 position].x)/3.0;
        position2.y = [p1 position].y + 2*([p2 position].y - [p1 position].y)/3.0;
        position2.z = [p1 position].z + 2*([p2 position].z - [p1 position].z)/3.0;
        position2.w = [p1 position].w + 2*([p2 position].w - [p1 position].w)/3.0;
        
        position0.x = position1.x/2.0 + [p1 position].x/2.0 + ([p0 position].x - [p1 position].x)/6.0;
        position0.y = position1.y/2.0 + [p1 position].y/2.0 + ([p0 position].y - [p1 position].y)/6.0;
        position0.z = position1.z/2.0 + [p1 position].z/2.0 + ([p0 position].z - [p1 position].z)/6.0;
        position0.w = position1.w/2.0 + [p1 position].w/2.0 + ([p0 position].w - [p1 position].w)/6.0;
        
        position3.x = position2.x/2.0 + [p2 position].x/2.0 + ([p3 position].x - [p2 position].x)/6.0;
        position3.y = position2.y/2.0 + [p2 position].y/2.0 + ([p3 position].y - [p2 position].y)/6.0;
        position3.z = position2.z/2.0 + [p2 position].z/2.0 + ([p3 position].z - [p2 position].z)/6.0;
        position3.w = position2.w/2.0 + [p2 position].w/2.0 + ([p3 position].w - [p2 position].w)/6.0;
        
        bezierPointsArray[i*4+0] = position0;
        bezierPointsArray[i*4+1] = position1;
        bezierPointsArray[i*4+2] = position2;
        bezierPointsArray[i*4+3] = position3;
        
        GLKMatrix4 pointsPositionMatrix = GLKMatrix4MakeWithColumns(position0,
                                                                    position1,
                                                                    position2,
                                                                    position3);
        GLKVector4 tVec = GLKVector4Make(v*v*v, v*v, v, 1);
        GLKVector4 mulVec = GLKMatrix4MultiplyVector4(bernsteinMatrix, tVec);
        GLKVector4 pos = GLKMatrix4MultiplyVector4(pointsPositionMatrix, mulVec);
        
        switch (i) {
            case 0:
                pos0 = pos;
                break;
            case 1:
                pos1 = pos;
                break;
            case 2:
                pos2 = pos;
                break;
            case 3:
                pos3 = pos;
                break;
        }
        
    }
    
    GLKVector4 du = GLKVector4Make(0, 0, 0, 0);
    for (int i=0; i<4; i++) {
        GLKVector4 tmp2 = GLKVector4Make(0, 0, 0, 0);
        GLKVector4 tmp3 = GLKVector4Make(0, 0, 0, 0);
        for (int j=0; j<4; j++) {
            tmp2 = GLKVector4Add(tmp2, GLKVector4MultiplyScalar(bezierNewArray[i*4+j], Bernstein(j, u)));
        }
        du = GLKVector4Add(du, GLKVector4MultiplyScalar(tmp2, BernDriv(i, v)));
        
    }
    return du;
    
    GLKMatrix4 pointsPositionMatrix = GLKMatrix4MakeWithColumns(pos0,
                                                                pos1,
                                                                pos2,
                                                                pos3);
    bernsteinDivMatrix = GLKMatrix4Transpose(bernsteinDivMatrix);
    GLKVector4 tVec = GLKVector4Make(u*u*u, u*u, u, 1);
    GLKVector4 mulVec = GLKMatrix4MultiplyVector4(bernsteinDivMatrix, tVec);
    position = GLKMatrix4MultiplyVector4(pointsPositionMatrix, mulVec);
    
    return position;
}

float Bernstein(int i, float t)
{
    float res = 1.0f;
    if (i == 1 || i == 2)
    {
        res *= 3;
    }
    res *= (float)pow(t, i);
    res *= (float)pow(1 - t, 3 - i);
    return res;
}
float BernDriv(int i, float t)
{
    if (i == 3)
        return (3*t*t);
    if (i == 2)
        return (6*t) - (9*t*t);
    if (i == 1)
        return (3) - (12*t) + (9*t*t);
    return (6*t) - (3*t*t) - 3;
}

- (GLKVector4) getDivVForU:(GLfloat)u andForV:(GLfloat)v {
    
    u *= m_height;
    v *= m_width;
    
    
    GLKVector4 BsplineIntNewArray[16];
    for (int i=0; i<4; i++) {
        int offset = ((int)v)*(4+m_height-1)+ i*(4+m_height-1) + (int)u;
        for (int j=0; j<4; j++) {
            BsplineIntNewArray[i*4+j] = [((AKPoint*)[_nodesInternalArray objectAtIndex:offset+j]) position];
        }
    }
    
    GLKVector4 bezierIntNewArray[16];
    for (int i=0; i<4; i++) {
        GLKVector4 p0 = BsplineIntNewArray[i*4+0];
        GLKVector4 p1 = BsplineIntNewArray[i*4+1];
        GLKVector4 p2 = BsplineIntNewArray[i*4+2];
        GLKVector4 p3 = BsplineIntNewArray[i*4+3];
        
        GLKVector4 position0;
        GLKVector4 position1;
        GLKVector4 position2;
        GLKVector4 position3;
        
        position1.x = p1.x + (p2.x - p1.x)/3.0;
        position1.y = p1.y + (p2.y - p1.y)/3.0;
        position1.z = p1.z + (p2.z - p1.z)/3.0;
        position1.w = p1.w + (p2.w - p1.w)/3.0;
        
        position2.x = p1.x + 2*(p2.x - p1.x)/3.0;
        position2.y = p1.y + 2*(p2.y - p1.y)/3.0;
        position2.z = p1.z + 2*(p2.z - p1.z)/3.0;
        position2.w = p1.w + 2*(p2.w - p1.w)/3.0;
        
        position0.x = position1.x/2.0 + p1.x/2.0 + (p0.x - p1.x)/6.0;
        position0.y = position1.y/2.0 + p1.y/2.0 + (p0.y - p1.y)/6.0;
        position0.z = position1.z/2.0 + p1.z/2.0 + (p0.z - p1.z)/6.0;
        position0.w = position1.w/2.0 + p1.w/2.0 + (p0.w - p1.w)/6.0;
        
        position3.x = position2.x/2.0 + p2.x/2.0 + (p3.x - p2.x)/6.0;
        position3.y = position2.y/2.0 + p2.y/2.0 + (p3.y - p2.y)/6.0;
        position3.z = position2.z/2.0 + p2.z/2.0 + (p3.z - p2.z)/6.0;
        position3.w = position2.w/2.0 + p2.w/2.0 + (p3.w - p2.w)/6.0;
        
        bezierIntNewArray[i*4+0] = position0;
        bezierIntNewArray[i*4+1] = position1;
        bezierIntNewArray[i*4+2] = position2;
        bezierIntNewArray[i*4+3] = position3;
    }
    
    GLKVector4 bezierNewArray[16];
    for (int i=0; i<4; i++) {
        GLKVector4 p0 = bezierIntNewArray[i+0];
        GLKVector4 p1 = bezierIntNewArray[i+4];
        GLKVector4 p2 = bezierIntNewArray[i+8];
        GLKVector4 p3 = bezierIntNewArray[i+12];
        
        GLKVector4 position0;
        GLKVector4 position1;
        GLKVector4 position2;
        GLKVector4 position3;
        
        position1.x = p1.x + (p2.x - p1.x)/3.0;
        position1.y = p1.y + (p2.y - p1.y)/3.0;
        position1.z = p1.z + (p2.z - p1.z)/3.0;
        position1.w = p1.w + (p2.w - p1.w)/3.0;
        
        position2.x = p1.x + 2*(p2.x - p1.x)/3.0;
        position2.y = p1.y + 2*(p2.y - p1.y)/3.0;
        position2.z = p1.z + 2*(p2.z - p1.z)/3.0;
        position2.w = p1.w + 2*(p2.w - p1.w)/3.0;
        
        position0.x = position1.x/2.0 + p1.x/2.0 + (p0.x - p1.x)/6.0;
        position0.y = position1.y/2.0 + p1.y/2.0 + (p0.y - p1.y)/6.0;
        position0.z = position1.z/2.0 + p1.z/2.0 + (p0.z - p1.z)/6.0;
        position0.w = position1.w/2.0 + p1.w/2.0 + (p0.w - p1.w)/6.0;
        
        position3.x = position2.x/2.0 + p2.x/2.0 + (p3.x - p2.x)/6.0;
        position3.y = position2.y/2.0 + p2.y/2.0 + (p3.y - p2.y)/6.0;
        position3.z = position2.z/2.0 + p2.z/2.0 + (p3.z - p2.z)/6.0;
        position3.w = position2.w/2.0 + p2.w/2.0 + (p3.w - p2.w)/6.0;
        
        bezierNewArray[i+0] = position0;
        bezierNewArray[i+4] = position1;
        bezierNewArray[i+8] = position2;
        bezierNewArray[i+12] = position3;
    }
    GLKVector4 position = GLKVector4Make(0, 0, 0, 1);
    
    //NSArray *bezierArray = [nodesBezierInternalArray2 subarrayWithRange:NSMakeRange(offset*16, 16)];
    NSMutableArray *bezierArray = [[NSMutableArray alloc]init];
    GLKVector4 bezierPointsArray[16];
    
    for (int i=0; i<4; i++) {
        int offset = (3+3*((int)v-1))*(4+m_height-1)+ i*(4+m_height-1) + (int)u;
        [bezierArray addObject:[nodesBezierInternalArray2 objectAtIndex:offset + 0]];
        [bezierArray addObject:[nodesBezierInternalArray2 objectAtIndex:offset + 1]];
        [bezierArray addObject:[nodesBezierInternalArray2 objectAtIndex:offset + 2]];
        [bezierArray addObject:[nodesBezierInternalArray2 objectAtIndex:offset + 3]];
    }
    
    u -= (int)u;
    v -= (int)v;
    
    GLKVector4 pos0;
    GLKVector4 pos1;
    GLKVector4 pos2;
    GLKVector4 pos3;
    
    for (int i=0; i<4; i++) {
        AKPoint* p0 = [bezierArray objectAtIndex:i*4+0];
        AKPoint* p1 = [bezierArray objectAtIndex:i*4+1];
        AKPoint* p2 = [bezierArray objectAtIndex:i*4+2];
        AKPoint* p3 = [bezierArray objectAtIndex:i*4+3];
        
        GLKVector4 position0;
        GLKVector4 position1;
        GLKVector4 position2;
        GLKVector4 position3;
        
        position1.x = [p1 position].x + ([p2 position].x - [p1 position].x)/3.0;
        position1.y = [p1 position].y + ([p2 position].y - [p1 position].y)/3.0;
        position1.z = [p1 position].z + ([p2 position].z - [p1 position].z)/3.0;
        position1.w = [p1 position].w + ([p2 position].w - [p1 position].w)/3.0;
        
        position2.x = [p1 position].x + 2*([p2 position].x - [p1 position].x)/3.0;
        position2.y = [p1 position].y + 2*([p2 position].y - [p1 position].y)/3.0;
        position2.z = [p1 position].z + 2*([p2 position].z - [p1 position].z)/3.0;
        position2.w = [p1 position].w + 2*([p2 position].w - [p1 position].w)/3.0;
        
        position0.x = position1.x/2.0 + [p1 position].x/2.0 + ([p0 position].x - [p1 position].x)/6.0;
        position0.y = position1.y/2.0 + [p1 position].y/2.0 + ([p0 position].y - [p1 position].y)/6.0;
        position0.z = position1.z/2.0 + [p1 position].z/2.0 + ([p0 position].z - [p1 position].z)/6.0;
        position0.w = position1.w/2.0 + [p1 position].w/2.0 + ([p0 position].w - [p1 position].w)/6.0;
        
        position3.x = position2.x/2.0 + [p2 position].x/2.0 + ([p3 position].x - [p2 position].x)/6.0;
        position3.y = position2.y/2.0 + [p2 position].y/2.0 + ([p3 position].y - [p2 position].y)/6.0;
        position3.z = position2.z/2.0 + [p2 position].z/2.0 + ([p3 position].z - [p2 position].z)/6.0;
        position3.w = position2.w/2.0 + [p2 position].w/2.0 + ([p3 position].w - [p2 position].w)/6.0;
        
        bezierPointsArray[i*4+0] = position0;
        bezierPointsArray[i*4+1] = position1;
        bezierPointsArray[i*4+2] = position2;
        bezierPointsArray[i*4+3] = position3;
        
        GLKMatrix4 pointsPositionMatrix = GLKMatrix4MakeWithColumns(position0,
                                                                    position1,
                                                                    position2,
                                                                    position3);
        GLKVector4 tVec = GLKVector4Make(v*v*v, v*v, v, 1);
        GLKVector4 mulVec = GLKMatrix4MultiplyVector4(bernsteinMatrix, tVec);
        GLKVector4 pos = GLKMatrix4MultiplyVector4(pointsPositionMatrix, mulVec);
        
        switch (i) {
            case 0:
                pos0 = pos;
                break;
            case 1:
                pos1 = pos;
                break;
            case 2:
                pos2 = pos;
                break;
            case 3:
                pos3 = pos;
                break;
        }
        
    }
    
    
    GLKVector4 du = GLKVector4Make(0, 0, 0, 0);
    for (int i=0; i<4; i++) {
        GLKVector4 tmp2 = GLKVector4Make(0, 0, 0, 0);
        GLKVector4 tmp3 = GLKVector4Make(0, 0, 0, 0);
        for (int j=0; j<4; j++) {
            tmp2 = GLKVector4Add(tmp2, GLKVector4MultiplyScalar(bezierNewArray[i*4+j], BernDriv(j, u)));
        }
        du = GLKVector4Add(du, GLKVector4MultiplyScalar(tmp2, Bernstein(i, v)));
        
    }
    return du;
    
    GLKMatrix4 pointsPositionMatrix = GLKMatrix4MakeWithColumns(pos0,
                                                                pos1,
                                                                pos2,
                                                                pos3);
    GLKVector4 tVec = GLKVector4Make(v*v*v, v*v, v, 1);
    GLKVector4 mulVec = GLKMatrix4MultiplyVector4(bernsteinDivMatrix, tVec);
    position = GLKMatrix4MultiplyVector4(pointsPositionMatrix, mulVec);
    
    return position;
}

- (void) applySurfaceToHeightMap:(float**)heightMap normalMap:(GLKVector4**)normalMap withSize:(unsigned int)size  {
    float modelFactor = 1.5;
    float step = 1.0f/(size*modelFactor);
    float maxModelSize = 1;
    float modelYTranslation = 0;
    
    dispatch_group_t d_group = dispatch_group_create();
    dispatch_queue_t bg_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    int threads = 1;
    for (int i=0; i<threads; i++) {
        dispatch_group_async(d_group, bg_queue, ^{
            [self applySurfaceToHeightMap:heightMap
                           andToNormalMap:normalMap
                                 withSize:size
                                    fromU:i/(float)threads
                                      toU:(i+1)/(float)threads
                          withModelFactor:modelFactor
                                 withStep:step
                         withMaxModelSize:maxModelSize
                    withModelYTranslation:modelYTranslation];
        });
    }
    dispatch_group_wait(d_group, DISPATCH_TIME_FOREVER);
}

- (void) applySurfaceToHeightMap:(float**)heightMap
                  andToNormalMap:(GLKVector4**)normalMap
                        withSize:(unsigned int)size
                           fromU:(float)fromU toU:(float)toU
                 withModelFactor:(float)modelFactor
                        withStep:(float)step
                withMaxModelSize:(float)maxModelSize
           withModelYTranslation:(float)modelYTranslation {
    
    for (float u=fromU; u<toU; u+=step) {
        for (float v=0; v<1; v+=step) {
            //if (v > 0.125 && v < 0.625f) {
            //    continue;
            //}
            GLKVector4 position = [self getPositionForU:u andForV:v];
            //NSLog(@"Position for u=%f v=%f = (%f, %f, %f, %f)", u, v, position.x, position.y, position.z, position.w);
            position = GLKVector4MultiplyScalar(position, modelFactor);
            position.y += modelYTranslation;
            if (position.x > maxModelSize || position.x < -maxModelSize || position.z > maxModelSize || position.z < -maxModelSize || position.y > maxModelSize || position.y < 0) {
                if (!(position.y < 0)) {
                    NSLog(@"Model wystaje!!");
                }
                continue;
            }
            position.x += maxModelSize;
            position.z += maxModelSize;
            
            position.x *= size/2;
            position.z *= size/2;
            position.y *= 40;
            
            float height = heightMap[(int)round(position.z)][(int)round(position.x)];
            float height2 = MAX(height, position.y*1.7);
            if (height2 > height) {
                height = height2;
                
                GLKVector4 divU = [self getDivUForU:u andForV:v];
                GLKVector4 divV = [self getDivVForU:u andForV:v];
                GLKVector4 normal = GLKVector4CrossProduct(divV, divU);
                //printf("%lf %lf %lf\n", divU.x, divU.y, divU.z);
                //printf("%lf %lf %lf\n", divV.x, divV.y, divV.z);
                if (normal.x==0 && normal.y==0 && normal.z==0 && normal.w==0) {
                    normal = GLKVector4Make(0, 0, 1, 0);
                }
                //printf("%lf %lf %lf\n\n", GLKVector4Normalize(normal).x, GLKVector4Normalize(normal).y, GLKVector4Normalize(normal).z);
                heightMap[(int)round(position.z)][(int)round(position.x)] = height;
                normalMap[(int)round(position.z)][(int)round(position.x)] = GLKVector4Normalize(normal);
            }
            
        }
    }
}

- (void) applySurfaceToHeightMap:(unsigned char*)heightMap withSize:(unsigned int)size andToNormalMap:(unsigned char*)normalMap {
    float modelFactor = 1.5;
    float step = 1.0f/(size*modelFactor);
    float maxModelSize = 1;
    float modelYTranslation = 0;
    
    dispatch_group_t d_group = dispatch_group_create();
    dispatch_queue_t bg_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    int threads = 1;
    for (int i=0; i<threads; i++) {
        dispatch_group_async(d_group, bg_queue, ^{
            [self applySurfaceToHeightMap:heightMap
                                 withSize:size
                           andToNormalMap:normalMap
                                    fromU:i/(float)threads
                                      toU:(i+1)/(float)threads
                          withModelFactor:modelFactor
                                 withStep:step
                         withMaxModelSize:maxModelSize
                    withModelYTranslation:modelYTranslation];
        });
    }
    dispatch_group_wait(d_group, DISPATCH_TIME_FOREVER);
}

- (void) applySurfaceToHeightMap:(unsigned char*)heightMap
                        withSize:(unsigned int)size
                  andToNormalMap:(unsigned char*)normalMap
                           fromU:(float)fromU toU:(float)toU
                 withModelFactor:(float)modelFactor
                        withStep:(float)step
                withMaxModelSize:(float)maxModelSize
           withModelYTranslation:(float)modelYTranslation {
    
    for (float u=fromU; u<toU; u+=step) {
        for (float v=0; v<1; v+=step) {
            if (v > 0.125 && v < 0.625f) {
                continue;
            }
            GLKVector4 position = [self getPositionForU:u andForV:v];
            //NSLog(@"Position for u=%f v=%f = (%f, %f, %f, %f)", u, v, position.x, position.y, position.z, position.w);
            position = GLKVector4MultiplyScalar(position, modelFactor);
            position.y += modelYTranslation;
            if (position.x > maxModelSize || position.x < -maxModelSize || position.z > maxModelSize || position.z < -maxModelSize || position.y > maxModelSize || position.y < 0) {
                if (!(position.y < 0)) {
                    NSLog(@"Model wystaje!!");
                }
                continue;
            }
            position.x += maxModelSize;
            position.z += maxModelSize;
            
            position.x *= size/2;
            position.z *= size/2;
            position.y *= 40;
            
            int heightmapPosition = (int)round(position.z)*size*4+(int)round(position.x)*4;
            unsigned char r = heightMap[heightmapPosition];
            int val = (int)r;
            int val2 = MAX(r, (int)(((position.y/maxModelSize)*255.0f)*2));
            if (val2 > val) {
                val = val2;
                r = MIN(val, 255);
                val = (int)r;
                heightMap[heightmapPosition+0] = r;
                heightMap[heightmapPosition+1] = r;
                heightMap[heightmapPosition+2] = r;
                
                GLKVector4 divU = [self getDivUForU:u andForV:v];
                GLKVector4 divV = [self getDivVForU:u andForV:v];
                //printf("%lf %lf %lf\n", divU.x, divU.y, divU.z);
                //printf("%lf %lf %lf\n\n", divV.x, divV.y, divV.z);
                
                GLKVector4 normal = GLKVector4CrossProduct(divU, divV);
                GLKVector4 normalToMap;
                normalToMap.x = normal.x;
                normalToMap.y = normal.y;
                normalToMap.z = normal.z;
                normalToMap.w = 0;
                normalToMap = GLKVector4Normalize(normalToMap);
                normalToMap.w = 1;
                normalMap[heightmapPosition+0] = normalToMap.x*255;
                normalMap[heightmapPosition+1] = normalToMap.y*255;
                normalMap[heightmapPosition+2] = normalToMap.z*255;
                normalMap[heightmapPosition+3] = normalToMap.w*255;
            }
            
        }
    }
}

- (void)generateFourthPathForFourthHeightMap:(float**)heightMap andNormalMap:(GLKVector4**)normalMap withSize:(unsigned int)size addToPaths:(NSMutableArray*)positions {
    float modelFactor = 1.5;
    float step = 0.01f;
    float maxModelSize = 1;
    float modelYTranslation = 0;
    float offset = 20;
    float r = 4;
    GLKVector3 millPosition;
    
    for (float u=0; u<1; u+=step) {
        for (float v=0; v<1; v+=step) {
            //if (v > 0.125 && v < 0.625f) {
            //    continue;
            //}
            GLKVector4 basePosition = [self getPositionForU:u andForV:v];
            GLKVector4 position = basePosition;
            basePosition = position = GLKVector4MultiplyScalar(position, modelFactor);
            //NSLog(@"Position: %lf %lf %lf %lf", position.x, position.y, position.z, position.w);
            position.y += modelYTranslation;
            if (position.x > maxModelSize || position.x < -maxModelSize || position.z > maxModelSize || position.z < -maxModelSize) {
                continue;
            }
            position.x += maxModelSize;
            position.z += maxModelSize;
            
            position.x *= size/2;
            position.z *= size/2;
            GLKVector4 normal = GLKVector4Normalize(normalMap[(int)round(position.z)][(int)round(position.x)] );
            //normal = GLKVector4MultiplyScalar(normal, r);
            //basePosition = GLKVector4Add(GLKVector4MultiplyScalar(basePosition, modelFactor), normal);
            //NSLog(@"Base Position: %lf %lf %lf %lf", basePosition.x, basePosition.y, basePosition.z, basePosition.w);
            //NSLog(@"Position: %lf %lf %lf %lf", position.x, position.y, position.z, position.w);
            //NSLog(@"Normal: %lf %lf %lf %lf", normal.x, normal.y, normal.z, normal.w);
            float height = [self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(position.z, position.x, position.y)];
            float height2 = [self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(position.z+normal.z*r*size/150.0f, position.x+normal.x*r*size/150.0f, position.y+normal.y*r*size/150.0f)];
            millPosition = GLKVector3Make(basePosition.z*75+normal.z*r, basePosition.x*75+normal.x*r, height2+offset-r);
            //float height2 = [self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(position.z, position.x, position.y)];
            //millPosition = GLKVector3Make(basePosition.z*75, basePosition.x*75, height2+offset-r);
            //millPosition = GLKVector3Make(basePosition.z*75, basePosition.x*75, height+offset-r);
            [self addVector:millPosition toArray:positions];
        }
        continue;
        /*
        for (float v=0; v<0.125f; v+=step) {
            //if (v > 0.125 && v < 0.625f) {
            //    continue;
            //}
            GLKVector4 basePosition = [self getPositionForU:u andForV:v];
            GLKVector4 position = basePosition;
            basePosition = position = GLKVector4MultiplyScalar(position, modelFactor);
            //NSLog(@"Position: %lf %lf %lf %lf", position.x, position.y, position.z, position.w);
            position.y += modelYTranslation;
            if (position.x > maxModelSize || position.x < -maxModelSize || position.z > maxModelSize || position.z < -maxModelSize) {
                continue;
            }
            position.x += maxModelSize;
            position.z += maxModelSize;
            
            position.x *= size/2;
            position.z *= size/2;
            GLKVector4 normal = GLKVector4Normalize(normalMap[(int)round(position.z)][(int)round(position.x)] );
            //normal = GLKVector4MultiplyScalar(normal, r);
            //basePosition = GLKVector4Add(GLKVector4MultiplyScalar(basePosition, modelFactor), normal);
            //NSLog(@"Base Position: %lf %lf %lf %lf", basePosition.x, basePosition.y, basePosition.z, basePosition.w);
            //NSLog(@"Position: %lf %lf %lf %lf", position.x, position.y, position.z, position.w);
            //NSLog(@"Normal: %lf %lf %lf %lf", normal.x, normal.y, normal.z, normal.w);
            float height = [self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(position.z, position.x, position.y)];
            float height2 = [self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(position.z+normal.y*r*size/150.0f, position.x+normal.x*r*size/150.0f, position.y+normal.z*r*size/150.0f)];
            millPosition = GLKVector3Make(basePosition.z*75+normal.z*r, basePosition.x*75+normal.x*r, height2+offset-r);
            //float height2 = [self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(position.z, position.x, position.y)];
            //millPosition = GLKVector3Make(basePosition.z*75, basePosition.x*75, height2+offset-r);
            //millPosition = GLKVector3Make(basePosition.z*75, basePosition.x*75, height+offset-r);
            [self addVector:millPosition toArray:positions];
        }
        u+=step;
        if (u>=1) break;
        for (float v=0.125f-step; v>=0; v-=step) {
            //if (v > 0.125 && v < 0.625f) {
            //    continue;
            //}
            GLKVector4 basePosition = [self getPositionForU:u andForV:v];
            GLKVector4 position = basePosition;
            basePosition = position = GLKVector4MultiplyScalar(position, modelFactor);
            //NSLog(@"Position: %lf %lf %lf %lf", position.x, position.y, position.z, position.w);
            position.y += modelYTranslation;
            if (position.x > maxModelSize || position.x < -maxModelSize || position.z > maxModelSize || position.z < -maxModelSize) {
                continue;
            }
            position.x += maxModelSize;
            position.z += maxModelSize;
            
            position.x *= size/2;
            position.z *= size/2;
            GLKVector4 normal = GLKVector4Normalize(normalMap[(int)round(position.z)][(int)round(position.x)] );
            //normal = GLKVector4MultiplyScalar(normal, r);
            //basePosition = GLKVector4Add(GLKVector4MultiplyScalar(basePosition, modelFactor), normal);
           // NSLog(@"Base Position: %lf %lf %lf %lf", basePosition.x, basePosition.y, basePosition.z, basePosition.w);
            //NSLog(@"Position: %lf %lf %lf %lf", position.x, position.y, position.z, position.w);
            //NSLog(@"Normal: %lf %lf %lf %lf", normal.x, normal.y, normal.z, normal.w);
            float height = [self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(position.z, position.x, position.y)];
            float height2 = [self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(position.z+normal.y*r*size/150.0f, position.x+normal.x*r*size/150.0f, position.y+normal.z*r*size/150.0f)];
            millPosition = GLKVector3Make(basePosition.z*75+normal.z*r, basePosition.x*75+normal.x*r, height2+offset-r);
            //float height2 = [self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(position.z, position.x, position.y)];
            //millPosition = GLKVector3Make(basePosition.z*75, basePosition.x*75, height2+offset-r);
            //millPosition = GLKVector3Make(basePosition.z*75, basePosition.x*75, height+offset-r);
            [self addVector:millPosition toArray:positions];
        }
        for (float v=1-step; v>=0.625f; v-=step) {
            //if (v > 0.125 && v < 0.625f) {
            //    continue;
            //}
            GLKVector4 basePosition = [self getPositionForU:u andForV:v];
            GLKVector4 position = basePosition;
            basePosition = position = GLKVector4MultiplyScalar(position, modelFactor);
            //NSLog(@"Position: %lf %lf %lf %lf", position.x, position.y, position.z, position.w);
            position.y += modelYTranslation;
            if (position.x > maxModelSize || position.x < -maxModelSize || position.z > maxModelSize || position.z < -maxModelSize) {
                continue;
            }
            position.x += maxModelSize;
            position.z += maxModelSize;
            
            position.x *= size/2;
            position.z *= size/2;
            GLKVector4 normal = GLKVector4Normalize(normalMap[(int)round(position.z)][(int)round(position.x)] );
            //normal = GLKVector4MultiplyScalar(normal, r);
            //basePosition = GLKVector4Add(GLKVector4MultiplyScalar(basePosition, modelFactor), normal);
            //NSLog(@"Base Position: %lf %lf %lf %lf", basePosition.x, basePosition.y, basePosition.z, basePosition.w);
            //NSLog(@"Position: %lf %lf %lf %lf", position.x, position.y, position.z, position.w);
            //NSLog(@"Normal: %lf %lf %lf %lf", normal.x, normal.y, normal.z, normal.w);
            float height = [self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(position.z, position.x, position.y)];
            float height2 = [self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(position.z+normal.y*r*size/150.0f, position.x+normal.x*r*size/150.0f, position.y+normal.z*r*size/150.0f)];
            millPosition = GLKVector3Make(basePosition.z*75+normal.z*r, basePosition.x*75+normal.x*r, height2+offset-r);
            //float height2 = [self getHeightFromHeightMap:heightMap withSize:size fromPosition:GLKVector3Make(position.z, position.x, position.y)];
            //millPosition = GLKVector3Make(basePosition.z*75, basePosition.x*75, height2+offset-r);
            //millPosition = GLKVector3Make(basePosition.z*75, basePosition.x*75, height+offset-r);
            [self addVector:millPosition toArray:positions];
        }
         */
    }
}

- (float) getHeightFromHeightMap:(float**)heightMap withSize:(unsigned int)size fromPosition:(GLKVector3) position {
    float height = 0;
    height = heightMap[(int)(position.x)][(int)(position.y)];
    return height;
}

- (void) addVector:(GLKVector3)vector toArray:(NSMutableArray*)mutableArray {
    //NSLog(@"Add vector:(%.3f, %.3f, %.3f", vector.x, vector.y, vector.z);
    [mutableArray addObject:[NSValue valueWithBytes:&vector objCType:@encode(GLKVector3)]];
    
}


@end
