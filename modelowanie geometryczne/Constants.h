//
//  Constants.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 25.03.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef modelowanie_geometryczne_Constants_h
#define modelowanie_geometryczne_Constants_h

const float selectionMouseEpsilon = 0.05;
const float selectionCursorEpsilon = 0.01;
const int MaxVertices = 1000000;
const int MaxIndices = MaxVertices * 4;
const unsigned int shaderAttribute = 0;

#endif
