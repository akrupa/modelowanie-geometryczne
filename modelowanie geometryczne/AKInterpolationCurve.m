//
//  AKInterpolationCurve.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 06.06.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKInterpolationCurve.h"

@implementation AKInterpolationCurve

static int m_qualityDivisor = 2;

- (AKInterpolationCurve*) initWithPoints:(NSMutableArray*)points
                                 screenWidth:(int)screenWidth
                                screenHeight:(int)screenHeight
                                      number:(NSUInteger)number
                                   andMatrix:(GLKMatrix4)M {
    
    originalNodesInternalArray = [[NSMutableArray alloc]init];
    _nodesInternalArray = [[NSMutableArray alloc]init];
    for (int i=0; i<[points count]; i++) {
        AKPoint *point = [points objectAtIndex:i];
        AKPoint *pointToAdd = [[AKPoint alloc]initAtPosition:[point position] andNumber:0];
        if (i==0) {
            AKPoint *pointToAdd2 = [[AKPoint alloc]initAtPosition:[point position] andNumber:0];
            AKPoint *pointToAdd3 = [[AKPoint alloc]initAtPosition:[point position] andNumber:0];
            [originalNodesInternalArray addObject:pointToAdd2];
            [originalNodesInternalArray addObject:pointToAdd3];
        }
        [originalNodesInternalArray addObject:pointToAdd];
        [_nodesInternalArray addObject:pointToAdd];
    }

    _bsplineNodesInternalArray = [[NSMutableArray alloc]initWithArray:points];

    name = [NSString stringWithFormat:@"Interpolation CurveC2%lu", number];
    
    bernsteinMatrix.m00 = -1;
    bernsteinMatrix.m11 = -6;
    bernsteinMatrix.m22 = 0;
    bernsteinMatrix.m33 = 0;
    bernsteinMatrix.m01 = bernsteinMatrix.m10 = 3;
    bernsteinMatrix.m02 = bernsteinMatrix.m20 = -3;
    bernsteinMatrix.m03 = bernsteinMatrix.m30 = 1;
    bernsteinMatrix.m12 = bernsteinMatrix.m21 = 3;
    bernsteinMatrix.m13 = bernsteinMatrix.m31 = 0;
    bernsteinMatrix.m23 = bernsteinMatrix.m32 = 0;
    
    m_screenWidth = screenWidth;
    m_screenHeight = screenHeight;
    internalArray = 0;
    internalIndicSize = 0;
    segments = (int)[originalNodesInternalArray count]-3;
    [self refreshBernstein];
    return self;
}

- (void) preparePointsArray:(NSArray*)pointsArray {
    if ([pointsArray count] <= 4) {
        _nodesInternalArray = [[NSMutableArray alloc]initWithArray:pointsArray];
        while ([_nodesInternalArray count]%4 != 0) {
            [_nodesInternalArray addObject:[_nodesInternalArray lastObject]];
        }
    } else {
        _nodesInternalArray = [[NSMutableArray alloc]init];
        
        AKPoint *point0 = [pointsArray objectAtIndex:0];
        [_nodesInternalArray addObject:point0];
        
        AKPoint *point1 = [pointsArray objectAtIndex:1];
        [_nodesInternalArray addObject:point1];
        
        AKPoint *point2 = [pointsArray objectAtIndex:2];
        [_nodesInternalArray addObject:point2];
        
        AKPoint *point3 = [pointsArray objectAtIndex:3];
        [_nodesInternalArray addObject:point3];
        
        AKPoint *point6;
        
        for (int i = 4; i < [pointsArray count]; i++) {
            
            point6 = [pointsArray objectAtIndex:i];
            
            GLKVector4 position;
            position.x = 2*[point3 position].x - [point2 position].x;
            position.y = 2*[point3 position].y - [point2 position].y;
            position.z = 2*[point3 position].z - [point2 position].z;
            position.w = 2*[point3 position].w - [point2 position].w;
            AKPoint *point4 = [[AKPoint alloc]initAtPosition:position andNumber:0];
            
            position.x = 2*[point4 position].x - (2*[point2 position].x - [point1 position].x);
            position.y = 2*[point4 position].y - (2*[point2 position].y - [point1 position].y);
            position.z = 2*[point4 position].z - (2*[point2 position].z - [point1 position].z);
            position.w = 2*[point4 position].w - (2*[point2 position].w - [point1 position].w);
            
            AKPoint *point5 = [[AKPoint alloc]initAtPosition:position andNumber:0];
            
            [_nodesInternalArray addObject:point4];
            [_nodesInternalArray addObject:point5];
            [_nodesInternalArray addObject:point6];
            
            point0 = point3;
            point1 = point4;
            point2 = point5;
            point3 = point6;
        }
    }
    
    nodesCount = (int)[_nodesInternalArray count];
    segments = nodesCount/3.0;
    
}

- (void) addPointsToCurve:(NSArray*)pointsArray {
    for(int i=0;i<[pointsArray count];i++) {
        AKPoint* point = [pointsArray objectAtIndex:i];
        [_bsplineNodesInternalArray addObject:point];
        [originalNodesInternalArray addObject:[[AKPoint alloc]initAtPosition:[point position] andNumber:0]];
    }
    [self refreshBernstein];

}

- (void) addPointToCurve:(AKPoint*)pointsArray {
    [_bsplineNodesInternalArray addObject:pointsArray];
    [originalNodesInternalArray addObject:[[AKPoint alloc]initAtPosition:[pointsArray position] andNumber:0]];
    [self refreshBernstein];
}

- (int) pointsCount {
    return self.pointsCount;
}

- (int) linesCount {
    return lines;
}

- (void) countStepsFromMatrix:(GLKMatrix4) M {
    [self refreshBernstein];

    MOld = M;
    free(steps2);
    free(lines2);
    steps2 = malloc(segments * sizeof(int));
    lines2 = malloc(segments * sizeof(int));
    steps = 0;
    lines = 0;
    GLKVector4 pointsPositions[4*segments];
    
    for (int segment = 0; segment < segments; segment++) {
        float minX = MAXFLOAT;
        float minY = MAXFLOAT;
        float minZ = MAXFLOAT;
        float maxX = -MAXFLOAT;
        float maxY = -MAXFLOAT;
        float maxZ = -MAXFLOAT;
        
        for(int i=0; i<4; i++) {
            AKPoint* point = [_nodesInternalArray objectAtIndex:segment*3 +i];
            GLKVector4 pointPosition = GLKMatrix4MultiplyVector4(M, [point position]);
            //pointsPositions[4*segment + i] = GLKMatrix4MultiplyVector4(M, [point position]);
            pointsPositions[4*segment + i] = [point position];
            if (pointPosition.x < minX) {
                minX = pointPosition.x;
            }
            if (pointPosition.y < minY) {
                minY = pointPosition.y;
            }
            if (pointPosition.z < minZ) {
                minZ = pointPosition.z;
            }
            if (pointPosition.x > maxX) {
                maxX = pointPosition.x;
            }
            if (pointPosition.y > maxY) {
                maxY = pointPosition.y;
            }
            if (pointPosition.z > maxZ) {
                maxZ = pointPosition.z;
            }
        }
        
        double max = MAX(maxX-minX, maxY-minY);
        max = MAX(max, maxZ - minZ);
        
        steps2[segment] = m_screenWidth*(max/m_qualityDivisor);
        if(steps2[segment]<0) {
            steps2[segment]=0;
        }
        lines2[segment] = steps2[segment];
        
        steps += steps2[segment];
        lines += lines2[segment];
    }
    lines += segments-1;
    
    free(internalArray);
    free(internalIndices);
    internalArray = malloc((steps+segments)*sizeof(GLKVector4));
    internalArraySize = steps+segments;
    internalIndices = malloc((steps+segments-1) * sizeof(indic));
    internalIndicSize = steps+segments-1;
    
    int segmentOffset = 0;
    for (int segment = 0; segment < segments; segment++) {
        float step = 1.0f/steps2[segment];
        GLKMatrix4 pointsPositionMatrix = GLKMatrix4MakeWithColumns(pointsPositions[4*segment+0],
                                                                    pointsPositions[4*segment+1],
                                                                    pointsPositions[4*segment+2],
                                                                    pointsPositions[4*segment+3]);
        
        for (int i=0; i<=steps2[segment]; i++) {
            GLKVector4 tVec = GLKVector4Make((i*step)*(i*step)*(i*step), (i*step)*(i*step), (i*step), 1);
            GLKVector4 mulVec = GLKMatrix4MultiplyVector4(bernsteinMatrix, tVec);
            internalArray[i + segmentOffset] = GLKMatrix4MultiplyVector4(pointsPositionMatrix, mulVec);
        }
        segmentOffset += steps2[segment]+1;
    }
    
    for (int i=0; i<internalIndicSize; i++) {
        internalIndices[i].from = i;
        internalIndices[i].to = i+1;
    }
}

-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize
    andArrayIndicOffset:(int *)offset {
    
    memcpy(array + *arrayPoint, internalArray, internalArraySize * sizeof(GLKVector4));
    
    GLushort *indic = (GLushort *)indices;
    GLushort *internalIndic = (GLushort *)internalIndices;
    
    for (int i=0; i < internalIndicSize*2; i++) {
        indic[*indicesSize + i] = internalIndic[i] + *offset;
    }
    
    *offset += internalArraySize;
    *arrayPoint += internalArraySize;
    *indicesSize += internalIndicSize*2;
}

-(int)vertexArraySize {
    
    int sum = internalArraySize;
    
    for (int i=0; i<[self.nodesInternalArray count]; i++) {
        AKObject* obj = [self.nodesInternalArray objectAtIndex:i];
        sum += [obj vertexArraySize];
    }
    return sum;
}

-(int)indicArraySize {
    
    int sum = internalIndicSize*2;
    
    for (int i=0; i<[self.nodesInternalArray count]; i++) {
        AKObject* obj = [self.nodesInternalArray objectAtIndex:i];
        sum += [obj indicArraySize];
    }
    return sum;
}

- (void) deletePoint:(AKPoint*)point {
    [_bsplineNodesInternalArray removeObject:point];
    [originalNodesInternalArray removeObjectAtIndex:0];
    [self refreshBernstein];
}

- (void) refresh {
    [self preparePointsArray:originalNodesInternalArray];
}

-(void)refreshBernstein {
    
    int segmentsLocal = (int)[_bsplineNodesInternalArray count]-1;
    double aX[segmentsLocal+1];
    double bX[segmentsLocal+1];
    double cX[segmentsLocal+1];
    double dX[segmentsLocal+1];
    
    GLKVector4 pointsPositions[segmentsLocal+1];
    
    for (int segment = 0; segment < segmentsLocal; segment++) {
        
        for(int i=0; i<2; i++) {
            AKPoint* point = [_bsplineNodesInternalArray objectAtIndex:segment+i];
            pointsPositions[segment + i] = [point position];
        }
    }
    
    for(int i=0; i<segmentsLocal+1; i++) {
        if (i==0 || i == segmentsLocal) {
            bX[i] = 1;
            dX[i] = pointsPositions[i].x;
        } else {
            bX[i] = 4;
            dX[i] = 6*pointsPositions[i].x;
        }
        
        if (i == 0 || i == segmentsLocal) {
            cX[i] = 0;
        } else if (i <segmentsLocal) {
            cX[i] = 1;
        }
        if (i == segmentsLocal || i == 0) {
            aX[i] = 0;
        } else {
            aX[i] = 1;
        }
    }
    
    double aY[segmentsLocal+1];
    double bY[segmentsLocal+1];
    double cY[segmentsLocal+1];
    double dY[segmentsLocal+1];
    
    for(int i=0; i<segmentsLocal+1; i++) {
        if (i==0 || i == segmentsLocal) {
            bY[i] = 1;
            dY[i] = pointsPositions[i].y;
        } else {
            bY[i] = 4;
            dY[i] = 6*pointsPositions[i].y;
        }
        if (i == 0 || i == segmentsLocal) {
            cY[i] = 0;
        } else if (i <segmentsLocal) {
            cY[i] = 1;
        }
        if (i == segmentsLocal || i == 0) {
            aY[i] = 0;
        } else {
            aY[i] = 1;
        }
    }
    
    double aZ[segmentsLocal+1];
    double bZ[segmentsLocal+1];
    double cZ[segmentsLocal+1];
    double dZ[segmentsLocal+1];
    
    for(int i=0; i<segmentsLocal+1; i++) {
        if (i==0 || i == segmentsLocal) {
            bZ[i] = 1;
            dZ[i] = pointsPositions[i].z;
        } else {
            bZ[i] = 4;
            dZ[i] = 6*pointsPositions[i].z;
        }
        
        if (i == 0 || i == segmentsLocal) {
            cZ[i] = 0;
        } else if (i <segmentsLocal) {
            cZ[i] = 1;
        }
        if (i == segmentsLocal || i == 0) {
            aZ[i] = 0;
        } else {
            aZ[i] = 1;
        }
    }
    
    [self solveA:aX B:bX C:cX D:dX N:segmentsLocal+1];
    [self solveA:aY B:bY C:cY D:dY N:segmentsLocal+1];
    [self solveA:aZ B:bZ C:cZ D:dZ N:segmentsLocal+1];
    
    NSMutableArray* pointsArray = [[NSMutableArray alloc]init];
    
    AKPoint* p = [[AKPoint alloc]initAtPosition:GLKVector4Make(2*dX[0]-dX[1], 2*dY[0]-dY[1], 2*dZ[0]-dZ[1], 1) andNumber:0];
    [pointsArray addObject:p];
    for (int i=0; i<segmentsLocal+1; i++) {
        AKPoint* newPoint = [[AKPoint alloc]initAtPosition:GLKVector4Make(dX[i], dY[i], dZ[i], 1) andNumber:i];
        [pointsArray addObject:newPoint];
    }
    p = [[AKPoint alloc]initAtPosition:GLKVector4Make(2*dX[segmentsLocal]-dX[segmentsLocal-1], 2*dY[segmentsLocal]-dY[segmentsLocal-1], 2*dZ[segmentsLocal]-dZ[segmentsLocal-1], 1) andNumber:0];
    [pointsArray addObject:p];
    
    

    
    AKPoint *k0 = [pointsArray objectAtIndex:0];
    AKPoint *k1 = [pointsArray objectAtIndex:1];
    AKPoint *k2 = [pointsArray objectAtIndex:2];
    AKPoint *k3 = [pointsArray objectAtIndex:3];
    
    
    GLKVector4 position;
    
    position.x = [k1 position].x + ([k2 position].x - [k1 position].x)/3.0;
    position.y = [k1 position].y + ([k2 position].y - [k1 position].y)/3.0;
    position.z = [k1 position].z + ([k2 position].z - [k1 position].z)/3.0;
    position.w = [k1 position].w + ([k2 position].w - [k1 position].w)/3.0;
    AKPoint *b1 = [originalNodesInternalArray objectAtIndex:1];
    [b1 setPosition:position];
    
    position.x = [k1 position].x + 2*([k2 position].x - [k1 position].x)/3.0;
    position.y = [k1 position].y + 2*([k2 position].y - [k1 position].y)/3.0;
    position.z = [k1 position].z + 2*([k2 position].z - [k1 position].z)/3.0;
    position.w = [k1 position].w + 2*([k2 position].w - [k1 position].w)/3.0;
    AKPoint *b2 = [originalNodesInternalArray objectAtIndex:2];
    [b2 setPosition:position];
    
    position.x = [b1 position].x/2.0 + [k1 position].x/2.0 + ([k0 position].x - [k1 position].x)/6.0;
    position.y = [b1 position].y/2.0 + [k1 position].y/2.0 + ([k0 position].y - [k1 position].y)/6.0;
    position.z = [b1 position].z/2.0 + [k1 position].z/2.0 + ([k0 position].z - [k1 position].z)/6.0;
    position.w = [b1 position].w/2.0 + [k1 position].w/2.0 + ([k0 position].w - [k1 position].w)/6.0;
    AKPoint *b0 = [originalNodesInternalArray objectAtIndex:0];
    [b0 setPosition:position];
    
    position.x = [b2 position].x/2.0 + [k2 position].x/2.0 + ([k3 position].x - [k2 position].x)/6.0;
    position.y = [b2 position].y/2.0 + [k2 position].y/2.0 + ([k3 position].y - [k2 position].y)/6.0;
    position.z = [b2 position].z/2.0 + [k2 position].z/2.0 + ([k3 position].z - [k2 position].z)/6.0;
    position.w = [b2 position].w/2.0 + [k2 position].w/2.0 + ([k3 position].w - [k2 position].w)/6.0;
    AKPoint *b3 = [originalNodesInternalArray objectAtIndex:3];
    [b3 setPosition:position];
    
    for (int i=4; i < [originalNodesInternalArray count]; i++) {
        AKPoint *k4 = [pointsArray objectAtIndex:i];
        
        position.x = [k2 position].x/2.0 + [k3 position].x/2.0 + 2*([k3 position].x - [k2 position].x)/6.0 + ([k4 position].x - [k3 position].x)/6.0;
        position.y = [k2 position].y/2.0 + [k3 position].y/2.0 + 2*([k3 position].y - [k2 position].y)/6.0 + ([k4 position].y - [k3 position].y)/6.0;
        position.z = [k2 position].z/2.0 + [k3 position].z/2.0 + 2*([k3 position].z - [k2 position].z)/6.0 + ([k4 position].z - [k3 position].z)/6.0;
        position.w = [k2 position].w/2.0 + [k3 position].w/2.0 + 2*([k3 position].w - [k2 position].w)/6.0 + ([k4 position].w - [k3 position].w)/6.0;
        
        AKPoint *b6 = [originalNodesInternalArray objectAtIndex:i];
        [b6 setPosition:position];
        
        k2 = k3;
        k3 = k4;
    }
    
    [self refresh];
    
}

- (void) switchToBsplineMode {
    
    if ([_nodesInternalArray count] < 4) {
        return;
    }
    
    AKPoint *b0 = [originalNodesInternalArray objectAtIndex:0];
    AKPoint *b1 = [originalNodesInternalArray objectAtIndex:1];
    AKPoint *b2 = [originalNodesInternalArray objectAtIndex:2];
    AKPoint *b3 = [originalNodesInternalArray objectAtIndex:3];
    
    GLKVector4 position;
    
    position.x = 2*[b1 position].x - [b2 position].x;
    position.y = 2*[b1 position].y - [b2 position].y;
    position.z = 2*[b1 position].z - [b2 position].z;
    position.w = 2*[b1 position].w - [b2 position].w;
    AKPoint *k1 = [[AKPoint alloc]initAtPosition:position andNumber:0];
    
    position.x = 2*[b2 position].x - [b1 position].x;
    position.y = 2*[b2 position].y - [b1 position].y;
    position.z = 2*[b2 position].z - [b1 position].z;
    position.w = 2*[b2 position].w - [b1 position].w;
    AKPoint *k2 = [[AKPoint alloc]initAtPosition:position andNumber:0];
    
    position.x = 6*[b0 position].x - 3*[b1 position].x - 2*[k1 position].x;
    position.y = 6*[b0 position].y - 3*[b1 position].y - 2*[k1 position].y;
    position.z = 6*[b0 position].z - 3*[b1 position].z - 2*[k1 position].z;
    position.w = 6*[b0 position].w - 3*[b1 position].w - 2*[k1 position].w;
    AKPoint *k0 = [[AKPoint alloc]initAtPosition:position andNumber:0];
    
    
    position.x = 6*[b3 position].x - 3*[b2 position].x - 2*[k2 position].x;
    position.y = 6*[b3 position].y - 3*[b2 position].y - 2*[k2 position].y;
    position.z = 6*[b3 position].z - 3*[b2 position].z - 2*[k2 position].z;
    position.w = 6*[b3 position].w - 3*[b2 position].w - 2*[k2 position].w;
    AKPoint *k3 = [[AKPoint alloc]initAtPosition:position andNumber:0];
    
    [_bsplineNodesInternalArray removeAllObjects];
    [_bsplineNodesInternalArray addObject:k0];
    [_bsplineNodesInternalArray addObject:k1];
    [_bsplineNodesInternalArray addObject:k2];
    [_bsplineNodesInternalArray addObject:k3];
    
    
    for (int i=4; i < [originalNodesInternalArray count]; i++) {
        AKPoint *b6 = [originalNodesInternalArray objectAtIndex:i];
        
        
        position.x = 6*[b6 position].x - [k2 position].x - 4*[k3 position].x;
        position.y = 6*[b6 position].y - [k2 position].y - 4*[k3 position].y;
        position.z = 6*[b6 position].z - [k2 position].z - 4*[k3 position].z;
        position.w = 6*[b6 position].w - [k2 position].w - 4*[k3 position].w;
        
        AKPoint *k4 = [[AKPoint alloc]initAtPosition:position andNumber:0];
        [_bsplineNodesInternalArray addObject:k4];
        k2 = k3;
        k3 = k4;
        
    }
    
}

+ (void) setQualityDivisor:(int)divisor {
    if (divisor < 1) {
        divisor = 1;
    }
    m_qualityDivisor = divisor;
}

+ (int) qualityDivisor {
    return m_qualityDivisor;
}

-(void) solveA:(double*)a B:(double*)b C:(double*)c D:(double*)d N:(int)n {

    /*
     // n is the number of unknowns
     
     |b0 c0 0 ||x0| |d0|
     |a1 b1 c1||x1|=|d1|
     |0  a2 b2||x2| |d2|
     
     1st iteration: b0x0 + c0x1 = d0 -> x0 + (c0/b0)x1 = d0/b0 ->
     
     x0 + g0x1 = r0               where g0 = c0/b0        , r0 = d0/b0
     
     2nd iteration:     | a1x0 + b1x1   + c1x2 = d1
     from 1st it.: -| a1x0 + a1g0x1        = a1r0
     -----------------------------
     (b1 - a1g0)x1 + c1x2 = d1 - a1r0
     
     x1 + g1x2 = r1               where g1=c1/(b1 - a1g0) , r1 = (d1 - a1r0)/(b1 - a1g0)
     
     3rd iteration:      | a2x1 + b2x2   = d2
     from 2st it. : -| a2x1 + a2g1x2 = a2r2
     -----------------------
     (b2 - a2g1)x2 = d2 - a2r2
     x2 = r2                      where                     r2 = (d2 - a2r2)/(b2 - a2g1)
     Finally we have a triangular matrix:
     |1  g0 0 ||x0| |r0|
     |0  1  g1||x1|=|r1|
     |0  0  1 ||x2| |r2|
     
     Condition: ||bi|| > ||ai|| + ||ci||
     
     in this version the c matrix reused instead of g
     and             the d matrix reused instead of r and x matrices to report results
     Written by Keivan Moradi, 2014
     */
    
    
    n--; // since we start from x0 (not x1)
    c[0] /= b[0];
    d[0] /= b[0];
    
    for (int i = 1; i < n; i++) {
        c[i] /= b[i] - a[i]*c[i-1];
        d[i] = (d[i] - a[i]*d[i-1]) / (b[i] - a[i]*c[i-1]);
    }
    
    d[n] = (d[n] - a[n]*d[n-1]) / (b[n] - a[n]*c[n-1]);
    
    for (int i = n; i-- > 0;) {
        d[i] -= c[i]*d[i+1];
    }
}

@end
