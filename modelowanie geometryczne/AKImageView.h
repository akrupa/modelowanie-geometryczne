//
//  AKImageView.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 03.03.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AKImageView : NSImageView

@property (nonatomic) IBOutlet  NSTextField *mImageValueTextField;
@property (nonatomic) IBOutlet  NSTextField *aImageValueTextField;
@property (nonatomic) IBOutlet  NSTextField *bImageValueTextField;
@property (nonatomic) IBOutlet  NSTextField *cImageValueTextField;
@property (nonatomic) IBOutlet  NSTextField *scaleValueTextField;

@end
