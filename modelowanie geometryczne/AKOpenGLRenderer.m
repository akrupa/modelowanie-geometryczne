//
//  AKOpenGLRenderer.m
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 25.02.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKOpenGLRenderer.h"
#import <GLKit/GLKMath.h>
#import "AKObject.h"
#import "AKPoint.h"
#import "AKBezierCurve.h"
#import "AKBezierC2Curve.h"
#import "AKGregoryPatch.h"
#import "Constants.h"

@implementation AKOpenGLRenderer

GLuint m_viewWidth;
GLuint m_viewHeight;
GLuint m_modelVAO;

GLuint m_vertexVBO;
GLuint m_indicesVBO;

GLuint m_vertShader;
GLuint m_fragShader;

GLuint program;
GLint colorUniformPosition;

NSMutableArray *sceneElementsArray;

GLKVector4 *array;
indic *indices;
int arraySize = 0;
int indicSize = 0;
int selectedPoint = -1;

GLKVector4 cursorArray[4];
indic cursorIndices[3];

float r_projection = 3.0;
float e_projection = 0.3;


NSUInteger pointPlusCounter;

GLKMatrix4 transformationMatrix;
GLKMatrix4 cursorTransformationMatrix;
GLKMatrix4 projectionMatrixR;
GLKMatrix4 projectionMatrixL;
GLKMatrix4 projectionMatrix;

NSColor *leftColor;
NSColor *rightColor;

GLKVector4 cursorScenePosition;
GLKVector4 cursorScreenPostion;

bool use3D = false;

- (void) render3DScene:(AKScene*)scene {
    glEnable(GL_DEPTH_CLAMP);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glBlendEquation(GL_MAX);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable (GL_BLEND);
    glBindVertexArray(m_modelVAO);
    glUseProgram(program);
    
    use3D = true;
    
    [self setColor:leftColor toUniformLocation:colorUniformPosition];
    [self copyLeftEyeDataFromScene:scene];
    [self renderCursorFromScene:scene];
    [self renderPointsFromScene:scene];
    [self renderBezierCurvesFromScene:scene];
    
    [self setColor:rightColor toUniformLocation:colorUniformPosition];
    [self copyRightEyeDataFromScene:scene];
    [self renderCursorFromScene:scene];
    [self renderPointsFromScene:scene];
    [self renderBezierCurvesFromScene:scene];
}

- (void) renderScene:(AKScene*)scene {
    glEnable(GL_DEPTH_CLAMP);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_BLEND);
    glBindVertexArray(m_modelVAO);
    glUseProgram(program);
    
    use3D = false;
    
    [self copyDataFromScene:scene];
    [self renderCursorFromScene:scene];
    [self renderPointsFromScene:scene];
    [self renderBezierCurvesFromScene:scene];
}

- (void) copyDataFromScene:(AKScene*)scene {
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexVBO);
    GLfloat *vertexArray = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indicesVBO);
    GLushort *indicesArray = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
    
    GLKVector4 *vertexTempArray;
    indic *indicesTempArray;
    int vertexTempArraySize = 0;
    int indicTempArraySize = 0;
    
    [scene copySceneToVertexArray:&vertexTempArray
                         withSize:&vertexTempArraySize
                  andIndicesArray:&indicesTempArray
                         withSize:&indicTempArraySize];
    
    memcpy(vertexArray, vertexTempArray, vertexTempArraySize * sizeof(GLKVector4));
    memcpy(indicesArray, indicesTempArray, indicTempArraySize * sizeof(indic));
    
    glUnmapBuffer(GL_ARRAY_BUFFER);
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
}

- (void) copyLeftEyeDataFromScene:(AKScene*)scene {
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexVBO);
    GLfloat *vertexArray = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indicesVBO);
    GLushort *indicesArray = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
    
    GLKVector4 *vertexTempArray;
    indic *indicesTempArray;
    int vertexTempArraySize = 0;
    int indicTempArraySize = 0;
    
    [scene copyLeftEyeSceneSceneToVertexArray:&vertexTempArray
                                     withSize:&vertexTempArraySize
                              andIndicesArray:&indicesTempArray
                                     withSize:&indicTempArraySize];
    
    memcpy(vertexArray, vertexTempArray, vertexTempArraySize * sizeof(GLKVector4));
    memcpy(indicesArray, indicesTempArray, indicTempArraySize * sizeof(indic));
    
    glUnmapBuffer(GL_ARRAY_BUFFER);
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
}

- (void) copyRightEyeDataFromScene:(AKScene*)scene {
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexVBO);
    GLfloat *vertexArray = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indicesVBO);
    GLushort *indicesArray = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
    
    GLKVector4 *vertexTempArray;
    indic *indicesTempArray;
    int vertexTempArraySize = 0;
    int indicTempArraySize = 0;
    
    [scene copyRightEyeSceneToVertexArray:&vertexTempArray
                                 withSize:&vertexTempArraySize
                          andIndicesArray:&indicesTempArray
                                 withSize:&indicTempArraySize];
    
    memcpy(vertexArray, vertexTempArray, vertexTempArraySize * sizeof(GLKVector4));
    memcpy(indicesArray, indicesTempArray, indicTempArraySize * sizeof(indic));
    
    glUnmapBuffer(GL_ARRAY_BUFFER);
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
}

- (void) renderCursorFromScene:(AKScene*)scene {
    if (use3D) {
        glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, 0);
        glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, (void*)4);
        glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, (void*)8);
    } else {
        [self setColor:[NSColor redColor] toUniformLocation:colorUniformPosition];
        glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, 0);
        [self setColor:[NSColor greenColor] toUniformLocation:colorUniformPosition];
        glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, (void*)4);
        [self setColor:[NSColor blueColor] toUniformLocation:colorUniformPosition];
        glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, (void*)8);
    }
}

- (void) renderPointsFromScene:(AKScene*)scene {
    glPointSize(7.0f);
    if (!use3D) {
        [self setColor:[NSColor greenColor] toUniformLocation:colorUniformPosition];
    }
    glDrawElements(GL_POINTS, [scene pointsCount], GL_UNSIGNED_SHORT, (void*)[scene pointsIndexBegin]);
    
    if (!use3D) {
        [self setColor:[NSColor blueColor] toUniformLocation:colorUniformPosition];
    }
    glDrawElements(GL_POINTS, [scene bsplinePointsCount], GL_UNSIGNED_SHORT, (void*)([scene bsplinePointsIndexBegin]* sizeof(GLushort)));
    
    if ([scene selectedPointIndex] >= 0) {
        glPointSize(11.0f);
        if (!use3D) {
            [self setColor:[NSColor redColor] toUniformLocation:colorUniformPosition];
        }
        glDrawElements(GL_POINTS, 1, GL_UNSIGNED_SHORT, (void*)([scene pointsIndexBegin] + [scene selectedPointIndex] * sizeof(GLushort)));
    }

}

- (void) renderBezierCurvesFromScene:(AKScene*)scene {
    if (!use3D) {
        [self setColor:[NSColor magentaColor] toUniformLocation:colorUniformPosition];
    }
    //NSLog(@"drawing %d lines and beginning at %d", [scene bezierLinesCount], [scene bezierCurvesIndexBegin]);
    glDrawElements(GL_LINES, [scene bezierLinesCount]*2, GL_UNSIGNED_SHORT, (void*)([scene bezierCurvesIndexBegin] * sizeof(GLushort)));
    
    [self setColor:[NSColor greenColor] toUniformLocation:colorUniformPosition];
    glDrawElements(GL_LINES, [scene trimmingLinesCount]*2, GL_UNSIGNED_SHORT, (void*)([scene trimmingCurvesIndexBegin] * sizeof(GLushort)));
    
    int lines = [scene bezierLinesCount];
    if (lines > 65535.0 * 3.0/4.0) {
        [AKBezierCurve setQualityDivisor:[AKBezierCurve qualityDivisor]*2];
        [AKBezierC2Curve setQualityDivisor:[AKBezierC2Curve qualityDivisor]*2];
        [AKGregoryPatch setQualityDivisor:[AKBezierC2Curve qualityDivisor]*2];
    } else if (lines < 65535.0 * 1.0/4.0) {
        [AKBezierC2Curve setQualityDivisor:[AKBezierC2Curve qualityDivisor]/2];
        [AKBezierCurve setQualityDivisor:[AKBezierCurve qualityDivisor]/2];
        [AKGregoryPatch setQualityDivisor:[AKBezierC2Curve qualityDivisor]/2];
    }
}

- (void) renderCursor {
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexVBO);
    GLfloat *vertexArray = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    GLKMatrix4 M = GLKMatrix4Multiply(transformationMatrix, cursorTransformationMatrix);
    M = GLKMatrix4Multiply(projectionMatrix, M);
    GLKVector4 array2[4];
    for (int i=0; i<4; i++) {
        array2[i] = GLKMatrix4MultiplyVector4(M, cursorArray[i]);
    }
    memcpy(vertexArray, array2, sizeof(array2));
    glUnmapBuffer(GL_ARRAY_BUFFER);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indicesVBO);
    GLushort *indicesArray = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
    memcpy(indicesArray, cursorIndices, 3*sizeof(indic));
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

    [self setColor:[NSColor redColor] toUniformLocation:colorUniformPosition];
    glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, 0);
    [self setColor:[NSColor greenColor] toUniformLocation:colorUniformPosition];
    glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, (void*)4);
    [self setColor:[NSColor blueColor] toUniformLocation:colorUniformPosition];
    glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, (void*)8);
    
    
    cursorScreenPostion = GLKMatrix4MultiplyVector4(M, cursorArray[0]);
    cursorScreenPostion = GLKVector4DivideScalar(cursorScreenPostion, cursorScreenPostion.w);
}

- (void) renderCursor3D {
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexVBO);
    GLfloat *vertexArray = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    GLKMatrix4 M = GLKMatrix4Multiply(transformationMatrix, cursorTransformationMatrix);
    M = GLKMatrix4Multiply(projectionMatrixL, M);
    GLKVector4 array2[4];
    for (int i=0; i<4; i++) {
        array2[i] = GLKMatrix4MultiplyVector4(M, cursorArray[i]);
    }
    memcpy(vertexArray, array2, sizeof(array2));
    glUnmapBuffer(GL_ARRAY_BUFFER);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indicesVBO);
    GLushort *indicesArray = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
    memcpy(indicesArray, cursorIndices, 3*sizeof(indic));
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
    
    [self setColor:leftColor toUniformLocation:colorUniformPosition];
    glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, 0);
    glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, (void*)4);
    glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, (void*)8);
    
    
    
    
    
    
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexVBO);
    vertexArray = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    M = GLKMatrix4Multiply(transformationMatrix, cursorTransformationMatrix);
    M = GLKMatrix4Multiply(projectionMatrixR, M);
    for (int i=0; i<4; i++) {
        array2[i] = GLKMatrix4MultiplyVector4(M, cursorArray[i]);
    }
    memcpy(vertexArray, array2, sizeof(array2));
    glUnmapBuffer(GL_ARRAY_BUFFER);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indicesVBO);
    indicesArray = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
    memcpy(indicesArray, cursorIndices, 3*sizeof(indic));
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
    
    [self setColor:rightColor toUniformLocation:colorUniformPosition];
    glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, 0);
    glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, (void*)4);
    glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, (void*)8);
    
    cursorScreenPostion = array2[0];
    cursorScreenPostion = GLKVector4DivideScalar(cursorScreenPostion, cursorScreenPostion.w);
}

- (void) setColor:(NSColor*)color toUniformLocation:(GLint)uniformLocation {
    float colorf[4];
    colorf[0] = color.redComponent;
    colorf[1] = color.greenComponent;
    colorf[2] = color.blueComponent;
    colorf[3] = 1.0f;
    glUniform4fv(uniformLocation, 1, colorf);
}


- (void) resizeWithWidth:(GLuint)width AndHeight:(GLuint)height
{
	glViewport(0, 0, width, height);
    
	m_viewWidth = width;
	m_viewHeight = height;
}

- (id) initWithDefaultFBO: (GLuint) defaultFBOName {
    
    if((self = [super init]))
	{
        NSLog(@"%s %s", glGetString(GL_RENDERER), glGetString(GL_VERSION));
        
		m_viewWidth = 600;
		m_viewHeight = 600;
        
        glGenVertexArrays(1, &m_modelVAO);
        glBindVertexArray(m_modelVAO);
        
        glGenBuffers(1, &m_vertexVBO);
        glBindBuffer(GL_ARRAY_BUFFER, m_vertexVBO);
        glBufferData(GL_ARRAY_BUFFER, MaxVertices  * sizeof(GLKVector4), NULL, GL_DYNAMIC_DRAW);
        
        glGenBuffers(1, &m_indicesVBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indicesVBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, MaxIndices * 2 * sizeof(GLushort), NULL, GL_DYNAMIC_DRAW);
        
        glVertexAttribPointer(shaderAttribute, 4, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(shaderAttribute);
        
        glBindVertexArray(0);
        
        program = glCreateProgram();
        
        [self loadShaders];
        
        glAttachShader(program, m_vertShader);
        glAttachShader(program, m_fragShader);
        
        transformationMatrix = GLKMatrix4Identity;
        cursorTransformationMatrix = GLKMatrix4Identity;
        projectionMatrixR = GLKMatrix4Identity;
        projectionMatrixL = GLKMatrix4Identity;
        projectionMatrix = GLKMatrix4Identity;
        //projectionMatrix.m22 = 0;
        projectionMatrixR.m23 = 1.0/3;
        projectionMatrixL.m23 = 1.0/3;
        projectionMatrix.m23 = 1.0/r_projection;
        
        projectionMatrixR.m20 = -0.3/6;
        projectionMatrixL.m20 = 0.3/6;
        
        glBindAttribLocation(program, shaderAttribute, "position");
        glLinkProgram(program);
        colorUniformPosition = glGetUniformLocation(program, "colorFrag");

        glClearColor(0, 0, 0, 0);
        sceneElementsArray = [[NSMutableArray alloc]init];
        

        cursorArray[0].w = 1;
        
        cursorArray[1].x = 0.1;
        cursorArray[1].w = 1;
        
        cursorArray[2].y = 0.1;
        cursorArray[2].w = 1;
        
        cursorArray[3].z = 0.1;
        cursorArray[3].w = 1;
        
        cursorIndices[0].from = 0;
        cursorIndices[0].to = 1;
        
        cursorIndices[1].from = 0;
        cursorIndices[1].to = 2;
        
        cursorIndices[2].from = 0;
        cursorIndices[2].to = 3;
        //[self loadInitData];
        // [self rotateX:M_PI_4];
        
        leftColor = [NSColor cyanColor];
        rightColor = [NSColor redColor];
        
        //cursorScenePosition.x = 1;
        //cursorScenePosition.y = 1;
        //cursorScenePosition.z = 1;
        cursorScenePosition.w = 1;
        pointPlusCounter = 0;
        
    }
    return self;
}

- (void) loadShaders {
    NSString *vertShaderPathname;
    NSString *fragShaderPathname;
    const GLchar *vertSource;
    const GLchar *fragSource;
    vertShaderPathname = [[NSBundle mainBundle]pathForResource:@"AKLineShader" ofType:@"vsh"];
    fragShaderPathname = [[NSBundle mainBundle]pathForResource:@"AKLineShader" ofType:@"fsh"];

    vertSource = (GLchar *)[[NSString stringWithContentsOfFile:vertShaderPathname encoding:NSUTF8StringEncoding error:nil] UTF8String];
    fragSource = (GLchar *)[[NSString stringWithContentsOfFile:fragShaderPathname encoding:NSUTF8StringEncoding error:nil] UTF8String];

    m_vertShader = glCreateShader(GL_VERTEX_SHADER);
    m_fragShader = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(m_vertShader, 1, &vertSource, NULL);
    glShaderSource(m_fragShader, 1, &fragSource, NULL);
    
    glCompileShader(m_vertShader);

    glCompileShader(m_fragShader);

}

- (void) dealloc {
	glBindVertexArray(m_modelVAO);
	
    glDeleteBuffers(1, &m_vertexVBO);
    glDeleteBuffers(1, &m_indicesVBO);
	
	glDeleteVertexArrays(1, &m_modelVAO);
}

@end
