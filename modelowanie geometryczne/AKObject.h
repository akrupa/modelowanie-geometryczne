//
//  AKObject.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 21.03.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKMath.h>

typedef struct indic {
    GLushort from,to;
} indic;

@interface AKObject : NSObject
{
    NSString *name;
    GLKVector4 *internalArray;
    indic *internalIndices;
    
    int internalArraySize;
    int internalIndicSize;
}

-(NSString*) elementName;
-(void) setElementName:(NSString*)nameS;

-(void)addObjectToArray:(GLKVector4**)array
               withSize:(int*)arraySize
             andIndices:(indic**)indices
        withIndicesSize:(int*)indicesSize;

-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize;

-(AKObject*)initAtPosition:(GLKVector4)positionV andNumber:(NSUInteger)number;

-(int)vertexArraySize;
-(int)indicArraySize;
-(void)replacePoints:(NSMutableArray*)pointsToReplace withPoint:(AKObject*)point;

@end
