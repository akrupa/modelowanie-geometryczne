//
//  AKOpenGLRenderer.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 25.02.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGL/gl3.h>
#import "AKScene.h"

@interface AKOpenGLRenderer : NSObject

- (id) initWithDefaultFBO: (GLuint) defaultFBOName;
- (void) resizeWithWidth:(GLuint)width AndHeight:(GLuint)height;
- (void) renderScene:(AKScene*)scene;
- (void) render3DScene:(AKScene*)scene;

@end
