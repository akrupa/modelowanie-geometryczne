//
//  AKInterpolationCurve.h
//  modelowanie geometryczne
//
//  Created by Adrian Krupa on 06.06.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKObject.h"
#import "AKPoint.h"

@interface AKInterpolationCurve : AKObject {
    NSUInteger pointsCount;
    NSMutableArray *originalNodesInternalArray;
    
    GLKMatrix4 bernsteinMatrix;
    int m_screenWidth;
    int m_screenHeight;
    int nodesCount;
    int steps;
    int segments;
    int lines;
    
    int *steps2;
    int *lines2;
    GLKMatrix4 MOld;
    
}
@property NSMutableArray *nodesInternalArray;
@property NSMutableArray *bsplineNodesInternalArray;

+ (int) qualityDivisor;
+ (void) setQualityDivisor:(int)divisor;

-(AKInterpolationCurve*) initWithPoints:(NSMutableArray*)points
                            screenWidth:(int)screenWidth
                           screenHeight:(int)screenHeight
                                 number:(NSUInteger)number
                              andMatrix:(GLKMatrix4)M;

- (void) preparePointsArray:(NSArray*)pointsArray;
- (int) pointsCount;
- (int) linesCount;
- (void) countStepsFromMatrix:(GLKMatrix4)M;
- (void) addPointsToCurve:(NSArray*)pointsArray;
- (void) addPointToCurve:(AKPoint*)pointsArray;
- (void) deletePoint:(AKPoint*)point;
-(void)refresh;
-(void)refreshBernstein;
-(void)addObjectToArray:(GLKVector4 *)array
           atArrayPoint:(int *)arrayPoint
             andIndices:(indic *)indices
         atIndicesPoint:(int *)indicesSize
    andArrayIndicOffset:(int *)offset;
-(void) solveA:(double*)a B:(double*)b C:(double*)c D:(double*)d N:(int)n;

@end
